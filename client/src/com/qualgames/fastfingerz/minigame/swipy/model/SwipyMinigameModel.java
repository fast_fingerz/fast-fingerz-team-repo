package com.qualgames.fastfingerz.minigame.swipy.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.qualgames.fastfingerz.minigame.GameType;
import com.qualgames.fastfingerz.minigame.model.MinigameModel;
import com.qualgames.fastfingerz.minigame.swipy.view.SwipyMinigameFragment;
import com.qualgames.fastfingerz.minigame.view.MinigameFragment;

public class SwipyMinigameModel extends MinigameModel
{
	final static String TAG = "Fast Fingerz";
	
	public SwipyMinigameModel()
	{
		super();
		Log.d(TAG, "SwipyMinigameModel::SwipyMinigameModel");
	}
	
	public SwipyMinigameModel(JSONArray json) throws JSONException
	{
		super(json);
		Log.d(TAG, "SwipyMinigameModel::SwipyMinigameModel");
	}
	
	// TODO: Don't think we need this function
	/*
	public static SwipyMinigameModel DecodeFromJSON(JSONObject msgData) throws JSONException 
	{
		Log.d(TAG, "SwipyMinigameModel::DecodeFromJSON");
		return new SwipyMinigameModel(msgData);
	}
	*/

	@Override
	protected void GenerateGameData() 
	{		
		Log.d(TAG, "SwipyMinigameModel::GenerateGameData from scratch");
		for ( int i = 0; i < m_numberOfQuestions; i++ )
		{
			m_questionList.add(new SwipyQuestion());
		}		
	}

	@Override
	protected void GenerateGameData(JSONArray jsonArray) throws JSONException 
	{
		Log.d(TAG, "SwipyMinigameModel::GenerateGameData from json");

		for ( int i = 0; i < jsonArray.length(); i++ )
		{
			m_questionList.add(new SwipyQuestion(jsonArray.getJSONObject(i)));
		}
	}

	@Override
	public MinigameFragment GetMyMinigameFragment() {
		Log.d(TAG, "SwipyMinigameModel::GetMyMinigameFragment");
		return new SwipyMinigameFragment();		
	}

	@Override
	public GameType GetMyGameType() {
		return GameType.SWIPY;
	}
	
	

}
