package com.qualgames.fastfingerz.minigame.swipy.model;

import java.util.Random;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.qualgames.fastfingerz.minigame.model.Question;
import com.qualgames.fastfingerz.minigame.model.UserInput;


// TODO: Make additional equations such as >, <, <=. >=
public class SwipyQuestion extends Question {
	
	final static String TAG = "Fast Fingerz";

	public enum OperatorType
	{
		ADDITION (0),
		SUBTRACTION (1),
		DIVISION (2),
		MULTIPLICATION (3);
		
		public int value;
		OperatorType(int pValue)
		{
			value = pValue;
		}
		
		public String toString()
		{
			String operator = "";
			switch (value)
			{
			case 0:
				operator =  "+";
				break;
			case 1:
				operator = "-";
				break;
			case 2:
				operator = "�";
				break;
			case 3:
				operator = "x";
				break;
			}
			return operator;
		}
	}
	
	
	public enum EqualsType
	{
		EQUALS (0),
		LOWER_THAN (1),
		GREATER_THAN (2),
		EQUALS_OR_LOWER_THAN (3),
		EQUALS_OR_GREATER_THAN (4);
		
		public int value;
		EqualsType(int pValue)
		{
			value = pValue;
		}
		
		public String toString()
		{
			String equals = "";
			switch (value)
			{
			case 0:
				equals = "=";
				break;
			case 1:
				equals = "<";
				break;
			case 2:
				equals = ">";
				break;
			case 3:
				equals = "<=";
				break;
			case 4:
				equals = ">=";
				break;
			}
			return equals;
		}
	}
	
	private static final int MAX_QUESTION_INT = 10;
	private static final int INCORRECT_ANSWER_RANGE = 5;
	
	private String m_equationString;
	private OperatorType m_operatorType;
	private int m_firstInt;
	private int m_secondInt;
	private int m_answer;
	private boolean m_isEquationCorrect;
	
	public SwipyQuestion()
	{
		super();
		Log.d(TAG, "SwipyQuestion::SwipyQuestion");
	}
	
	public SwipyQuestion(JSONObject json) throws JSONException
	{
		super(json);
		Log.d(TAG, "SwipyQuestion::SwipyQuestion from json");
	}
	
	public String GetEquationString()
	{
		return m_equationString;
	}

	@Override
	public JSONObject EncodeToJSON() throws JSONException{
		Log.d(TAG, "SwipyQuestion::EncodeToJSON");
		// TODO Figure out how to convert object to JSON
		JSONObject json = new JSONObject();
		
		// first int
		json.put("fi", m_firstInt);
		// second int
		json.put("si", m_secondInt);
		// answer
		json.put("a", m_answer);
		// operator type
		json.put("o", m_operatorType.value);
		// if equation is correct
		json.put("ic", m_isEquationCorrect);
		// if question is worth double
		json.put("wd", m_bIsWorthDouble);
		
		return json;		
	}
	
	@Override
	public void GenerateQuestionData(JSONObject json) throws JSONException 
	{
		m_firstInt = json.getInt("fi");
		m_secondInt = json.getInt("si");
		m_answer = json.getInt("a");
		m_operatorType = OperatorType.values()[(json.getInt("o"))];
		m_isEquationCorrect = json.getBoolean("ic");
		m_bIsWorthDouble = json.getBoolean("wd");
		
		GenerateEquationString();
	}

	@Override
	public boolean IsUserInputCorrect(UserInput userInput) {
		
		Log.d(TAG, "SwipyQuestion::IsUserInputCorrect");
		SwipyUserInput swipyUserInput = (SwipyUserInput)userInput;
		
		if ( (swipyUserInput.GetAnswer() == SwipyUserInput.Answer.LEFT && m_isEquationCorrect == false) || 
			 (swipyUserInput.GetAnswer() == SwipyUserInput.Answer.RIGHT && m_isEquationCorrect == true) )
		{
			return true;
		}
		
		return false;
	}

	@Override
	public void GenerateQuestionData() 
	{
		Log.d(TAG, "SwipyQuestion::GenerateQuestionData");
		
		Random r = new Random();
		int correctAnswer = 0;
		
		// generate the first number in equation
		m_firstInt = r.nextInt(MAX_QUESTION_INT);
		// generate the second number in equation
		m_secondInt = r.nextInt(MAX_QUESTION_INT);
		// generate the operator (+, -, /, x)
		m_operatorType = OperatorType.values()[r.nextInt(OperatorType.values().length)];
		// sanity check to make sure we don't divide by 0
		if ( m_operatorType == OperatorType.DIVISION && m_secondInt == 0 )
		{
			m_secondInt++;
		}
		// will equation be correct
		m_isEquationCorrect = r.nextInt(2) == 1;
		
		correctAnswer = GenerateAnswer(m_firstInt, m_secondInt, m_operatorType);
		
		if (m_isEquationCorrect)
		{
			m_answer = correctAnswer;
		}
		else
		{
			boolean toAdd = r.nextInt(2) == 1;
			int offset = r.nextInt(INCORRECT_ANSWER_RANGE);
			
			if ( !toAdd )
			{
				offset *= -1;
			}
			m_answer = correctAnswer + offset;
		}
		
		GenerateEquationString();
	}
	
	private void GenerateEquationString()
	{
		m_equationString = String.valueOf(m_firstInt) + " " + m_operatorType.toString() + " " + 
				   String.valueOf(m_secondInt) + " = " + String.valueOf(m_answer);

		Log.d(TAG, "SwipyQuestion::GenerateQuestionData equationString " + m_equationString);
	}
	
	private int GenerateAnswer(int firstNumber, int secondNumber, OperatorType operator)
	{
		int answer = 0;
		
		switch (operator)
		{
			case ADDITION:
				answer = firstNumber + secondNumber;
				break;
			case DIVISION:
				answer = firstNumber/secondNumber;
				break;
			case MULTIPLICATION:
				answer = firstNumber * secondNumber;
				break;
			case SUBTRACTION:
				answer = firstNumber - secondNumber;
				break;
		}
		Log.d(TAG, "SwipyQuestion::GenerateAnswer " + answer);
		
		return answer;
	}
}
