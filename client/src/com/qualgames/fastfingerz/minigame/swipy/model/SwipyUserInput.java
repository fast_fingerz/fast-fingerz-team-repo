package com.qualgames.fastfingerz.minigame.swipy.model;

import android.util.Log;

import com.qualgames.fastfingerz.minigame.model.UserInput;

public class SwipyUserInput extends UserInput {
	
	final static String TAG = "Fast Fingerz";
	
	public enum Answer
	{
		LEFT,
		RIGHT
	}
	
	private Answer m_answer;
	public SwipyUserInput(Answer answer)
	{
		Log.d(TAG, "SwipyUserInput::SwipyUserInput");
		m_answer = answer;
	}
	
	public Answer GetAnswer()
	{
		Log.d(TAG, "SwipyUserInput::GetAnswer");
		return m_answer;
	}

}
