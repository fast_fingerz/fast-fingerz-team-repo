package com.qualgames.fastfingerz.minigame.swipy.view;

import android.os.Handler;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.qualgames.fastfingerz.R;
import com.qualgames.fastfingerz.minigame.model.Question;
import com.qualgames.fastfingerz.minigame.swipy.model.SwipyQuestion;
import com.qualgames.fastfingerz.minigame.swipy.model.SwipyUserInput;
import com.qualgames.fastfingerz.minigame.swipy.model.SwipyUserInput.Answer;
import com.qualgames.fastfingerz.minigame.view.MinigameFragment;

public class SwipyMinigameFragment extends MinigameFragment {
	
	final static String TAG = "Fast Fingerz";
	
	private static final int SWIPE_MIN_DISTANCE = 50;
    private static final int SWIPE_MAX_OFF_PATH = 250;
    private static final int SWIPE_THRESHOLD_VELOCITY = 150;
    private GestureDetector m_gestureDetector;
    View.OnTouchListener m_gestureListener;
    private boolean m_isAnimationPending = false;
    private String m_nextEquationString;
    
 	private Handler m_buzzerShowHandler = new Handler();

	@Override
	protected void DisplayQuestion(Question question) {

		Log.d(TAG, "SwipyMinigameFragment::DisplayQuestion");
		
		SwipyQuestion swipyQuestion = (SwipyQuestion) question;
		
		TextView equationView = (TextView) getActivity().findViewById(R.id.equation_text);
		
		// set the equation string just in case
		m_nextEquationString = swipyQuestion.GetEquationString();
		if ( swipyQuestion.IsWorthDouble() )
		{
			m_nextEquationString += "  xx";
		}
		
		if ( !m_isAnimationPending )
		{
			equationView.setText(m_nextEquationString);
			equationView.setVisibility(View.VISIBLE);
			equationView.refreshDrawableState();
		}
		// if we got a new equation string while animation is not yet complete save the equation string to be put when
		// animation is complete
		else
		{
			//m_nextEquationString = swipyQuestion.GetEquationString();
		}
	}

	@Override
	public void ProcessUserInputCorrect() 
	{
		Log.d(TAG, "SwipyMinigameFragment::ProcessUserInputCorrect");
		/*
		ImageView buzzerView = (ImageView) getActivity().findViewById(R.id.buzzer_view);
		buzzerView.setImageResource(R.drawable.ic_correct_buzzer);
		
		// set the timer for how long to show buzzer color
		m_buzzerShowHandler.postDelayed(m_buzzerShowThread, BUZZER_SHOW_TIME);
		*/
	}

	@Override
	public void ProcessUserInputWrong() 
	{
		Log.d(TAG, "SwipyMinigameFragment::ProcessUserInputWrong");
		/*
		ImageView buzzerView = (ImageView) getActivity().findViewById(R.id.buzzer_view);
		buzzerView.setImageResource(R.drawable.ic_wrong_buzzer);
		
		// set the timer for how long to show buzzer color
		m_buzzerShowHandler.postDelayed(m_buzzerShowThread, BUZZER_SHOW_TIME);
		*/
	}

	@Override
	protected int GetMyResourceID() {

		return R.layout.swipy_minigame_fragment_layout;
	}
	
	@Override
	protected void SetInitialLayout()
	{
	
	}
	
	@Override
	protected void RegisterUserInputListeners(View view)
	{
		Log.d(TAG, "SwipyMinigameFragment::RegisterUserInputListeners");
		
		// register click for correct answer
		Button correctButton = (Button) view.findViewById(R.id.swipy_button_correct);
		correctButton.setOnClickListener(new OnClickListener()
	    {
             @Override
             public void onClick(View v)
             {
            	 if (m_isIgnoreUserInput) {
            		 return;
            	 }
            	 SwipyUserInput userInput = new SwipyUserInput(Answer.RIGHT);
            	 // send this to minigameController
            	 SendUserInputToController(userInput);
             } 
	    }); 
	    
	    
		// register click for correct answer
 		Button wrongButton = (Button) view.findViewById(R.id.swipy_button_wrong);
 		wrongButton.setOnClickListener(new OnClickListener()
 	    {
              @Override
              public void onClick(View v)
              {
            	 if (m_isIgnoreUserInput) {
             		 return;
             	 }
             	 SwipyUserInput userInput = new SwipyUserInput(Answer.LEFT);
             	 // send this to minigameController
             	 SendUserInputToController(userInput);
              } 
 	    });
 		
 		// register listeners for left and right swipe events
 		// Gesture detection
        m_gestureDetector = new GestureDetector(getActivity(), new MyGestureDetector());
        
        m_gestureListener = new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                return m_gestureDetector.onTouchEvent(event);
            }
        };
        
        view.setOnTouchListener(m_gestureListener);
	}
	
	/*
	private Runnable m_buzzerShowThread = new Runnable() 
	{
		public void run()
		{
			Log.d(TAG, "SwipyMinigameFragment::buzzer timer has expired");
			// switch the button back to neutral buzzer
			ImageView buzzerView = (ImageView) getActivity().findViewById(R.id.buzzer_view);
			buzzerView.setImageResource(R.drawable.ic_neutral_buzzer);
		}
	};
	*/
	
	
	private void AnimateAnswer(Answer answer)
	{		
		Log.d(TAG, "MyGestureDetector::AnimateAnswer");
		int xTo = 0;
		if (answer == Answer.LEFT)
		{
			xTo = -700;
		}
		else
		{
			xTo = 700;
		}
		
		// create the animation object
    	TranslateAnimation animation = new TranslateAnimation(0, xTo, 0, 0);
    	animation.setDuration(100);
    	
    	// set animation listener to update the new equation string
    	animation.setAnimationListener(new AnimationListener() 
		{ 		
    		@Override
            public void onAnimationStart(Animation animation) 
    		{
    			Log.d(TAG, "AnimationListener::onAnimationStart");
    			m_isAnimationPending = true;
    		}

            @Override
            public void onAnimationEnd(Animation animation) 
            {
            	Log.d(TAG, "AnimationListener::onAnimationEnd");
            	m_isAnimationPending = false;
            	// update the new equation string
            	TextView equationView = (TextView) getActivity().findViewById(R.id.equation_text);
            	equationView.setText(m_nextEquationString);
    			equationView.setVisibility(View.VISIBLE);
    			equationView.refreshDrawableState();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {}
    	});
    	
    	TextView equationView = (TextView) getActivity().findViewById(R.id.equation_text);
    	// start animation
    	equationView.startAnimation(animation);
    }
	
	// TODO: Make this class independent with listeners that can register callbacks
	
	// Gesture class to recognize swipe events
	class MyGestureDetector extends SimpleOnGestureListener {
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            try {
                if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH)
                    return false;
                
                if (m_isIgnoreUserInput) {
            		 return false;
            	 }
                
                Log.d(TAG, "MyGestureDetector::onFling");
                if (m_isAnimationPending)
                {
                	// if animation has not finished then ignore any user events
                	return false;
                }
                // right to left swipe
                if(e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) 
                {
                	// Animate for equation text to go left
                	AnimateAnswer(Answer.LEFT);  
                	
                	SwipyUserInput userInput = new SwipyUserInput(Answer.LEFT);
                	// send this to minigameController
                	SendUserInputToController(userInput);
                	
                	              	
                }  
                else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) 
                {
                	// Animate for equation text to go right
	               	AnimateAnswer(Answer.RIGHT);
	               	
                	SwipyUserInput userInput = new SwipyUserInput(Answer.RIGHT);
	               	// send this to minigameController
	               	SendUserInputToController(userInput);	               	
                }
            } catch (Exception e) {
                // nothing
            }
            return false;
        }

        @Override
        public boolean onDown(MotionEvent e) {
              return true;
        }
    }
}
