package com.qualgames.fastfingerz.minigame.model;

import android.util.Log;

public abstract class UserInput {
	
	final static String TAG = "Fast Fingerz";
	
	private int m_timeToAnswer;
	
	public void SetTimeToAnswer(int timeToAnswer)
	{
		Log.d(TAG, "UserInput::SetTimeToAnswer");
		m_timeToAnswer = timeToAnswer;
	}
	
	public int GetTimeToAnswer()
	{
		Log.d(TAG, "Question::GetTimeToAnswer");
		return m_timeToAnswer;
	}

}
