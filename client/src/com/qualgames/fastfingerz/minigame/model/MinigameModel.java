package com.qualgames.fastfingerz.minigame.model;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.qualgames.fastfingerz.minigame.GameType;
import com.qualgames.fastfingerz.minigame.swipy.model.SwipyMinigameModel;
import com.qualgames.fastfingerz.minigame.view.MinigameFragment;

import android.util.Log;

public abstract class MinigameModel {
	
	final static String TAG = "Fast Fingerz";
	final static String GAME_TYPE_STR = "GameType";
	final static String QUESTIONS_STR = "Questions";
	final static String TARGET_POINTS = "TargetPoints";
	final static String POINTS_PER_QUESTION = "PointsPerQuestion";
	
	// these three should be added to game_data sent by server
	final int DEFAULT_GAME_LENGTH_IN_SEC = 30;
	final int DEFAULT_NUMBER_OF_QUESTIONS = 15;
	final static int POINTS_GOAL = 1000;
	
	final static int DEFAULT_MULTIPLIER_TIME_RANGE_MSESC = 2000;
	final static int DEFAULT_MINIMUM_SCORE_FOR_QUESTION = 60;
	final static int DEFAULT_MAXIMUM_SCORE_FOR_QUESTION = 100;
	final static int POINTS_DIV_FACTOR = 17;
	
	
	protected ArrayList<Question> m_questionList;
	protected int m_currentQuestionIndex;
	
	// Below values can be modified by children
	protected int m_gameDurationInSecs;
	protected int m_numberOfQuestions;
	protected int m_multiplierTimeRangeInMSecs; 
	protected int m_minimumScoreForQuestion;
	protected int m_maximumScoreForQuestion;
	protected int m_pointsGoal;
	protected int m_pointsPerQuestion;
	
	// this method will generate an array list of Question objects
	protected abstract void GenerateGameData();
	
	// this method will generate an array list of Question objects based on JSON
	protected abstract void GenerateGameData(JSONArray json) throws JSONException;
	
	// returns the child's GameType
	public abstract GameType GetMyGameType();
	
	// return the child's MinigameFragment instance
	public abstract MinigameFragment GetMyMinigameFragment();
	
	public int GetPointsGoal()
	{
	    return m_pointsGoal;
	}
	
	public int GetPointsPerQuestion()
	{
	    return m_pointsPerQuestion;
	}
	
	// this method will determine whether the time elapsed for correct answer meets the Multiplier specification
	// can be overwritten by children
	/*
	public boolean ShouldMultiplierBeApplied(int timeElapsedForCorrectAnswer)
	{
		Log.d(TAG, "MinigameModel::ShouldMultiplierBeApplied");
		return (timeElapsedForCorrectAnswer <= m_multiplierTimeRangeInMSecs);
	}
	*/
	
	// this method will calculate the score 
	// can be overwritten by children
	public int CalculateScoreForCorrectAnswer(int timeElapsedForCorrectAnswer)
	{
		Log.d(TAG, "MinigameModel::CalculateScoreForCorrectAnswer");
		/*
		 * Keep this in case we want to look at the time it too to answer a question
		if ( timeElapsedForCorrectAnswer <= m_multiplierTimeRangeInMSecs )
		{
		    // if the max threshold has been reached
		    if ( ((m_multiplierTimeRangeInMSecs - timeElapsedForCorrectAnswer)/POINTS_DIV_FACTOR) + m_minimumScoreForQuestion >= m_maximumScoreForQuestion)
		    {
		        return m_maximumScoreForQuestion;
		    }
		    else
		    {
		        return ((m_multiplierTimeRangeInMSecs - timeElapsedForCorrectAnswer)/POINTS_DIV_FACTOR) + m_minimumScoreForQuestion;
		    }
		}
		return m_minimumScoreForQuestion;
		*/
		
		/*
		if (multiplier > 1)
		{
			return (multiplier * m_minimumScoreForQuestion) + ((m_multiplierTimeRangeInMSecs - timeElapsedForCorrectAnswer)/2);
		}
		return m_minimumScoreForQuestion;
		*/
		
		return m_pointsPerQuestion;
	}
		
	
	public MinigameModel()
	{	
		Log.d(TAG, "MinigameModel::MinigameModel");
		Init();
		GenerateGameData();
	}
	
	public MinigameModel(JSONArray json) throws JSONException
	{
		Log.d(TAG, "MinigameModel::MinigameModel with json object");
		Init();
		GenerateGameData(json);
	}
	
	private void Init()
	{
		Log.d(TAG, "MinigameModel::Init");
		// this is the default game length unless explicitly changed by a child
		m_gameDurationInSecs = DEFAULT_GAME_LENGTH_IN_SEC;
		m_currentQuestionIndex = 0;
		m_questionList = new ArrayList<Question>();
		// this is the default number of questions unless explicitly changed by a child
		m_numberOfQuestions = DEFAULT_NUMBER_OF_QUESTIONS;
		m_multiplierTimeRangeInMSecs = DEFAULT_MULTIPLIER_TIME_RANGE_MSESC;
		m_minimumScoreForQuestion = DEFAULT_MINIMUM_SCORE_FOR_QUESTION;
		m_maximumScoreForQuestion = DEFAULT_MAXIMUM_SCORE_FOR_QUESTION;
		//m_pointsGoal = POINTS_GOAL;
	}
	
	public JSONObject EncodeToJSON() throws JSONException
	{
		Log.d(TAG, "MinigameModel::EncodeToJSON");
		JSONObject json = new JSONObject();
		JSONArray jsonArray = new JSONArray();
		
		if ( m_questionList == null )
		{
			Log.d(TAG, "Error: Question list is null!!");
			return null;
		}
		
		json.put(GAME_TYPE_STR, GetMyGameType().value);
		
		for (int i = 0; i < m_questionList.size(); i++)
		{
			jsonArray.put( m_questionList.get(i).EncodeToJSON());
		}
		json.put( QUESTIONS_STR, jsonArray );
		
		return json;
	}
	
	
	public static MinigameModel GenerateMinigameModelBasedOnJSON(JSONObject json) throws JSONException
	{
		Log.d(TAG, "MinigameModel::GetMinigameModelBasedOnJSON");
		// get the game type
		GameType gameType = GameType.values()[json.getInt(GAME_TYPE_STR)];
		MinigameModel model = null;
		
		switch (gameType)
		{
			case SWIPY:
				model = new SwipyMinigameModel(json.getJSONArray(QUESTIONS_STR));
			case MATH_QUESTION:
			case TRIVIA:
				break;
		}
		
		if (model != null) 
		{
		    model.m_pointsGoal = json.getInt(TARGET_POINTS);
		    model.m_pointsPerQuestion = 50; //HACK - json.getInt(POINTS_PER_QUESTION);
		}
		
		return model;
	}
	
	public ArrayList<Question> GetQuestionList()
	{
		return m_questionList;
	}
	
	public void SetGameDuration(int numSecs)
	{
		m_gameDurationInSecs = numSecs;
	}
	
	public int GetGameDuration()
	{
		return m_gameDurationInSecs;
	}
	
	public Question GetNextQuestion()
	{
		Log.d(TAG, "MinigameModel::GetNextQuestion");
		Question question = m_questionList.get(m_currentQuestionIndex);
		m_currentQuestionIndex++;
		
		// sanity check to make sure we still have question left... if not then start at beggining
		if ( m_currentQuestionIndex >= m_questionList.size() )
		{
			// THIS SHOULD NOT HAPPEN... IDEALLY!
			m_currentQuestionIndex = 0;
		}
		
		return question;
	}
}
