package com.qualgames.fastfingerz.minigame.model;

import java.io.Serializable;
import java.util.Random;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcelable;
import android.util.Log;

public abstract class Question implements Serializable{
	
	final static String TAG = "Fast Fingerz";
	final static int WORTH_DOUBLE_CHANCE = 15;
	
	public abstract JSONObject EncodeToJSON() throws JSONException;
	
	public abstract boolean IsUserInputCorrect(UserInput userInput);
	
	public abstract void GenerateQuestionData();
	
	public abstract void GenerateQuestionData(JSONObject json) throws JSONException;
	
	public Question()
	{
		Log.d(TAG, "Question::Question");
		Random r = new Random();
		m_worthDoubleChance = WORTH_DOUBLE_CHANCE;
		m_bIsWorthDouble = r.nextInt(m_worthDoubleChance) == 1;
		
		GenerateQuestionData();
	}
	
	public Question(JSONObject json) throws JSONException
	{
		Log.d(TAG, "Question::Question from JSON");		
		GenerateQuestionData(json);
	}
	
	public boolean IsWorthDouble()
	{
		return m_bIsWorthDouble;
	}
	
	// optional difficulty setting
	protected int m_difficultyLevel;
	
	// small chance that some questions may be worth double
	protected boolean m_bIsWorthDouble;
	// the chances that a question may be worth double
	protected int m_worthDoubleChance;
	
	public void SetDifficultyLevel(int difficulty)
	{
		m_difficultyLevel = difficulty;
	}
}
