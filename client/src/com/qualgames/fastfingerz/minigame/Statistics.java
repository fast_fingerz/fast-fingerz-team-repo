package com.qualgames.fastfingerz.minigame;

import org.json.JSONException;
import org.json.JSONObject;

public class Statistics 
{
    // TODO: perhaps add which question specifically we answered correct or wrong
    public int questions_answered = 0;
    public int questions_answered_correctly = 0;
    // add more stats and complexity if necessary
    
    public static class MessageProtocol
    {
        public static String QUESTIONS_ANSWERED = "qAnswered";
        public static String QUESTIONS_ANSWERED_CORRECTLY = "qAnsweredCorrectly";
    };
    
    public JSONObject EncodeToJSON() throws JSONException
    {
        JSONObject data = new JSONObject();
        data.put(MessageProtocol.QUESTIONS_ANSWERED, questions_answered);
        data.put(MessageProtocol.QUESTIONS_ANSWERED_CORRECTLY, questions_answered_correctly);
        
        return data;
    }
    
    public static Statistics GenerateFromData(JSONObject data) throws JSONException
    {
        Statistics stats = new Statistics();
        stats.questions_answered = data.getInt(MessageProtocol.QUESTIONS_ANSWERED);
        stats.questions_answered_correctly = data.getInt(MessageProtocol.QUESTIONS_ANSWERED_CORRECTLY);
        
        return stats;
    }
}
