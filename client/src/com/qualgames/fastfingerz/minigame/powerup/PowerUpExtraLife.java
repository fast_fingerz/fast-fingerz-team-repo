package com.qualgames.fastfingerz.minigame.powerup;

import com.qualgames.fastfingerz.minigame.controller.MinigameController;

public class PowerUpExtraLife extends PowerUp 
{
    public PowerUpExtraLife(MinigameController minigameController)
    {
        super(minigameController);
        m_powerUpDuration = 0;
    }
    
    @Override
    public PowerUpType GetMyPowerUpType() 
    {
        return PowerUpType.EXTRA_LIFE;
    }

    @Override
    public PowerUpUserInputStatus DetermineAction() 
    {
        return PowerUpUserInputStatus.APPLY_IMMEDIATLY;
    }

    @Override
    public void ApplyToUserInput(PowerUpUserInputData userInputData) 
    {
        userInputData.numLives++;        
    }

    @Override
    protected void ProcessActivate() 
    {
        // Nothing to do here...
    }

}
