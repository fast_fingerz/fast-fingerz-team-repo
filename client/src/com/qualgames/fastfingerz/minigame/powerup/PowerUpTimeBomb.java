package com.qualgames.fastfingerz.minigame.powerup;

import com.qualgames.fastfingerz.minigame.controller.MP_MinigameController;

public class PowerUpTimeBomb extends PowerUp 
{
	private PowerUpType m_myPowerUpType;
	
	public PowerUpTimeBomb(MP_MinigameController minigameController, boolean bIsIncoming) {
		
		super(minigameController);
		
		if( bIsIncoming )
		{
			m_myPowerUpType = PowerUpType.TIME_BOMB_INCOMING;
		}
		else
		{
			m_myPowerUpType = PowerUpType.TIME_BOMB;
		}
		
		// TODO: Do we need to do anything else here?
	}

	@Override
	public PowerUpType GetMyPowerUpType() 
	{
		return m_myPowerUpType;
	}

	@Override
	protected void ProcessActivate() 
	{
		// send message to indicate power up has been activated
	    ((MP_MinigameController)(m_minigameController)).SendGameStatusUpdateMessage(MP_MinigameController.MessageProtocol.POWER_UP, m_myPowerUpType.value);
	}

    @Override
    public PowerUpUserInputStatus DetermineAction() 
    {
        if ( m_myPowerUpType == PowerUpType.TIME_BOMB_INCOMING )
        {
            return PowerUpUserInputStatus.IGNORE_USER_INPUT;
        }
        // if we are the ones that did time bomb then process user input as you normally would
        return PowerUpUserInputStatus.PROCESS_DEFAULT;
    }

    @Override
    public void ApplyToUserInput(PowerUpUserInputData userInputData) 
    {
        // TODO Auto-generated method stub
    }

}
