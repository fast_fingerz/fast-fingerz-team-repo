package com.qualgames.fastfingerz.minigame.powerup;

import com.qualgames.fastfingerz.minigame.controller.MinigameController;

public class PowerUpInvincible extends PowerUp {
	
	public PowerUpInvincible(MinigameController minigameController) {
		super(minigameController);
	}

	@Override
	public PowerUpType GetMyPowerUpType() {
		return PowerUpType.INVINCIBLE;
	}

	@Override
	public PowerUpUserInputStatus DetermineAction() {
		return PowerUpUserInputStatus.APPLY_TO_USER_INPUT;
	}

	@Override
	public void ApplyToUserInput(PowerUpUserInputData userInputData) {
		if (!userInputData.isUserInputCorrect) {
			// the only thing we are doing here is not decreasing lives...
		}
	}

	@Override
	protected void ProcessActivate() {
		// nothing to do here		
	}

}
