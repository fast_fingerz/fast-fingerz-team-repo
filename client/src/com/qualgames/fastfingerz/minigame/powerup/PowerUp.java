package com.qualgames.fastfingerz.minigame.powerup;

import java.util.ArrayList;

import com.qualgames.fastfingerz.minigame.controller.MP_MinigameController;
import com.qualgames.fastfingerz.minigame.controller.MinigameController;
import com.qualgames.fastfingerz.minigame.controller.MinigameController.MinigameModeType;

import android.os.Handler;
import android.util.Log;

public abstract class PowerUp {
	
	final static String TAG = "Fast Fingerz";
	
	public enum PowerUpType
	{
		TIME_BOMB(0),	// for MP only - freezes other players for x num of secs   // ignore any input
		TIME_BOMB_INCOMING(1),
		DISTRACTION_BOMB(2), // for MP only - provides some distraction animation for x num of secs
		DISTRACTION_BOMB_INCOMING(3),
		INVINCIBLE(4), // MP & SP - lives do not decrease if answer is wrong for x num of secs   // process default then don't take lives away
		SPEED_BOOST(5), // MP & SP - increases score by a factor of 1.5 for x num of secs    // process default then multiply by 1.5
		ANSWER_HINT(6),      // SP & MP - highlights the correct answers for a question for y number of question
		EXTRA_LIFE(7);    // MP & SP - Adds an extra life 
		/*
		EXTRA_TIME,		// SP only - extends the game by x num of secs
		COMBO_ENHANCER,	// SP & MP - combo does not reset for a wrong answer and allows extra time for combo score to count (for x number of questions, or y num of secs)
		
		SLOW_DOWN_TIME,	// SP only - time slows down but calculations are normal... increases points and combo... TODO: maybe keep this and get rid of COMBO_ENHANCER
		ANSWER_FRENZY,	// SP & MP - all answers are counted as correct
		DOUBLE_VALUE	// SP & MP - all correct answers are worth double what they normally would be
		*/
		
		public int value;
		private PowerUpType(int value) {
			this.value = value;
		}
	}
	
	public enum PowerUpUserInputStatus
	{
		APPLY_TO_USER_INPUT,		// power up should be called after user input has been evaluated
		APPLY_IMMEDIATLY,           // power up should be applied immediately to change state regardless of user input
		IGNORE_USER_INPUT,			// power up has determined that user input should be ignored
		PROCESS_DEFAULT				// either power up is not active or does not affect user input
	}
	
	private final static int DEFAULT_POWER_UP_DURATION_TIME = 3000;
	protected boolean m_bIsActive = false;
	protected int m_powerUpDuration;
	// minigame controller instance to send messages to
	protected MinigameController m_minigameController;
	
	// Handler for the power up timer
	private Handler m_powerUpTimerHandler = new Handler();
	
	public boolean IsActive()
	{
		return m_bIsActive;
	}
	
	public int GetDuration()
	{
		return m_powerUpDuration;
	}
	
	public PowerUp(MinigameController minigameController)
	{
		m_minigameController = minigameController;
		m_powerUpDuration = DEFAULT_POWER_UP_DURATION_TIME;
	}
	
	public static PowerUp GetPowerUpInstance(PowerUpType powerUpType, MinigameController minigameController)
	{
		PowerUp powerUp = null;
		switch (powerUpType)
		{
		case ANSWER_HINT:
			//powerUp = new PowerUpAnswerHint(minigameController);
			break;
		case INVINCIBLE:
			powerUp = new PowerUpInvincible(minigameController);
			break;
		case DISTRACTION_BOMB:
			//powerUp = new PowerUpDistractionBomb(minigameController, false);
			break;
		case DISTRACTION_BOMB_INCOMING:
			//powerUp = new PowerUpDistractionBomb(minigameController, true);
			break;
		case SPEED_BOOST:
			powerUp = new PowerUpSpeedBoost(minigameController);
			break;
		case TIME_BOMB:
			powerUp = new PowerUpTimeBomb((MP_MinigameController)minigameController, false);
			break;
		case TIME_BOMB_INCOMING:
			powerUp = new PowerUpTimeBomb((MP_MinigameController)minigameController, true);
			break;
		case EXTRA_LIFE:
		    powerUp = new PowerUpExtraLife(minigameController);
		    break;			
		}
		
		return powerUp;
	}
	
	// TODO: This should be removed and replaced by GetAvailablePowerUps
	// validates that power up is applicable to the minigame type
	public static boolean IsPowerUpValid(PowerUpType powerUp, MinigameModeType minigameModeType)
	{
		if ( powerUp == PowerUpType.ANSWER_HINT || powerUp == PowerUpType.SPEED_BOOST || powerUp == PowerUpType.INVINCIBLE )
			return true;
		
		if ( (powerUp == PowerUpType.DISTRACTION_BOMB || powerUp == PowerUpType.TIME_BOMB) && 
			  minigameModeType == MinigameModeType.SINGLE_PLAYER )
			  return false;
			  
		return true;
	}
	
	// returns the power ups that are valid for this mode
	public static ArrayList<PowerUpType> GetAvailablePowerUps(MinigameModeType minigameModeType) 
	{
	    ArrayList<PowerUpType> powerUpTypes = new ArrayList<PowerUpType>();
	    if (minigameModeType == MinigameModeType.MULTI_PLAYER || minigameModeType == MinigameModeType.HEAD_TO_HEAD)
	    {
	        powerUpTypes.add(PowerUpType.EXTRA_LIFE);
	        powerUpTypes.add(PowerUpType.TIME_BOMB);
	        powerUpTypes.add(PowerUpType.SPEED_BOOST);
	        powerUpTypes.add(PowerUpType.DISTRACTION_BOMB);
	    }
	    
	    return powerUpTypes;
	}
	
	public void Activate()
	{
		m_bIsActive = true;
		StartPowerUpTimer();
		ProcessActivate();
	}
	
	protected void StartPowerUpTimer()
	{
		m_powerUpTimerHandler.postDelayed(m_powerUpTimerThread, m_powerUpDuration);
	}	
	
	private Runnable m_powerUpTimerThread = new Runnable() 
	{
		public void run()
		{
			Log.d(TAG, "PowerUp:: power up timer has expired... deactivate");
			
			// handle the power up timer expiration event
			ProcessDeActivate();
		}
	};
	
	protected void ProcessDeActivate()
	{
		m_bIsActive = false;
		// notify minigame controller
		m_minigameController.PowerUpDeactivated(GetMyPowerUpType());
	}
	
	@Override
	public boolean equals(Object other)
	{
	    return ( GetMyPowerUpType() == ((PowerUp)other).GetMyPowerUpType() );
	}
	
	
	// abstract methods for children to overwrite
	public abstract PowerUpType GetMyPowerUpType();
	
	// this will determine what action power up will need to preform
	public abstract PowerUpUserInputStatus DetermineAction();
	
	public abstract void ApplyToUserInput(PowerUpUserInputData userInputData);
	
	// this may only need to be implemented by some children (ex. SLOW_DOWN_TIME)
	protected abstract void ProcessActivate();	
	
}
