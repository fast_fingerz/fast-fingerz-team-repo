package com.qualgames.fastfingerz.minigame.powerup;

import com.qualgames.fastfingerz.minigame.model.MinigameModel;

// the whole point of this class is so that input params can be changed by PowerUp class passed from MinigameController
public class PowerUpUserInputData {

	public int score;
	public int numLives;
	public boolean isUserInputCorrect;
}
