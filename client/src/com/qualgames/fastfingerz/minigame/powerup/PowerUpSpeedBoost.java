package com.qualgames.fastfingerz.minigame.powerup;

import com.qualgames.fastfingerz.minigame.controller.MinigameController;

public class PowerUpSpeedBoost extends PowerUp {
	
	private static final double BOOST_FACTOR = 1.5;
	
	public PowerUpSpeedBoost(MinigameController minigameController)
    {
        super(minigameController);
    }

	@Override
	public PowerUpType GetMyPowerUpType() {
		return PowerUpType.SPEED_BOOST;
	}

	@Override
	public PowerUpUserInputStatus DetermineAction() {
		return PowerUpUserInputStatus.APPLY_TO_USER_INPUT;
	}

	@Override
	public void ApplyToUserInput(PowerUpUserInputData userInputData) {
		if (userInputData.isUserInputCorrect) {
			userInputData.score *= BOOST_FACTOR;		
		}
		else {
			userInputData.numLives--;
		}
	}

	@Override
	protected void ProcessActivate() {
		// nothing to do
	}

}
