package com.qualgames.fastfingerz.minigame.view;

import java.util.ArrayList;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.qualgames.fastfingerz.R;
import com.qualgames.fastfingerz.mp_session.Participant;
import com.qualgames.fastfingerz.mp_session.UserScores;

public class RaceTrackScoreView extends View {
    
    final static String TAG = "Fast Fingerz " + RaceTrackScoreView.class.getSimpleName();

    private Context mContext;

    // constants for telling rotation of a progress
    public static final int CLOCKWISE = 0;
    public static final int ANTICLOCKWISE = 1;

    // constants to tell from which position we want to start the progress
    public static final int LEFT = 0;
    public static final int TOP = 1;
    public static final int RIGHT = 2;
    public static final int BOTTOM = 3;
    
    private enum RaceTrackQuadrant {
        TOP_RIGHT_LINE,
        RIGHT_SEMICRICLE,
        BOTTOM_LINE,
        LEFT_SEMICRICLE,
        TOP_LEFT_LINE;
    };
    
    // BEGIN HACK
    private static final float CIRCLE_PERCENTAGE = 0.6f;
    private static final float LINE_PERCENTAGE = 1 - CIRCLE_PERCENTAGE;
    // actually the bigger this value the smaller the circle
    private static final int HEIGHT_OFFSET = 110;
    
    private float m_lesserHalfH = 0.0f;
    // END HACK

    // coloring the traversal
    private float mTraverse = 0.0f;

    // Save instance of canvas to draw anywhere in the file
    private Canvas mCanvas;

    // Current progress of progressbar in percentage
    private float m_currentProgress;
    
    // the points goal of the game
    private int m_pointsGoal;

    // Maximus progress of progressbar
    private float mMaxProgress;

    // Thickness of a progressbar
    private int mThickness;

    // RectF ecnlosing the circular progressbar
    private final RectF m_rightSemiCircleProgress = new RectF();
    private final RectF m_rightSemiCircleUnprogress = new RectF();
    
    private final RectF m_leftSemiCircleProgress = new RectF();
    private final RectF m_leftSemiCircleUnprogress = new RectF();
    
    // the bar starts in the middle of the top line so we need to separate it into two pieces
    private float m_topLineLeftProgress[] = new float[] {0.0f, 0.0f, 0.0f, 0.0f};
    private float m_topLineRightProgress[] = new float[] {0.0f, 0.0f, 0.0f, 0.0f};
    private float m_topLineLeftUnprogress[] = new float[] {0.0f, 0.0f, 0.0f, 0.0f};
    private float m_topLineRightUnprogress[] = new float[] {0.0f, 0.0f, 0.0f, 0.0f};
    private float m_bottomLineProgress[] = new float[] {0.0f, 0.0f, 0.0f, 0.0f};
    private float m_bottomLineUnprogress[] = new float[] {0.0f, 0.0f, 0.0f, 0.0f};
    
    private float m_rightSemiCirclePercentage = 0.0f;
    private float m_leftSemiCirclePercentage = 0.0f;
    private float m_topRightLinePercentage = 0.0f;
    private float m_topLeftLinePercentage = 0.0f;
    private float m_bottomLinePercentage = 0.0f;
    
    private float m_straightTrackLength = 0.0f;
    
    // for drawing all the scores
    private UserScores m_userScores = null;

    // color of progressbar, which has traversed
    private int mProgressbarColor;

    // default (normal) color of progressbar, which has not made any progress
    private int mProgressbarBackgroundColor;

    // integer value indicating at which position the progress should start
    // 0 : left
    // 1 : top
    // 2 : right
    // 3 : bottom
    private int mStartAt;

    // rotation value
    private int mRotation;

    // used to paint the progress of progressbar
    private Paint mProgressbarPaint;

    // used to paint un-progress part of progressbar
    private Paint mUnProgressbarPaint;

    public RaceTrackScoreView(final Context context) {
        this(context, null);
        Log.d(TAG, "RaceTrackScoreView");
    }

    public RaceTrackScoreView(final Context context, final AttributeSet attrs) {
        this(context, attrs, R.attr.circularProgressbarStyle);
        Log.d(TAG, "RaceTrackScoreView");
    }

    public RaceTrackScoreView(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        final TypedArray attributes = mContext.obtainStyledAttributes(attrs, R.styleable.SimpleCircularProgressbar , defStyleAttr, 0);

        if(attributes != null){
            try {
                mMaxProgress = attributes.getFloat(R.styleable.SimpleCircularProgressbar_maxProgress, 100.0f);
                m_currentProgress = attributes.getFloat(R.styleable.SimpleCircularProgressbar_progress, 0.0f);
                mStartAt = attributes.getInteger(R.styleable.SimpleCircularProgressbar_startAt, 1);     // default will be top
                mRotation = attributes.getInteger(R.styleable.SimpleCircularProgressbar_rotation, 0);
                mProgressbarColor = attributes.getColor(R.styleable.SimpleCircularProgressbar_progressColor, Color.BLACK);
                mProgressbarBackgroundColor = attributes.getColor(R.styleable.SimpleCircularProgressbar_backgroundColor, Color.GRAY);
                mThickness = attributes.getInteger(R.styleable.SimpleCircularProgressbar_thickness, 15);
            }
            finally {
                attributes.recycle();
            }

            init();
        }
        Log.d(TAG, "RaceTrackScoreView");
    }
    
    public void SetPointsGoal(int pointsGoal)
    {
        m_pointsGoal = pointsGoal;
        Log.d(TAG, "pointsGoal is " + pointsGoal);
    }

    /**
        This code block does main initialization of the view related stuff
     */
    private void init(){
        mProgressbarPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mProgressbarPaint.setColor(mProgressbarColor);
        mUnProgressbarPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mUnProgressbarPaint.setColor(mProgressbarBackgroundColor);
        calculateProgress();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int defH = getDefaultSize(getSuggestedMinimumHeight()+getPaddingBottom()+getPaddingTop(),
                heightMeasureSpec);
        int defW = getDefaultSize(getSuggestedMinimumWidth()+getPaddingLeft()+getPaddingRight(),
                widthMeasureSpec);

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        if( (widthMode == MeasureSpec.UNSPECIFIED || widthMode == MeasureSpec.AT_MOST)){
            defW = 75;
        }
        if( (heightMode == MeasureSpec.UNSPECIFIED || heightMode == MeasureSpec.AT_MOST)){
            defH = 75;
        }
        setMeasuredDimension(defW + 60, defH);
        if(defH < defW) {
            
            // height is small
            final float pHalfH = defH/2f;
            final float pHalfW = defW/2f;
            defH = defH - 20 - HEIGHT_OFFSET;
            final float halfH = defH/2f;
            
            Log.d(TAG, "defH " + defH + " halfH " + halfH);
            
            // this is right point - left point
            final float diameter = 2 * halfH; //(pHalfW + halfH) - (pHalfW - halfH);
            final float radius = diameter / 2;
            // figure out the circumference of the two semi circles combined (or one full circle)
            final float circumference = (float)Math.PI * radius * 2;
            // determine the length of the straight track based on percentage that circle circumferences will take up
            m_straightTrackLength = ((circumference/CIRCLE_PERCENTAGE) * ( 1 - CIRCLE_PERCENTAGE ))/2;
            
            float left = pHalfW-halfH + m_straightTrackLength/2;
            float top = pHalfH - halfH;
            float right = pHalfW+halfH + m_straightTrackLength/2;
            float bottom = pHalfH + halfH;
            
            //setMeasuredDimension(defW + 60 + (int)straightTrackLength + 100, defH + 20 + HEIGHT_OFFSET);
            
            Log.d(TAG, "straightTrackLength " + m_straightTrackLength + " circumference " + circumference);
       
            m_rightSemiCircleProgress.set( (int)left, (int)top, (int)right, (int)bottom );
            m_rightSemiCircleUnprogress.set((int)left, (int)top, (int)right, (int)bottom); 
            
            left = pHalfW-halfH - m_straightTrackLength/2;
            right = pHalfW+halfH - m_straightTrackLength/2;
            
            m_leftSemiCircleProgress.set( (int)left, (int)top, (int)right, (int)bottom );
            m_leftSemiCircleUnprogress.set((int)left, (int)top, (int)right, (int)bottom); 
            
            // left x - top of left semi circle 
            m_topLineLeftUnprogress[0] = m_leftSemiCircleProgress.left + radius;
            // left y - top of left semi circle
            m_topLineLeftUnprogress[1] = m_leftSemiCircleProgress.top;
            // right x - top of right semi circle
            m_topLineLeftUnprogress[2] = m_rightSemiCircleProgress.left + radius - (m_straightTrackLength/2);
            // right y - top of right semi circle
            m_topLineLeftUnprogress[3] = m_rightSemiCircleProgress.top;
            
            // left x - top of left semi circle 
            m_topLineRightUnprogress[0] = m_leftSemiCircleProgress.left + radius + (m_straightTrackLength/2);
            // left y - top of left semi circle
            m_topLineRightUnprogress[1] = m_leftSemiCircleProgress.top;
            // right x - top of right semi circle
            m_topLineRightUnprogress[2] = m_rightSemiCircleProgress.left + radius;
            // right y - top of right semi circle
            m_topLineRightUnprogress[3] = m_rightSemiCircleProgress.top;
            
            // left x - bottom of left semi circle
            m_bottomLineUnprogress[0] = m_leftSemiCircleProgress.left + radius;
            // left y - bottom of left semi circle
            m_bottomLineUnprogress[1] = m_leftSemiCircleProgress.bottom;
            // right x - bottom of right semi circle
            m_bottomLineUnprogress[2] = m_rightSemiCircleProgress.left + radius;
            // right y - bottom of right semi circle
            m_bottomLineUnprogress[3] = m_rightSemiCircleProgress.bottom;
            
            
        }
        else {
            
            Log.d(TAG, "Width is small");
            // width is small
            final float pHalfH = defH/2f;
            final float pHalfW = defW/2f;
            defW = defW - 20;
            final float halfW = defW/2f;
            m_rightSemiCircleProgress.set( pHalfW-halfW + 30, pHalfH-halfW, pHalfW+halfW + 30, pHalfH+halfW);
            m_rightSemiCircleUnprogress.set(pHalfW-halfW + 30, pHalfH-halfW, pHalfW+halfW + 30, pHalfH+halfW);
        }
        mProgressbarPaint.setStrokeWidth(mThickness);
        mProgressbarPaint.setStyle(Paint.Style.STROKE);

        mUnProgressbarPaint.setStrokeWidth(mThickness);
        mUnProgressbarPaint.setStyle(Paint.Style.STROKE);
    }

    @Override
    protected void onDraw(final Canvas canvas) {
        Log.d(TAG, "onDraw");
        mCanvas = canvas;

        switch (mRotation){
            case 0:
                // clockwise
                printClockwise(canvas);
                break;

            case 1:
                // anti clockwise
                printAntiClockwise(canvas);
                break;
        }
    }

    private void printClockwise(final Canvas canvas){
        switch (mStartAt) {
            case 0:
                // left
                canvas.drawArc(m_rightSemiCircleProgress, 180, mTraverse, false, mProgressbarPaint);
                canvas.drawArc(m_rightSemiCircleUnprogress, 180, -((360-mTraverse)), false, mUnProgressbarPaint);
                break;

            case 1:
                // top
                // draw the unpogress objects
                canvas.drawArc(m_rightSemiCircleUnprogress, 270, 180, false, mUnProgressbarPaint);
                canvas.drawArc(m_leftSemiCircleUnprogress, 90, 180, false, mUnProgressbarPaint);
                canvas.drawLine((int)m_topLineLeftUnprogress[0], (int)m_topLineLeftUnprogress[1], (int)m_topLineLeftUnprogress[2], (int)m_topLineLeftUnprogress[3], mUnProgressbarPaint);
                canvas.drawLine((int)m_topLineRightUnprogress[0], (int)m_topLineRightUnprogress[1], (int)m_topLineRightUnprogress[2], (int)m_topLineRightUnprogress[3], mUnProgressbarPaint);
                canvas.drawLine((int)m_bottomLineUnprogress[0], (int)m_bottomLineUnprogress[1], (int)m_bottomLineUnprogress[2], (int)m_bottomLineUnprogress[3], mUnProgressbarPaint);
                
                // draw the progress objects
                canvas.drawLine((int)m_topLineLeftProgress[0], (int)m_topLineLeftProgress[1], (int)m_topLineLeftProgress[2], (int)m_topLineLeftProgress[3], mProgressbarPaint);
                canvas.drawLine((int)m_topLineRightProgress[0], (int)m_topLineRightProgress[1], (int)m_topLineRightProgress[2], (int)m_topLineRightProgress[3], mProgressbarPaint);
                canvas.drawLine((int)m_bottomLineProgress[0], (int)m_bottomLineProgress[1], (int)m_bottomLineProgress[2], (int)m_bottomLineProgress[3], mProgressbarPaint);
                canvas.drawArc(m_rightSemiCircleProgress, 270, 180 * m_rightSemiCirclePercentage, false, mProgressbarPaint);
                canvas.drawArc(m_leftSemiCircleProgress, 90, 180 * m_leftSemiCirclePercentage, false, mProgressbarPaint);
                 
                // Draw the positions of the users based on score!
                DrawUserScores(canvas);

                break;

            case 2:
                // right
                canvas.drawArc(m_rightSemiCircleProgress, 0, mTraverse, false, mProgressbarPaint);
                canvas.drawArc(m_rightSemiCircleUnprogress, 0, -((360-mTraverse)), false, mUnProgressbarPaint);
                break;

            case 3:
                // bottom
                canvas.drawArc(m_rightSemiCircleProgress, 90, mTraverse, false, mProgressbarPaint);
                canvas.drawArc(m_rightSemiCircleUnprogress, 90, -((360-mTraverse)), false, mUnProgressbarPaint);
                break;
        }
        
    }

    private void printAntiClockwise(final Canvas canvas){
        switch (mStartAt) {
            case 0:
                // left
                canvas.drawArc(m_rightSemiCircleProgress, 180, 360, false, mProgressbarPaint);
                canvas.drawArc(m_rightSemiCircleUnprogress, 180, (360-mTraverse), false, mUnProgressbarPaint);
                break;

            case 1:
                // top
                canvas.drawArc(m_rightSemiCircleProgress, 270, 360, false, mProgressbarPaint);
                canvas.drawArc(m_rightSemiCircleUnprogress, 270, (360-mTraverse), false, mUnProgressbarPaint);
                break;

            case 2:
                // right
                canvas.drawArc(m_rightSemiCircleProgress, 0, 360, false, mProgressbarPaint);
                canvas.drawArc(m_rightSemiCircleUnprogress, 0, (360-mTraverse), false, mUnProgressbarPaint);
                break;

            case 3:
                // bottom
                canvas.drawArc(m_rightSemiCircleProgress, 90, 360, false, mProgressbarPaint);
                canvas.drawArc(m_rightSemiCircleUnprogress, 90, (360-mTraverse), false, mUnProgressbarPaint);
                break;
        }
    }
    
    private void DrawUserScores(Canvas canvas)
    {
        // get the points of the user in 1st place
        ArrayList<Participant> userScores = m_userScores.GetSortedUserScoreList();
        int topUserScore = userScores.get(0).gameScore;
        int myScore = 0;
        
        // draw all users on race track based on top score
        for (Participant p : userScores)
        {
            Log.d(TAG, "gameScore - " + p.gameScore);
            if (p.isSelf)
            {   
                myScore = p.gameScore;
            }
            else
            {
                DrawUserScore(canvas, topUserScore, p.gameScore);
            }
        }
        
        // now draw our UserScore
        DrawUserScore(canvas, topUserScore, myScore, m_userScores.GetMyRank(), true );
        
    }
    
    private void DrawUserScore(Canvas canvas, int topScore, int userScore)
    {
        DrawUserScore(canvas, topScore, userScore, 0, false);
    }
    
    // TODO: Somehow make a smooth animation of when a user's score is updated
    private void DrawUserScore(Canvas canvas, int topScore, int userScore, int rank, boolean bIsMyUser)
    {
        Log.d(TAG, "DrawUserScore");
        float userProgress = 0.0f;
        /*
        if ( topScore > 0 && m_currentProgress > 0)
        {   
            Log.d(TAG, "userScore " + userScore + " topScore " + topScore);
            userProgress = m_currentProgress * (float)((float)userScore/(float)topScore);
        }
        */
          
        if (userScore > 0 && userScore < m_pointsGoal) 
        {
            userProgress = (float)((float)userScore/(float)m_pointsGoal);
        }
        
        Log.d(TAG, "userProgress " + userProgress);
        
        float cirRadius = 20;
        Paint cirPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        cirPaint.setColor(Color.BLACK);
        
        Paint textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        textPaint.setColor(Color.WHITE);
        textPaint.setTextSize(50);
        textPaint.setTextAlign(Align.CENTER);
        
        if (bIsMyUser)
        {
            cirPaint.setColor(Color.GREEN);
            cirRadius = 40;
        }
        
        float x = 0, y = 0;
        
        RaceTrackQuadrant quadrant = GetRaceTrackQuadrant(userProgress);
        float percentageProgress = GetPercentageForProgress(userProgress, quadrant);
        
        Log.d(TAG, "quadrant " + quadrant);
        Log.d(TAG, "percentageProgress " + percentageProgress);
        
        switch ( quadrant )
        {
            case TOP_RIGHT_LINE:
            {
                float topRightLinePercentage = percentageProgress;
                // x is variable
                x = m_topLineRightUnprogress[2] - ((1 - topRightLinePercentage) * (m_straightTrackLength/2));
                y = m_topLineRightUnprogress[3];
                
                break;
            }
            case RIGHT_SEMICRICLE:
            {
                float rightSemiCirclePercentage = percentageProgress;
                
                // need to do fancy trig to find coordinates of x and y based on percentage
                float angle = (rightSemiCirclePercentage * 180) - (float)90;
                float radius = m_rightSemiCircleUnprogress.width()/2;
                //x = r cos (t) + offset
                x = (float) (radius * Math.cos(Math.toRadians(angle)) + m_rightSemiCircleUnprogress.left + radius);
                //y = r sin (t) + offset
                y = (float) (radius * Math.sin(Math.toRadians(angle)) + m_rightSemiCircleUnprogress.top + radius);
                
                Log.d( TAG, "angle " + angle + " radius " + radius + " x " + x + " y " + y );
                
                break;
            }
            case BOTTOM_LINE:
            {
                float bottomLinePercentage = percentageProgress;
                
                // x is variable
                x = m_bottomLineUnprogress[0] + ((1 - bottomLinePercentage) * (m_straightTrackLength));
                y = m_bottomLineUnprogress[1];
                
                break;
            }
            case LEFT_SEMICRICLE:     
            {
                float leftSemiCirclePercentage = percentageProgress;
                
                // need to do fancy trig to find coordinates of x and y based on percentage
                float angle = (leftSemiCirclePercentage * 180) + (float)90;
                float radius = m_leftSemiCircleUnprogress.width()/2;
                //x = r cos (t) + offset
                x = (float) (radius * Math.cos(Math.toRadians(angle)) + m_leftSemiCircleUnprogress.left + radius);
                //y = r sin (t) + offset
                y = (float) (radius * Math.sin(Math.toRadians(angle)) + m_leftSemiCircleUnprogress.top + radius);
                
                Log.d( TAG, "angle " + angle + " radius " + radius + " x " + x + " y " + y );
                
                break;
            }
            case TOP_LEFT_LINE:
            {
                float topLeftLinePercentage = percentageProgress;
                
                // x is variable
                x = m_topLineLeftUnprogress[2] - ((1 - topLeftLinePercentage) * (m_straightTrackLength/2));
                y = m_topLineLeftUnprogress[3];  
                
                break;
            }
        }
        
        canvas.drawCircle(x, y, cirRadius, cirPaint);
        if ( bIsMyUser )
        {
            // draw the rank
            canvas.drawText(Integer.toString(rank), x, y, textPaint);
        }
    }

    /**
     * Sets the passed progress to current progress and
     * updates the Progressbar
     * @param progress current value traced by progressbar
     */
    public void setProgress(final float progress, UserScores userScores) 
    {
        Log.d(TAG, "setProgress");
        m_currentProgress = progress;
        m_userScores = userScores;
        calculateProgress();
        invalidate();
    }

    /**
    * Sets the maximum progress for progress bar
    * @param maxProgress maximum value of progress
    * @see : setProgress()
    */
    public void setMaxProgress(final float maxProgress){
        mMaxProgress = maxProgress;
    }

    /**
     * Sets the rotation of progressbar
     * @param rotation 0 for clockwise, 1 for anti-clockwise
     */
    public void setRotation(final int rotation){
        switch (rotation){
            case 1:
                mRotation = rotation;
                break;

            default:
                mRotation = 0;
                break;
        }
    }

    /**
     * Sets the starting point of progress
     * whether it is top, bottom , right or left
     * @param from position from which it progress should start
     */
    public void startFrom(final int from){
        switch(from){
            case 0:
                //left
                mStartAt = 0;
                break;
            case 2:
                // right
                mStartAt = 2;
                break;
            case 3:
                // bottom
                mStartAt = 3;
                break;
            default:
                mStartAt = 1;
                break;

        }
    }

    /**
     * Sets thickness of the ring
     * @param thickness it will specify size of ring
     */
    public void setThickness(final int thickness){
        mThickness = thickness;
    }

    /**
     * Sets color which will be highlighted.
     * It will be actual progress color
     * @param color color which will be color which will be set to progressed bar
     */
    public void setProgressColor(final int color){
        mProgressbarColor = color;
        mProgressbarPaint.setColor(mProgressbarColor);
    }

    /**
     * Sets color which won't be highlighted.
     * It will be actual un-progressed color
     * @param color color which will be set to un-progressed bar
     */
    public void setSecondaryColor(final int color){
        mProgressbarBackgroundColor = color;
        mUnProgressbarPaint.setColor(mProgressbarBackgroundColor);
    }

    /**
     * Calculated perfect progress according to max and current progress
     */
    private void calculateProgress(){
        
        if ( m_currentProgress < 0 )
        {
            Log.e(TAG, "Current Progress Percentage < 0!!!!");
            return;
        }
        
        // initialize all to 0 just in case
        m_topRightLinePercentage = m_topLeftLinePercentage = m_bottomLinePercentage = m_leftSemiCirclePercentage = m_rightSemiCirclePercentage = 0;
        
        // need to figure out progress of each component based on total progress percentage
        RaceTrackQuadrant quadrant = GetRaceTrackQuadrant(m_currentProgress);
        float percentageProgress = GetPercentageForProgress(m_currentProgress, quadrant);
        switch (quadrant)
        {
            case TOP_RIGHT_LINE:
            {
                // only top right line needs filling
                m_topRightLinePercentage = percentageProgress;
                break;
            }
            case RIGHT_SEMICRICLE:
            {
                // top right line and right semi circle need filling
                m_topRightLinePercentage = 1;
                m_rightSemiCirclePercentage = percentageProgress;
                break;
            }
            case BOTTOM_LINE:
            {
                // top right line, right semi circle, and bottom line need filling
                m_topRightLinePercentage = 1;
                m_rightSemiCirclePercentage = 1;
                m_bottomLinePercentage = percentageProgress;
                break;
            }
            case LEFT_SEMICRICLE:
            {
                m_topRightLinePercentage = 1;
                m_rightSemiCirclePercentage = 1;
                m_bottomLinePercentage = 1;
                m_leftSemiCirclePercentage = percentageProgress;
                break;
            }
            case TOP_LEFT_LINE:
            {
                m_topRightLinePercentage = 1;
                m_rightSemiCirclePercentage = 1;
                m_bottomLinePercentage = 1;
                m_leftSemiCirclePercentage = 1;
                m_topLeftLinePercentage = percentageProgress;
                break;
            }
        }
        
        CalculateProgressBasedOnPercentage();
        
        mTraverse = (m_currentProgress*360)/mMaxProgress;
        Log.d(TAG, "calculateProgress mTraverse " + mTraverse);
    }
    
    
    private void CalculateProgressBasedOnPercentage()
    {
        if ( m_topLineRightUnprogress[2] <= 0 )
        {
            Log.d(TAG, "We are not yet ready to draw  the progress");
            return;
        }
        // left x stays same
        m_topLineRightProgress[0] = m_topLineRightUnprogress[0];
        // left y stays same
        m_topLineRightProgress[1] = m_topLineRightUnprogress[1];
        // right x is variable
        m_topLineRightProgress[2] = m_topLineRightUnprogress[2] - ((1 - m_topRightLinePercentage) * (m_straightTrackLength/2));
        // right y stays same
        m_topLineRightProgress[3] = m_topLineRightUnprogress[3];
        
        // left x is variable
        m_bottomLineProgress[0] = m_bottomLineUnprogress[0] + ((1 - m_bottomLinePercentage) * (m_straightTrackLength));
        // left y stays same
        m_bottomLineProgress[1] = m_bottomLineUnprogress[1];
        // right x stays same
        m_bottomLineProgress[2] = m_bottomLineUnprogress[2];
        // right y stays same
        m_bottomLineProgress[3] = m_bottomLineUnprogress[3];
        
        // left x stays same
        m_topLineLeftProgress[0] = m_topLineLeftUnprogress[0];
        // left y stays same
        m_topLineLeftProgress[1] = m_topLineLeftUnprogress[1];
        // right x is variable
        m_topLineLeftProgress[2] = m_topLineLeftUnprogress[2] - ((1 - m_topLeftLinePercentage) * (m_straightTrackLength/2));
        // right y stays same
        m_topLineLeftProgress[3] = m_topLineLeftUnprogress[3];
    }
    
    private RaceTrackQuadrant GetRaceTrackQuadrant(float progress)
    {
        RaceTrackQuadrant quadrant = RaceTrackQuadrant.TOP_RIGHT_LINE;
        
        if ( progress <= LINE_PERCENTAGE/4 )
        {
            // only top right line needs filling
            quadrant = RaceTrackQuadrant.TOP_RIGHT_LINE;            
        }
        else if ( progress <= (LINE_PERCENTAGE/4 + CIRCLE_PERCENTAGE/2) )
        {
            // top right line and right semi circle need filling
            quadrant = RaceTrackQuadrant.RIGHT_SEMICRICLE;
        }
        else if ( progress <= (LINE_PERCENTAGE/4 + CIRCLE_PERCENTAGE/2 + LINE_PERCENTAGE/2) )
        {
            // top right line, right semi circle, and bottom line need filling
            quadrant = RaceTrackQuadrant.BOTTOM_LINE;
        }
        else if ( progress <= (LINE_PERCENTAGE/4 + CIRCLE_PERCENTAGE/2 + LINE_PERCENTAGE/2 + CIRCLE_PERCENTAGE/2) )
        {
            quadrant = RaceTrackQuadrant.LEFT_SEMICRICLE;
        }
        else if ( progress <= (LINE_PERCENTAGE/4 + CIRCLE_PERCENTAGE/2 + LINE_PERCENTAGE/2 + CIRCLE_PERCENTAGE/2 + LINE_PERCENTAGE/4))
        {
           quadrant = RaceTrackQuadrant.TOP_LEFT_LINE;
        }
        else // ideally we should never here but this means that all lines are full
        {
            quadrant = RaceTrackQuadrant.TOP_LEFT_LINE;
        }
        
        return quadrant;
    }
    
    private float GetPercentageForProgress(float progress, RaceTrackQuadrant quadrant)
    {
        float percentage = 0.0f;
        switch (quadrant)
        {
            case TOP_RIGHT_LINE:
            {
                // only top right line needs filling
                percentage = progress * (1/(LINE_PERCENTAGE/4));
                break;
            }
            case RIGHT_SEMICRICLE:
            {
                // top right line and right semi circle need filling
                percentage = (progress - LINE_PERCENTAGE/4) * (1/(CIRCLE_PERCENTAGE/2));
                break;
            }
            case BOTTOM_LINE:
            {
                // top right line, right semi circle, and bottom line need filling
                percentage = (progress - LINE_PERCENTAGE/4 - CIRCLE_PERCENTAGE/2) * (1/(LINE_PERCENTAGE/2));
                break;
            }
            case LEFT_SEMICRICLE:
            {
                percentage = (progress - LINE_PERCENTAGE/4 - CIRCLE_PERCENTAGE/2 - LINE_PERCENTAGE/2 ) *
                                             ( 1/(CIRCLE_PERCENTAGE/2) );
                break;
            }
            case TOP_LEFT_LINE:
            {
                percentage = ( progress - LINE_PERCENTAGE/4 - CIRCLE_PERCENTAGE/2 - LINE_PERCENTAGE/2 - CIRCLE_PERCENTAGE/2 ) *
                                          ( 1/(LINE_PERCENTAGE/4) );
                break;
            }
        }
        
        return percentage;
    }
}
