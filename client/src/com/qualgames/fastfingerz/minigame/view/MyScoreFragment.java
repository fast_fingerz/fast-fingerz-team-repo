package com.qualgames.fastfingerz.minigame.view;

import java.io.Serializable;

import com.qualgames.fastfingerz.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

// this fragment will show our current score and number of lives left
public class MyScoreFragment extends Fragment implements Serializable {
    
    final static String TAG = "Fast Fingerz " + MyScoreFragment.class.getSimpleName();
    
    private int m_maxLives;
    
    public MyScoreFragment(int maxLives) 
    {
        m_maxLives = maxLives;
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, 
            Bundle savedInstanceState) {
        
        Log.d(TAG, "onCreateView");
        
        return inflater.inflate(R.layout.my_score_fragment_layout, container, false);
    }
    
    public void HandleCorrectAnswer(int myScore)
    {
        TextView displayScore = (TextView) getActivity().findViewById(R.id.display_score);
        displayScore.setText(String.valueOf(myScore));
    }
    
    public void SetLives(int numLivesLeft)
    {
        FuelBar fuelBar = (FuelBar) getActivity().findViewById(R.id.display_fuel);
        fuelBar.SetCustomProgress( (int)( (float)numLivesLeft/(float)m_maxLives * 100 ) );
    }
}
