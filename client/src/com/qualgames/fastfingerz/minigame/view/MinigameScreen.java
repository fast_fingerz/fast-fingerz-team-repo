package com.qualgames.fastfingerz.minigame.view;

import com.qualgames.fastfingerz.R;
import com.qualgames.fastfingerz.minigame.controller.MinigameController;
import com.qualgames.fastfingerz.minigame.model.MinigameModel;
import com.qualgames.fastfingerz.minigame.powerup.PowerUp.PowerUpType;

import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

// This activity manages all the minigame related aspects such as passing events to MinigameFragment and ScoreFragment
public class MinigameScreen extends Fragment {
	
	final static String TAG = "Fast Fingerz " + MinigameScreen.class.getSimpleName();
	private static final int ANSWER_INDICATION_SHOW_TIME = 800;
	
	MinigameController m_minigameController;
	MinigameModel m_minigameModel;
	
	// Fragments
	ScoreFragment m_scoreFragment;
	MyScoreFragment m_myScoreFragment;
	MinigameFragment m_minigameFragment;
	PowerUpFragment m_powerUpFragment;
	
	// handler for the time to show correct/wrong answer
	private Handler m_answerIndicationHandler = new Handler();
	
	private Handler m_pitStopHandler = new Handler();
	private Handler m_pitStopWarningHandler = new Handler();
	
	private int m_pitStopDuration, m_pitStopWarningDuration;
	
	public MinigameScreen(MinigameModel minigameModel, MinigameController minigameController)
	{
	    m_minigameController = minigameController;
	    m_minigameModel = minigameModel;
	}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		Log.d(TAG, "onCreateView");
		
		super.onCreate(savedInstanceState);
		
		// pass our instance to controller (kinda hacky?)
        m_minigameController.SetActivity(this);
		
		// get the scoring fragment 
		m_scoreFragment = m_minigameController.GetScoreFragmentInstance();
		// add the scoring Fragment to container
		getChildFragmentManager().beginTransaction().add(R.id.scoring_fragment_container, m_scoreFragment).commit();
		
		m_myScoreFragment = new MyScoreFragment(m_minigameController.GetNumLives());
		// add my score fragment to container
		getChildFragmentManager().beginTransaction().add(R.id.my_scoring_fragment_container, m_myScoreFragment).commit();
		
		// get the minigame fragment
		m_minigameFragment = m_minigameController.GetMinigameFragment();
		// set the controller
		m_minigameFragment.SetController(m_minigameController);
		// add the fragment to fragment container
		getChildFragmentManager().beginTransaction().add(R.id.minigame_fragment_container, m_minigameFragment).commit();
		
		// Add PowerUpFragment here
		m_powerUpFragment = new PowerUpFragment(m_minigameController);
		// add the fragment to fragment container
		getChildFragmentManager().beginTransaction().add(R.id.power_up_fragment_container, m_powerUpFragment).commit();

		// TODO: Set the layout for the rest of the UI in MinigameActivity
		/*
		TextView scoreView = (TextView) getActivity().findViewById(R.id.display_score);
		scoreView.setText("0");
		
		TextView comboView = (TextView) getActivity().findViewById(R.id.display_combo);
		comboView.setText("x1");
		*/
		
		View view = inflater.inflate(R.layout.minigame_layout, container, false);
		return view;
	}
	
	// indication that score has been updated
	public void UpdateScore()
	{
	    Log.d(TAG, "UpdateScore");
	    m_scoreFragment.HandleScoreUpdate();
	    
	    // TODO: Handle other activity update
	}
	
	
	public void GameTimerExpired()
	{
	    Log.d(TAG, "GameTimerExpired");
	    // TODO: Do something to indicate that game has finished if anything
	}
	
	// indication that power up has been activated
	public void ActivatePowerUp(PowerUpType powerUpType, int powerUpDuration)
	{
	    Log.d(TAG, "ActivatePowerUp");
	    switch (powerUpType) {
	    case EXTRA_LIFE:
	    	m_myScoreFragment.SetLives(m_minigameController.GetNumLives());
	    	break;
	    case INVINCIBLE:
	    case TIME_BOMB:
	    	break;
	    case TIME_BOMB_INCOMING:
	    	ImageView frozenImage = (ImageView)getActivity().findViewById(R.id.frozen_screen);
	    	frozenImage.setVisibility(View.VISIBLE);
	    	m_minigameFragment.SetIgnoreUserInput(true);
	    	break;
	    case SPEED_BOOST:
	    	
	    	break;
	    default:
	    	break;
	    }
	    
	    m_powerUpFragment.ActivatePowerUp(powerUpType);
	}
	
	// indication that power up has been deactivated
	public void DeactivatePowerUp(PowerUpType powerUpType)
	{
	    Log.d(TAG, "DeactivatePowerUp");
	    m_powerUpFragment.DeactivatePowerUp(powerUpType);
	    
	    if (powerUpType == PowerUpType.TIME_BOMB_INCOMING) 
	    {
	    	ImageView frozenImage = (ImageView)getActivity().findViewById(R.id.frozen_screen);
	    	frozenImage.setVisibility(View.INVISIBLE);
	    	m_minigameFragment.SetIgnoreUserInput(false);
	    }
	}
	
	public void ProcessPitStopWarning(int pitStopWarningDuration, int pitStopDuration) 
	{
		// This should be a circular progress bar with the seconds remaining in the middle
		ProgressBar circularProgressBar = (ProgressBar)getActivity().findViewById(R.id.pitStopCircularBar);
		circularProgressBar.setVisibility(View.VISIBLE);
		TextView pitStopWarningText = (TextView)getActivity().findViewById(R.id.pitStopText);
		pitStopWarningText.setVisibility(View.VISIBLE);
		TextView pitStopWarningCountdownText = (TextView)getActivity().findViewById(R.id.pitStopCountdownText);
		pitStopWarningCountdownText.setText(String.valueOf(pitStopWarningDuration));
		pitStopWarningCountdownText.setVisibility(View.VISIBLE);
		
		ObjectAnimator animation = ObjectAnimator.ofInt (circularProgressBar, "progress", 1, 500);
		animation.setDuration (pitStopWarningDuration * 1000); //in milliseconds
		animation.start();
		
		m_pitStopWarningHandler.postDelayed(m_pitStopWarningThread, 1000);
		m_pitStopDuration = pitStopDuration;
		m_pitStopWarningDuration = pitStopWarningDuration;
	}
	
	public void ProcessPitStop(int pitStopDuration)
	{
		// display pit stop
		ProgressBar circularProgressBar = (ProgressBar)getActivity().findViewById(R.id.pitStopCircularBar);
		TextView pitStopText = (TextView)getActivity().findViewById(R.id.pitStopText);
		pitStopText.setText("Use your powerups to win the race!");
		TextView pitStopCountdownText = (TextView)getActivity().findViewById(R.id.pitStopCountdownText);
		pitStopCountdownText.setText(String.valueOf(pitStopDuration));
		
		ObjectAnimator animation = ObjectAnimator.ofInt (circularProgressBar, "progress", 1, 500);
		animation.setDuration (pitStopDuration * 1000); //in milliseconds
		animation.start();
		
		ImageView pitStopSign = (ImageView)getActivity().findViewById(R.id.pit_stop_sign);
		pitStopSign.setVisibility(View.VISIBLE);
		// disable the fragments
    	m_minigameFragment.SetIgnoreUserInput(true);
    	m_powerUpFragment.SetIgnoreUserInput(true);
    	// start the timer
    	m_pitStopHandler.postDelayed(m_pitStopThread, 1000);
    	// let score fragment know to pause timer
    	m_scoreFragment.StopTimer();
    	
	}
	
	public void ProcessPitStopOver()
	{
		m_minigameFragment.SetIgnoreUserInput(false);
		m_powerUpFragment.SetIgnoreUserInput(false);
		
		m_scoreFragment.StartTimer();
		// hide the pit stop display
		ImageView pitStopSign = (ImageView)getActivity().findViewById(R.id.pit_stop_sign);
		pitStopSign.setVisibility(View.INVISIBLE);
		
		ProgressBar circularProgressBar = (ProgressBar)getActivity().findViewById(R.id.pitStopCircularBar);
		circularProgressBar.setVisibility(View.INVISIBLE);
		TextView pitStopWarningText = (TextView)getActivity().findViewById(R.id.pitStopText);
		pitStopWarningText.setVisibility(View.INVISIBLE);
		TextView pitStopWarningCountdownText = (TextView)getActivity().findViewById(R.id.pitStopCountdownText);
		pitStopWarningCountdownText.setVisibility(View.INVISIBLE);
		// TODO: get next question or not?
	}
	
	
	public void ProcessCorrectAnswer()
	{
	    Log.d(TAG, "ProcessCorrectAnswer");
	    
	    // notify score fragment of scoring change
	    m_scoreFragment.HandleScoreUpdate();
        // notify minigame fragment that answer is correct
	    m_minigameFragment.ProcessUserInputCorrect();
        
        // change the background screen to green
        View view = (View)getActivity().findViewById(R.id.minigame_view_container);
        view.setBackground(getResources().getDrawable(R.drawable.correct_answer_box));
        
        // notify MyScoreFragment that a live has been lost(if any)
        m_myScoreFragment.HandleCorrectAnswer(m_minigameController.GetMyScore());
        
        // start the show timer
        m_answerIndicationHandler.postDelayed(m_answerIndicationThread, ANSWER_INDICATION_SHOW_TIME);
        
        // get the next question
        m_minigameFragment.GetNextQuestion();
	}
	
	
	public void ProcessWrongAnswer()
	{	    
	    Log.d(TAG, "ProcessCorrectAnswer");
	    // notify minigame fragment that score is correct
	    m_minigameFragment.ProcessUserInputWrong();
        
        // TODO: handle other activity update
        View view = (View)getActivity().findViewById(R.id.minigame_view_container);
        view.setBackground(getResources().getDrawable(R.drawable.wrong_answer_box));
        
        // start the show timer
        m_answerIndicationHandler.postDelayed(m_answerIndicationThread, ANSWER_INDICATION_SHOW_TIME);
        
        // notify MyScoreFragment that a live has been lost(if any)
        m_myScoreFragment.SetLives(m_minigameController.GetNumLives());
        
        // get the next question
        m_minigameFragment.GetNextQuestion();
	}
	
	
	// we have finished the race... display something to user?
	public void TargetScoreReached()
	{
	    Log.d(TAG, "TargetScoreReached");
	}
	
	public long GetTimeRemaining()
	{
	    return m_scoreFragment.GetTimeRemaining();
	}
	
	
	// for showing the correct/wrong indication
	private Runnable m_answerIndicationThread = new Runnable() 
	{
		public void run()
		{
			Log.d(TAG, "answer indication timer has expired");
			// switch the background back to white color
			View view = (View)getActivity().findViewById(R.id.minigame_view_container);
			view.setBackgroundColor(0x00ffffff);
		}
	};
	
	
	// show the pit stop warning countdown
	private Runnable m_pitStopWarningThread = new Runnable() 
	{
		public void run()
		{
			m_pitStopWarningDuration--;
			if (m_pitStopWarningDuration > 0)
			{
				TextView pitStopWarningCountdownText = (TextView)getActivity().findViewById(R.id.pitStopCountdownText);
				pitStopWarningCountdownText.setText(String.valueOf(m_pitStopWarningDuration));
				m_pitStopWarningHandler.postDelayed(m_pitStopWarningThread, 1000);
			}
			Log.d(TAG, "pit stop warning timer has expired");
			//ProcessPitStop(m_pitStopDuration);
		}
	};
	
	// show the pit stop countdown
	private Runnable m_pitStopThread = new Runnable() 
	{
		public void run()
		{
			m_pitStopDuration--;
			if (m_pitStopDuration > 0)
			{
				TextView pitStopWarningCountdownText = (TextView)getActivity().findViewById(R.id.pitStopCountdownText);
				pitStopWarningCountdownText.setText(String.valueOf(m_pitStopDuration));
				m_pitStopHandler.postDelayed(m_pitStopThread, 1000);
			}
			else
			{
				Log.d(TAG, "pit stop timer has expired");
				ProcessPitStopOver();
			}
		}
	};

}
