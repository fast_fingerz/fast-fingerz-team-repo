package com.qualgames.fastfingerz.minigame.view;

import java.io.Serializable;
import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.qualgames.fastfingerz.R;
import com.qualgames.fastfingerz.minigame.controller.MinigameController;
import com.qualgames.fastfingerz.minigame.powerup.PowerUp.PowerUpType;

public class PowerUpFragment extends Fragment implements Serializable {
    
    private MinigameController m_minigameController;
    private final static String TAG = PowerUpFragment.class.getSimpleName();
    private boolean m_isIgnoreUserInput = false;
    
    public PowerUpFragment(MinigameController minigameController)
    {
        m_minigameController = minigameController;
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, 
            Bundle savedInstanceState) {
        
        Log.d(TAG, "onCreateView");
        
        return inflater.inflate(R.layout.power_up_fragment_layout, container, false);
    }

    @Override
    public void onStart()
    {
        super.onStart();
        Log.d(TAG, "ScoreFragment::onStart");

        SetInitialLayout();
    }
    
    public void ActivatePowerUp(PowerUpType powerUpType) 
    {
    	ImageButton button = (ImageButton)getActivity().findViewById(GetResourceForPowerUp(powerUpType));
    	
    	switch (powerUpType) {
    	case EXTRA_LIFE:
    		button.setBackgroundResource(R.drawable.extra_life_active);
    		break;
    	case SPEED_BOOST:
    		button.setBackgroundResource(R.drawable.speed_boost_active);
    		break;
    	case INVINCIBLE:
    		button.setBackgroundResource(R.drawable.invincible_active);
    		break;
    	case TIME_BOMB:
    		button.setBackgroundResource(R.drawable.freeze_bomb_active);
    		break;
    	case TIME_BOMB_INCOMING:
    		m_isIgnoreUserInput = true;
    		break;
    	default:
    		break;
    	}
    }
    
    public void DeactivatePowerUp(PowerUpType powerUpType) 
    {
    	
    	ImageButton button = (ImageButton)getActivity().findViewById(GetResourceForPowerUp(powerUpType));
    	
    	switch (powerUpType) {
    	case EXTRA_LIFE:
    		button.setBackgroundResource(R.drawable.extra_life);
    		break;
    	case SPEED_BOOST:
    		button.setBackgroundResource(R.drawable.speed_boost);
    		break;
    	case INVINCIBLE:
    		button.setBackgroundResource(R.drawable.invincible);
    		break;
    	case TIME_BOMB:
    		button.setBackgroundResource(R.drawable.freeze_bomb);
    		break;
    	case TIME_BOMB_INCOMING:
    		m_isIgnoreUserInput = false;
    		break;
    	default:
    		break;
    	}
    }
    
    public void SetIgnoreUserInput(boolean ignoreUserInput)
    {
    	m_isIgnoreUserInput = ignoreUserInput;
    	// TODO: Maybe grey out the power up icons?
    }
    
    private void SetInitialLayout()
    {
    	// TODO: Will need to dynamically populate buttons only based on what is in the list
        ArrayList<PowerUpType> powerUpList = m_minigameController.GetAvailablePowerUps();
        
        RegisterListenerForButton(PowerUpType.SPEED_BOOST);
        RegisterListenerForButton(PowerUpType.TIME_BOMB);
        RegisterListenerForButton(PowerUpType.INVINCIBLE);
        RegisterListenerForButton(PowerUpType.EXTRA_LIFE);
    }
    
    
    private void RegisterListenerForButton(final PowerUpType powerUpType) 
    {
    	int resourceId = GetResourceForPowerUp(powerUpType);
        ImageButton button = (ImageButton)getActivity().findViewById(resourceId);
        button.setOnClickListener(new OnClickListener() {
            
            @Override
            public void onClick(View v) {
            	if (m_isIgnoreUserInput) {
            		return;
            	}
                PowerUpSelected(powerUpType);                
            }
        });
    }
    
    private int GetResourceForPowerUp(PowerUpType powerUpType) 
    {    	
    	switch (powerUpType) {
	    case EXTRA_LIFE:
	    	return R.id.button_extra_life;
		case SPEED_BOOST:
			return R.id.button_turbo_boost;
		case INVINCIBLE:
			return R.id.button_invincible;
		case TIME_BOMB:
		case TIME_BOMB_INCOMING:
			return R.id.button_time_bomb;
    	}
    	
    	return 0;
    }
    
    private void PowerUpSelected(PowerUpType powerUpType) {
        
        m_minigameController.IFacePowerUpSelected(powerUpType);
    }
}
