package com.qualgames.fastfingerz.minigame.view;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.ProgressBar;

public class FuelBar extends ProgressBar {
    
    public int m_lastProgress = 100;

    public FuelBar(Context context) {
        super(context);
        
        setMax(100);
        setProgress(100);
    }
    
    public FuelBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        
        setMax(100);
        setProgress(100);
    }
    
    public FuelBar(Context context, AttributeSet attrs, int defSty) {
        super(context, attrs, defSty);
        
        setMax(100);
        setProgress(100);
    }
    
    
    public void SetCustomProgress(int progress) 
    {
        // let's do an animation here instead
        // TODO: should super.setProgress be called?
        ObjectAnimator animator = ObjectAnimator.ofInt( this, "progress", m_lastProgress, progress );
        animator.setDuration(700);
        animator.start();
        
        m_lastProgress = progress;
    }

}
