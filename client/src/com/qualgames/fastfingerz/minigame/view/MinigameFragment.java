package com.qualgames.fastfingerz.minigame.view;

import java.io.Serializable;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.qualgames.fastfingerz.minigame.controller.MinigameController;
import com.qualgames.fastfingerz.minigame.model.Question;
import com.qualgames.fastfingerz.minigame.model.UserInput;

// This class is the UI for the actual minigame
public abstract class MinigameFragment extends Fragment implements Serializable{
	
	final static String TAG = "Fast Fingerz";
	
	protected MinigameController m_minigameController;
	protected MinigameScreen m_myActivity = null;
	private long m_questionDisplayTime;
	
	protected boolean m_isIgnoreUserInput = false;
	
	public void SetController(MinigameController minigameController)
	{
	    m_minigameController = minigameController;
	}
	
	@Override
	public void onStart()
	{
		super.onStart();
		Log.d(TAG, "MinigameFragment::onStart");
		
		Bundle args = getArguments();
		// get the minigame controller from args
		
		SetInitialLayout();
		
		// handle the first question
		GetNextQuestion();
	}
	
	// this method handles the incoming question
	public void GetNextQuestion()
	{
		Log.d(TAG, "MinigameFragment::IncomingQuestion");
		// get the next question from controller
		Question question = m_minigameController.GetNextQuestion();
		// display the question
		DisplayQuestion(question);
		// save the start time of the question
		m_questionDisplayTime = System.nanoTime();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, 
	        Bundle savedInstanceState) {
		
		Log.d(TAG, "MinigameFragment::onCreateView");
		
		View view = inflater.inflate(GetMyResourceID(), container, false);
		// test
		view.setBackgroundColor(0x00ffffff);
		
		// Register any event listeners
		RegisterUserInputListeners(view);
		
		return view;
	}
	
	public void SetIgnoreUserInput(boolean ignoreUserInput)
	{
		m_isIgnoreUserInput = ignoreUserInput;
	}
	
	protected abstract int GetMyResourceID();	
	
	protected abstract void DisplayQuestion(Question question);
	
	protected abstract void SetInitialLayout();
	
	protected abstract void RegisterUserInputListeners(View view);
	
	// TODO: Should we know more about correct answer... like the time it took to get it right?
	public abstract void ProcessUserInputCorrect();
	
	public abstract void ProcessUserInputWrong();

	
	protected void SendUserInputToController(UserInput userInput)
	{
		Log.d(TAG, "MinigameFragment::SendUserInputToController");
		// determine the time it took for user to get correct answer
		int timeElapsedForCorrectAnswer = (int)( (System.nanoTime() - m_questionDisplayTime)/1000000 );
		// set the user time for UserInput
		userInput.SetTimeToAnswer(timeElapsedForCorrectAnswer);
		// send the event to MinigameController
		m_minigameController.IFaceUserInput(userInput);
	}

}
