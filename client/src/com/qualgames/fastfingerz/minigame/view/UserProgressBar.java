package com.qualgames.fastfingerz.minigame.view;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.ProgressBar;

import com.qualgames.fastfingerz.mp_session.Participant;

public class UserProgressBar extends ProgressBar {
    
    // we store the participant in case we ever want to do something fancy
    private Participant m_myParticipant;
    private int m_lastProgress = 0;
    
    public UserProgressBar(Context context) 
    {
        super(context);
        // set the max out of 100
        setMax(100);
        
        // TODO: setBackgroundDrawable/setProgressDrawable based on avatar or something
    }
    
    public UserProgressBar(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        
        // set the max out of 100
        setMax(100);
        
        // TODO: setBackgroundDrawable/setProgressDrawable based on avatar or something
    }
    
    public UserProgressBar(Context context, AttributeSet attrs, int defSty)
    {
        super(context, attrs, defSty);
        
        // set the max out of 100
        setMax(100);
        // TODO: setBackgroundDrawable/setProgressDrawable based on avatar or something
    }
    
    
    public void SetParticipant(Participant p)
    {
        m_myParticipant = p;
    }
    

    public void SetCustomProgress(int progress) 
    {
        // let's do an animation here instead
        // TODO: should super.setProgress be called?
        ObjectAnimator animator = ObjectAnimator.ofInt( this, "progress", m_lastProgress, progress );
        animator.setDuration(700);
        animator.start();
        
        m_lastProgress = progress;
    }
    
    
    
    public boolean IsProgressBarForParticipant(Participant p)
    {
        return m_myParticipant.id.equals(p.id);
    }
    
    
}
