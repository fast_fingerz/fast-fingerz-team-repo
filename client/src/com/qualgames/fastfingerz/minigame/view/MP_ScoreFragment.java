package com.qualgames.fastfingerz.minigame.view;

import java.util.ArrayList;
import java.util.Calendar;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.qualgames.fastfingerz.R;
import com.qualgames.fastfingerz.minigame.controller.MP_MinigameController;
import com.qualgames.fastfingerz.mp_session.Participant;

public class MP_ScoreFragment extends ScoreFragment{
    
    final static String TAG = "Fast Fingerz " + MP_ScoreFragment.class.getSimpleName();
	
	private MP_MinigameController m_minigameController;
	
	public MP_ScoreFragment(MP_MinigameController minigameController)
	{
	    super(minigameController);
	    m_minigameController = minigameController;
	}

	@Override
	protected void SetInitialLayout() 
	{		
	    /*
		TextView timerView = (TextView) getActivity().findViewById(R.id.mp_time_left);
		timerView.setText("0");
		
		TextView rankView = (TextView) getActivity().findViewById(R.id.mp_rank_view);
		rankView.setText("Rank: 1");
		*/
	    /*
	    TextView timerView = (TextView) getActivity().findViewById(R.id.mp_time_left);
        timerView.setText(String.format("%02d:%02d", m_gameDurationInSecs, 0));
	    
	    RaceTrackScoreView raceTrack = (RaceTrackScoreView)getActivity().findViewById(R.id.racetrack_score_view);
	    raceTrack.SetPointsGoal(m_minigameController.GetTargetScore());
	    raceTrack.setProgress(0, m_minigameController.GetUserScores());
	    raceTrack.setMaxProgress(360);
	    raceTrack.setRotation(0);
	    */
	    
	    Log.d (TAG, "SetInitialLayout");
	    // need to set visibility of progress bars based on number of players
	    InitProgressBars();
	}
	
	private void InitProgressBars()
	{
	    Log.d (TAG, "InitProgressBars");
	    ArrayList<UserProgressBar> progressBars = GetVisibleProgressBars();
	    int i = 0;
	    
	    // TODO: How to ensure that my participant is always on top?
	    for (Participant p : m_minigameController.GetUserScores().GetSortedUserScoreList() )
	    {
	        progressBars.get(i).SetParticipant(p);
	        i++;
	    }
	}
	
	private ArrayList<UserProgressBar> GetVisibleProgressBars() 
	{
	    ArrayList<UserProgressBar> progressBars = new ArrayList<UserProgressBar>();
	    
	    // there will be at least 2 progress bars
	    UserProgressBar progressBar = (UserProgressBar)getActivity().findViewById(R.id.progress_bar_1);
	    progressBars.add(progressBar);
	    
	    progressBar = (UserProgressBar)getActivity().findViewById(R.id.progress_bar_2);
	    progressBars.add(progressBar);

	    switch (m_minigameController.GetNumParticipants())
	    {
	    case 6:
	        progressBar = (UserProgressBar)getActivity().findViewById(R.id.progress_bar_6);
            progressBar.setVisibility(View.VISIBLE);
            progressBars.add(progressBar);
	    
	    case 5:
	        progressBar = (UserProgressBar)getActivity().findViewById(R.id.progress_bar_5);
	        progressBar.setVisibility(View.VISIBLE);
	        progressBars.add(progressBar);
	        
	    case 4:
	        progressBar = (UserProgressBar)getActivity().findViewById(R.id.progress_bar_4);
            progressBar.setVisibility(View.VISIBLE);
            progressBars.add(progressBar);
            
	    case 3:
	        progressBar = (UserProgressBar)getActivity().findViewById(R.id.progress_bar_3);
            progressBar.setVisibility(View.VISIBLE);
            progressBars.add(progressBar);
            
         default:
             break;
	    }
	    
	    return progressBars;
	}
	
	private UserProgressBar GetProgressBarForParticipant(Participant p)
	{
	    ArrayList<UserProgressBar> progressBars = GetVisibleProgressBars();
	    
	    for (UserProgressBar progressBar : progressBars)
	    {
	        if (progressBar.IsProgressBarForParticipant(p))
	            return progressBar;
	    }
	    
	    return null;
	}

	@Override
	public void HandleScoreUpdate() 
	{
	    Log.d(TAG, "HandleScoreUpdate");
	    /*
		TextView rankView = (TextView) getActivity().findViewById(R.id.mp_rank_view);
		rankView.setText("Rank: " + m_minigameController.GetUserScores().GetMyRank());
		*/
		
		// Let's process all the scores here
	    for (Participant p : m_minigameController.GetUserScores().GetSortedUserScoreList())
	    {
	        GetProgressBarForParticipant(p).SetCustomProgress((int)((double)p.gameScore/m_minigameController.GetTargetScore() * 100));
	    }
	}

	@Override
	protected void HandleTickUpdate(long timeElapsedInGame) 
	{
	    /*
	    RaceTrackScoreView raceTrack = (RaceTrackScoreView)getActivity().findViewById(R.id.racetrack_score_view);
	    float progress = 1 - ( (float)timeRemaingInGame/(float)(m_gameDurationInSecs * 1000) );
	    Log.d(TAG, "HandleTickUpdate progress " + progress);
	    
	    // this will get the race track to draw itself
	    raceTrack.setProgress(progress, m_minigameController.GetUserScores());
	    */
	    
	    // should show time elapsed instead of time remaining
		TextView timerView = (TextView) getActivity().findViewById(R.id.mp_time_elapsed);
		timerView.setText(String.format("%02d:%d", timeElapsedInGame / 1000, (timeElapsedInGame / 100) 	% 10));
		
				
	}

	@Override
	protected int GetMyResourceID() {
		return R.layout.mp_scoring_fragment_layout;
	}

}
