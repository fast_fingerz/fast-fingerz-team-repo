package com.qualgames.fastfingerz.minigame.view;

import java.io.Serializable;

import com.qualgames.fastfingerz.minigame.controller.MinigameController;

import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public abstract class ScoreFragment extends Fragment implements Serializable {
	
	final static String TAG = "Fast Fingerz";
	
	// Handler to update the time
	private Handler m_updateTimerHandler = new Handler();

	// this method loads the layout (without any questions)
	protected abstract void SetInitialLayout();
	
	final long UPDATE_TIMER_INTERVAL = 10;	
	
	protected int m_gameDurationInSecs;
	protected long m_startGameTime;
	protected long m_timeRemainingInGame;
	protected int m_targetScore;
	protected long m_timeElapsed = 0;
	private MinigameController m_minigameController;
	
	public ScoreFragment(MinigameController minigameController)
	{
	    m_minigameController = minigameController;
	}
	
	@Override
	public void onStart()
	{
		super.onStart();
		Log.d(TAG, "ScoreFragment::onStart");

		// set the game duration
		m_gameDurationInSecs = m_minigameController.GetGameDuration();
		m_targetScore = m_minigameController.GetTargetScore();

		// set the layout
		SetInitialLayout();
		
		// start the timer
		m_startGameTime = SystemClock.uptimeMillis();
		StartTimer();
	}
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, 
	        Bundle savedInstanceState) {
		
		Log.d(TAG, "ScoreFragment::onCreateView");
		return inflater.inflate(GetMyResourceID(), container, false);
	}
	
	
	public void StartTimer()
	{
		Log.d(TAG, "ScoreFragment::StartTimer");
		// schedule a task to execute periodically to update the timer
		m_updateTimerHandler.postDelayed(m_updateTimerThread, UPDATE_TIMER_INTERVAL);
	}
	
	private Runnable m_updateTimerThread = new Runnable() 
	{
		public void run()
		{
			// determine how much time has gone by
			/*
			m_timeRemainingInGame = (m_gameDurationInSecs * 1000) - (SystemClock.uptimeMillis() - m_startGameTime);
			if ( m_timeRemainingInGame <= 0 )
            {
			    // set the time to 0 in case it is negative
                m_timeRemainingInGame = 0;	
            }
            */
			//m_timeElapsed += UPDATE_TIMER_INTERVAL;
			m_timeElapsed = SystemClock.uptimeMillis() - m_startGameTime;
			
			// update the UI with update time... we should always display time elapsed
			HandleTickUpdate(m_timeElapsed);
			
			// time to show the pit stop warning
			if (Math.abs(m_timeElapsed - (m_minigameController.GetPitStopTimeAfterGameStart() * 1000)) <= 10)
			{
				m_minigameController.ProcessPitStopWarning();
			}
			
			if (Math.abs(m_timeElapsed - ( (m_minigameController.GetPitStopTimeAfterGameStart() * 1000) +
								 (m_minigameController.GetPitStopWarningDuration() * 1000))) <= 10 )
			{
				m_minigameController.ProcessPitStop();
			}
			else if (m_timeElapsed < m_gameDurationInSecs * 1000)//( m_timeRemainingInGame > 0 )
			{
				m_updateTimerHandler.postDelayed(m_updateTimerThread, UPDATE_TIMER_INTERVAL);
			}
			else
			{
				// remove the callback if time has expired... is this causing a crash?
				// m_updateTimerHandler.removeCallbacks(m_updateTimerThread);
				m_minigameController.GameTimerExpired();
			}
		}
	};
	
	public long GetTimeRemaining()
	{
	    return (m_gameDurationInSecs * 1000) - (SystemClock.uptimeMillis() - m_startGameTime);
	}
	
	public void StopTimer()
	{
	    m_updateTimerHandler.removeCallbacks(m_updateTimerThread);
	}
	
	@Override
	public void onDestroy()
	{
		super.onDestroy();
		Log.d(TAG, "ScoreFragment::onDestroy");
		// stop the timer callbacks
		m_updateTimerHandler.removeCallbacks(m_updateTimerThread);
	}
	
	// handle an update in scoring
	public abstract void HandleScoreUpdate();
	
	// this method will get executed whenever a tick (defined by timer) occurs thus we have to update score progress bar
	protected abstract void HandleTickUpdate(long timeRemainingInGame);
	
	protected abstract int GetMyResourceID();	
	
}
