package com.qualgames.fastfingerz.minigame;

public enum GameType {
	
	SWIPY(0),
	TRIVIA(1),
	MATH_QUESTION(2);
	
	public int value;
	GameType( int value )
	{
		this.value = value;
	}

}
