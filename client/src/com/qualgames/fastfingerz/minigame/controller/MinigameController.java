package com.qualgames.fastfingerz.minigame.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONException;

import com.qualgames.fastfingerz.minigame.GameType;
import com.qualgames.fastfingerz.minigame.Statistics;
import com.qualgames.fastfingerz.minigame.model.MinigameModel;
import com.qualgames.fastfingerz.minigame.model.Question;
import com.qualgames.fastfingerz.minigame.model.UserInput;
import com.qualgames.fastfingerz.minigame.powerup.PowerUp;
import com.qualgames.fastfingerz.minigame.powerup.PowerUp.PowerUpType;
import com.qualgames.fastfingerz.minigame.powerup.PowerUp.PowerUpUserInputStatus;
import com.qualgames.fastfingerz.minigame.powerup.PowerUpUserInputData;
import com.qualgames.fastfingerz.minigame.swipy.model.SwipyMinigameModel;
import com.qualgames.fastfingerz.minigame.view.MinigameFragment;
import com.qualgames.fastfingerz.minigame.view.MinigameScreen;
import com.qualgames.fastfingerz.minigame.view.ScoreFragment;

import android.os.Handler;
import android.util.Log;

public abstract class MinigameController implements Serializable {
	
	// For logging
	final static String TAG = "Fast Fingerz";
	
	// messages to be sent to MainActivity as part of Intent
	public final static String INCOMING_CONTROLLER_EVENT = "INCOMING_CONTROLLER_EVENT";
	public final static String MINIGAME_MODE_TYPE = "MINIGAME_MODE_TYPE";
	public final static String NEXT_QUESTION = "NEXT_QUESTION";
	public final static String MINIGAME_GAME_TYPE = "MINIGAME_GAME_TYPE";
	public final static String POWER_UP_TYPE_STR = "POWER_UP_TYPE";
	public final static String POWER_UP_DURATION = "POWER_UP_DURATION";
	
	private final static int MAX_LIVES_DEFAULT = 5;
	
	// after how many secs to activate the pit stop
	private final static int PIT_STOP_AFTER_GAME_START_SECS = 10;
	private final static int PIT_STOP_WARNING_DURATION = 3;
	private final static int PIT_STOP_DURATION = 5;
	
	protected MinigameControllerState m_state;
	protected MinigameModel m_currentGameData;
	protected Question m_currentQuestion;
	protected UserInput m_currentUserInput;
	protected ArrayList<PowerUp> m_currentPowerUps;
	protected PowerUpType m_powerUpTypePayload;
	protected int m_millisecsToAdd;
	protected boolean m_bIsMyRaceActive;   // indicates whether questions should be processed or not
	protected Statistics m_stats;
	
	protected int m_minigameDuration;	// the duration of the minigame
	protected int m_myScore;	// my current score
	protected int m_numLives;  // number of questions that user can get wrong
	protected long m_myGameStartTime;	// the time at which the minigame has started... may need for power ups
	
	protected MinigameScreen m_minigameActivity;
	
	protected enum MinigameControllerState
	{
		INIT,
		PLAYING_GAME,
		GAME_FINISHED;
	}
	
	protected enum MinigameControllerEvent
	{
		LAUNCH,
		USER_INPUT_SELECTED,
		POWER_UP_SELECTED,
		POWER_UP_DEACTIVATED,
		ADD_TIME_TO_MINIGAME,
		GAME_TIMER_EXPIRED,
		LEAVE_GAME,       // user has voluntarily or was fairly forced to leave the game
		NETWORK_ERROR,    // unexpected network error has occured
		CLOSE_ACTIVITY;   // indication that activity is no longer needed so go ahead and close it (to remove from history)
	}
	
	public enum MinigameModeType
	{
		MULTI_PLAYER,
		SINGLE_PLAYER,
		HEAD_TO_HEAD;		
	}
	
	public MinigameController(MinigameModel gameData)
	{
		m_currentGameData = gameData;
		Init();
	}
	
	public enum RaceFinishedStatus
	{
	    TARGET_SCORE_REACHED("targetScoreReached"),
	    DNF_OUT_OF_TIME("DNFOutOfTime"),
	    DNF_OUT_OF_LIVES("DNFOutOfLives");  // or gas? 
	    
	    public String string;
	    RaceFinishedStatus (String pValue)
        {
            this.string = pValue;
        }
	}
	
	// this method will do any additional handling related to scoring
	protected abstract void ProcessAdditionalScoreUpdate();
	
	// notify session controller of target score reached (SP or MP)
	protected abstract void ProcessRaceFinished(RaceFinishedStatus raceFinishedStatus, long timeRemaining);
	
	// notify session controller of user left game (SP or MP)
	protected abstract void ProcessLeaveGameEvent();
	
	// return the type of scoring we are doing (MP or SP)
	public abstract ScoreFragment GetScoreFragmentInstance();
	
	// returns the minigame mode of child (MP, SP, H2H)
	protected abstract MinigameModeType GetMyMinigameModeType();
	
	/********************** PUBLIC METHODS ********************************/
	private void Init()
	{
		Log.d(TAG, "MinigameController::Init");
		m_state = MinigameControllerState.PLAYING_GAME;
		m_myScore = 0;
		m_minigameActivity = null;
		m_currentPowerUps = new ArrayList<PowerUp>();
        m_minigameDuration = m_currentGameData.GetGameDuration() * 1000;
        m_stats = new Statistics();
        m_numLives = MAX_LIVES_DEFAULT;
        m_bIsMyRaceActive = true;
	}
	
	
	public void CloseActivity() {
	    Log.d (TAG, "MinigameController::CloseActivity");
	    HandleControllerEvent(MinigameControllerEvent.CLOSE_ACTIVITY);
	}
	
	
	public void IFaceUserInput(UserInput userInput)
	{
		Log.d(TAG, "MinigameController::IFaceUserInput");
		m_currentUserInput = userInput;
		HandleControllerEvent(MinigameControllerEvent.USER_INPUT_SELECTED);
	}
	
	
	public void IFacePowerUpSelected(PowerUpType powerUp)
	{
		Log.d(TAG, "MinigameController::IFacePowerUpSelected");
		m_powerUpTypePayload = powerUp;
		HandleControllerEvent(MinigameControllerEvent.POWER_UP_SELECTED);
	}
	
	// user has chosen to leave the game
	public void IFaceLeaveGame()
	{
	    Log.d(TAG, "MinigameController::IFaceLeaveGame");
	    HandleControllerEvent(MinigameControllerEvent.LEAVE_GAME);
	}
	
	
	public void GameTimerExpired()
	{
	    Log.d(TAG, "MinigameController::GameTimerExpired");
	    HandleControllerEvent(MinigameControllerEvent.GAME_TIMER_EXPIRED); 
	}
	
	public void SetActivity(MinigameScreen activity)
	{
	    Log.d(TAG, "MinigameController::SetActivity");
	    m_minigameActivity = activity;
	}
	
	
	public ArrayList<PowerUpType> GetAvailablePowerUps() 
	{
	    Log.d(TAG, "MinigameController::GetListOfAvailablePowerUps");
	    return PowerUp.GetAvailablePowerUps(GetMyMinigameModeType());
	}
	
	/********************* PIT STOP BEGIN **************************************/
	public void ProcessPitStopWarning()
	{
		m_minigameActivity.ProcessPitStopWarning(PIT_STOP_WARNING_DURATION, PIT_STOP_DURATION);
	}
	
	public void ProcessPitStop()
	{
		m_minigameActivity.ProcessPitStop(PIT_STOP_DURATION);
	}
	/********************* PIT STOP END ****************************************/
	
	
	/********************* POWER UP BEGIN **************************************/
	// these functions are for PowerUp to call
	public void PowerUpDeactivated(PowerUpType powerUp)
	{
		Log.d(TAG, "MinigameController::DeactivatePowerUp");
		m_powerUpTypePayload = powerUp;
		HandleControllerEvent(MinigameControllerEvent.POWER_UP_DEACTIVATED);
	}
	
	// this can probably only be called by a power up
	// TODO: Remove this
	public void AddTimeToMinigame(int nMillisecsToAdd)
	{
		Log.d(TAG, "MinigameController::AddTimeToMinigame");
		m_millisecsToAdd = nMillisecsToAdd;
		HandleControllerEvent(MinigameControllerEvent.ADD_TIME_TO_MINIGAME);
	}
	
	// do not think this is necessary?
	public void ActivatePowerUp(PowerUpType powerUp)
	{
		
	}
	/********************* POWER UP END **************************************/
	
	public int GetMyScore()
	{
		return m_myScore;
	}
	
	
	public int GetGameDuration()
	{
		return m_currentGameData.GetGameDuration();
	}

	public int GetTargetScore()
	{
	    return m_currentGameData.GetPointsGoal();
	}
	
	public Question GetNextQuestion()
	{
	    Log.d(TAG, "MinigameController GetNextQuestion");
	    m_currentQuestion = m_currentGameData.GetNextQuestion();
		return m_currentQuestion;
	}
	
	public boolean IsMyRaceActive() 
	{
	    return m_bIsMyRaceActive;
	}
	
	public int GetNumLives() 
	{
	    return m_numLives;
	}
	
	public int GetPitStopTimeAfterGameStart()
	{
		return PIT_STOP_AFTER_GAME_START_SECS;
	}
	
	public int GetPitStopWarningDuration()
	{
		return PIT_STOP_WARNING_DURATION;
	}
	
	public int GetPitStopDuration()
	{
		return PIT_STOP_DURATION;
	}
	
	// This is bad code but it makes our lives so much easier by not having objects be Parcelable!
	// TODO: remove this method
	/*
	public static ScoreFragment GetScoreFragment(MinigameModeType modeType)
	{
		Log.d(TAG, "MinigameController::GetScoreFragment");
		
		ScoreFragment scoreFragment = null;
		switch (modeType)
		{
			case HEAD_TO_HEAD:
				break;
			case MULTI_PLAYER:
				scoreFragment = MP_MinigameController.getInstance().GetScoreFragmentInstance();
				break;
			case SINGLE_PLAYER:
				break;
		}
		
		return scoreFragment;
	}
	*/
	
	
	public MinigameFragment GetMinigameFragment()
	{
		return m_currentGameData.GetMyMinigameFragment();
	}
	
	/********************** STATIC METHODS FOR GAME DATA GENERATION ********************************/
	// generate specific minigame data (to be used for single player)
	public static ArrayList<MinigameModel> GenerateGameData(ArrayList<GameType> gameOrder)
	{
		Log.d(TAG, "MinigameController::GenerateGameData for single player");
		return GenerateAllMinigameData(gameOrder);
	}
	
	// generate random minigame data from scratch
	// TODO: Have a configurable way to set the time of each game... will come from server
	public static ArrayList<MinigameModel> GenerateGameData(int numGames)
	{
		Log.d(TAG, "MinigameController::GenerateGameData based on numGames - " + numGames);
		ArrayList<GameType> gameOrder = GenerateMinigameOrder(numGames);
		return GenerateAllMinigameData(gameOrder);
	}
	
	public static JSONArray EncodeToJSON(ArrayList<MinigameModel> gameData) throws JSONException
	{
		JSONArray jsonArray = new JSONArray();
		
		for (int i = 0; i < gameData.size(); i++)
		{
			jsonArray.put(gameData.get(i).EncodeToJSON());
		}
		
		return jsonArray;
	}
	
	// generate game data based on JSONObject
	public static ArrayList<MinigameModel> GenerateGameData(JSONArray jsonArray)  throws JSONException
	{
		Log.d(TAG, "MinigameController::GenerateGameData from json");
		ArrayList<MinigameModel> minigameData = new ArrayList<MinigameModel>();
		
		
		for (int i = 0; i < jsonArray.length(); i++)
		{	
			minigameData.add( MinigameModel.GenerateMinigameModelBasedOnJSON(jsonArray.getJSONObject(i)) );
		}
		
		return minigameData;
	}
	
	// generate a random order of minigames based on numGames
	private static ArrayList<GameType> GenerateMinigameOrder(int numGames)
	{
		Log.d(TAG, "MinigameController::GenerateMinigameOrder");
		ArrayList<GameType> gameTypes = new ArrayList<GameType>();
		Random r = new Random();
		
		for (int i = 0; i < numGames; i++)
		{
			// add a random GameType to the ArrayList
			// TODO: Once we have more games update this to include others
			gameTypes.add(GameType.SWIPY);            //[r.nextInt(GameType.values().length)]);
		}
		return gameTypes;
	}
	
	
	// generate the game data given the games
	private static ArrayList<MinigameModel> GenerateAllMinigameData(ArrayList<GameType> gameOrder)
	{
		Log.d(TAG, "MinigameController::GenerateAllMinigameData");
		ArrayList<MinigameModel> minigameData = new ArrayList<MinigameModel>();
		int numGames = gameOrder.size();
		
		// create a new model object for each type of game and generate the data
		for (int i = 0; i < numGames; i++)
		{
			switch (gameOrder.get(i))
			{
				case SWIPY:
				{
					// when instatiating a new model object the game data will automatically get created
					minigameData.add(new SwipyMinigameModel());
					break;
				}
				
				default:
					break;
			}
		}
		return minigameData;
	}
	
	/********************** INTERNAL EVENT HANDLING ********************************/
	// determine if this event came in a correct state and should be handled
	private boolean IsExpectedControllerEvent(MinigameControllerEvent event)
	{
		Log.d(TAG, "MinigameController::IsExpectedControllerEvent");
		boolean bIsExpectedEvent = true;
		
		switch (m_state)
		{
			case GAME_FINISHED:
			case INIT:
				if ( event != MinigameControllerEvent.LAUNCH )
				{
					bIsExpectedEvent = false;
				}
				break;
			default:
				break;
		}
		
		if ( !bIsExpectedEvent )
		{
			Log.d(TAG, "MinigameController:: Unexpected event " + event + "in state " + m_state);
			// should we throw an error to MainController in order to recover?
		}
		
		return bIsExpectedEvent;
	}
	
	private void HandleControllerEvent(MinigameControllerEvent event)
	{
		Log.d(TAG, "MinigameController::HandleControllerEvent event " + event);
		if (!IsExpectedControllerEvent(event))
			return;
		
		switch (event)
		{
		case GAME_TIMER_EXPIRED:
			ProcessGameTimerExpired();
			break;
		case USER_INPUT_SELECTED:
			ProcessUserInput();
			break;
		case POWER_UP_SELECTED:
			ProcessPowerUpSelected();
			break;
		case POWER_UP_DEACTIVATED:
			ProcessPowerUpDeactivated();
			break;
		case LEAVE_GAME:
		    ProcessLeaveGameEvent();
		    break;
		default:
			break;
		}
	}
	
	
	private void ProcessPowerUpDeactivated()
	{
		Log.d(TAG, "MinigameController::ProcessPowerUpSelected");
		
		// remove power up from array list
		m_currentPowerUps.remove(m_powerUpTypePayload);
		
		m_minigameActivity.DeactivatePowerUp(m_powerUpTypePayload);
	}
	
	
	private void ProcessPowerUpSelected()
	{
		Log.d(TAG, "MinigameController::ProcessPowerUpSelected");
		// do sanity check to ensure that power up is for right playing mode and already does not exist
		if (!PowerUp.IsPowerUpValid(m_powerUpTypePayload, GetMyMinigameModeType()))
		{	
			// throw an error or do something?
			Log.d(TAG, "Power up is not valid for this game type!");
			return;
		}
		
		// check if power up is already active...
		if ( m_currentPowerUps.size() >= 1 )
		{
			for (int i = 0; i < m_currentPowerUps.size(); i++)
			{
				if (m_powerUpTypePayload == m_currentPowerUps.get(i).GetMyPowerUpType())
				{
					if ( m_currentPowerUps.get(i).IsActive() )
					{
						// if power up is already active then substitute it with the current one
						Log.d(TAG, "PowerUp " + m_powerUpTypePayload + " already active... replace it with new one");
					}
					m_currentPowerUps.remove(i);
					break;
				}
			}
		}
		
		// TODO: Somewhere somehow we have to subtract the number of total power ups user has
		// TODO: Should be saved in shared preferences
		// TODO: Also pass the number of power ups remaining to MinigameActivity
		
		// make a new power up based on PowerUpType and add to ArrayList
		PowerUp powerUp = PowerUp.GetPowerUpInstance(m_powerUpTypePayload, this);
		
		// let's check if power up needs to be process right away or added to list for later processing
		if ( powerUp.DetermineAction() == PowerUpUserInputStatus.APPLY_IMMEDIATLY)
        {
		    // for now only lives and score can be immediately affected without user input
            PowerUpUserInputData userInputData = new PowerUpUserInputData();
            userInputData.score = m_myScore;
            userInputData.numLives = m_numLives;
            
            powerUp.ApplyToUserInput(userInputData);
            
            m_myScore = userInputData.score;
            m_numLives = userInputData.numLives;
            
            // check in case new lives were added
            if (m_numLives > 0)
            {
            	m_bIsMyRaceActive = true;
            }
        }
		else
		{
		    m_currentPowerUps.add(powerUp);
		    // activate the power up to preform its action
	        powerUp.Activate();
		}
		
		// send power up indication to activity
		m_minigameActivity.ActivatePowerUp(m_powerUpTypePayload, powerUp.GetDuration());
	}
	
	// TODO: Not sure this will be needed anymore as activity will tell controller when timer has expired
	private void ProcessGameTimerExpired()
	{
		Log.d(TAG, "MinigameController::ProcessGameTimerExpired");
		m_state = MinigameControllerState.GAME_FINISHED;
		// notify Activity of game over
		m_minigameActivity.GameTimerExpired();
		
		// notify either MP_SessionController or SP_SessionController of t
		ProcessRaceFinished(RaceFinishedStatus.DNF_OUT_OF_TIME, 0);
		m_bIsMyRaceActive = false;
	}
	
	
	private void ProcessUserInput()
	{		
		Log.d(TAG, "MinigameController::ProcessUserInput");
		
		PowerUpUserInputStatus processUserInputStatus = PowerUpUserInputStatus.PROCESS_DEFAULT;
		// check if power ups are active
		if ( m_currentPowerUps.size() >= 1 )
		{
		    for (PowerUp powerUp : m_currentPowerUps)
			{
		        // if power up is not active, then continue to next iteration
		        if (!powerUp.IsActive())
		        {
		            continue;
		        }
				// if any of the active power ups tells us to ignore user input then it takes precedence
				if ( powerUp.DetermineAction() == PowerUpUserInputStatus.IGNORE_USER_INPUT )
				{
					// user input should be ignored... just return
				    // or notify Activity that user input was ignored due to power up?
				    // TODO: Once a power up is activated that stops user from answering, Activity should be responsible for not sending any answers to controller
					return;
				}
				if ( powerUp.DetermineAction() == PowerUpUserInputStatus.APPLY_TO_USER_INPUT )
                {
                    // power ups have changed the variables so re-assign them
                    processUserInputStatus = PowerUpUserInputStatus.APPLY_TO_USER_INPUT;
                }
			}
		}
		
		// controller should process user input only if it hasn't already been processed by power ups
		if ((processUserInputStatus != PowerUpUserInputStatus.IGNORE_USER_INPUT) && m_bIsMyRaceActive)
		{	
		    m_stats.questions_answered++;
			// is user input correct?
			if ( m_currentQuestion.IsUserInputCorrect(m_currentUserInput) )
			{			
				// calculate our score
				int thisScore = GetScoreForCorrectAnswer(m_currentUserInput.GetTimeToAnswer(), m_currentQuestion.IsWorthDouble());
		        
				// if an additional power up needs to be applied to user input then do additional processing
				if (processUserInputStatus == PowerUpUserInputStatus.APPLY_TO_USER_INPUT)
				{
				    ApplyPowerUpToUserInput(thisScore, true);
				}
				else
				{
				    // since no power up can affect the score just add to it
				    m_myScore += thisScore;
				}
		        // keep track of stats
		        m_stats.questions_answered_correctly++;
				
				// optional activities related to scoring (for ex: send score to all users)
				ProcessAdditionalScoreUpdate();
				
				// Notify MinigameActivity of correct answer
				SendAnswerResultToActivity(true);
			}
			else
			{		
			    if (processUserInputStatus == PowerUpUserInputStatus.APPLY_TO_USER_INPUT)
                {
                    ApplyPowerUpToUserInput(0, false);
                }
                else
                {
                    m_numLives--;
                }
			    
				SendAnswerResultToActivity(false);
				
				// TODO: if out of lives, then need to notify server and start a timer to let user use power up before sending final score
                if (m_numLives <= 0) 
                {
                    ProcessRaceFinished(RaceFinishedStatus.DNF_OUT_OF_LIVES, m_minigameActivity.GetTimeRemaining());
                    m_bIsMyRaceActive = false;
                }
			}
		}
		
		// let's check if we finished the race
		if ( m_myScore >= m_currentGameData.GetPointsGoal() )
		{
		    m_minigameActivity.TargetScoreReached();
		    // need to wait for the race to finish here (if in mp)
		    // also need to stop accepting user input
		    ProcessRaceFinished(RaceFinishedStatus.TARGET_SCORE_REACHED, m_minigameActivity.GetTimeRemaining());
		    m_bIsMyRaceActive = false;
		}
		
		//SendNextQuestionToActivity();
	}
	
	
	private void ApplyPowerUpToUserInput(int thisScore, boolean isUserInputCorrect) 
	{
        for (PowerUp powerUp : m_currentPowerUps)
        {
            // this is done just so that we can change input parameters
            PowerUpUserInputData userInputData = new PowerUpUserInputData();
            userInputData.score = thisScore;
            userInputData.numLives = m_numLives;
            userInputData.isUserInputCorrect = isUserInputCorrect;
            
            if (powerUp.IsActive())
            {
                powerUp.ApplyToUserInput(userInputData);
            }
            
            m_myScore += userInputData.score;
            if (userInputData.numLives <= MAX_LIVES_DEFAULT) {
            	m_numLives = userInputData.numLives;
            }
        }
	}
	
	// this method will update m_myScore and m_myMultiplier depending on time of correct answer
	private int GetScoreForCorrectAnswer(int timeElapsedForCorrectAnswer, boolean isWorthDouble)
	{
		Log.d(TAG, "MinigameController::UpdateScoreForCorrectAnswer isWorthDouble - " + isWorthDouble);
		
		// let's calculate how much this correct answer was worth
		int thisScore = m_currentGameData.CalculateScoreForCorrectAnswer(timeElapsedForCorrectAnswer);
		// is it worth double?
		if ( isWorthDouble )
		{
			thisScore *= 2;
		}		
		
		return thisScore;
	}
		
	// TODO: Activity should be able to query game state in order to decide whether to allow user input or not
	private void SendAnswerResultToActivity(boolean isCorrectAnswer)
	{
		Log.d(TAG, "MinigameController::SendAnswerResultToActivity");
		if ( isCorrectAnswer )
		{
		    m_minigameActivity.ProcessCorrectAnswer();
		}
		else
		{
		    m_minigameActivity.ProcessWrongAnswer();
		}
	}	

	
	// TODO: Don't think this method is necessary since activity will poll every 10 ms all the scores anyway
	protected void SendScoreEventToActivity()
	{
		Log.d(TAG, "MinigameController::SendScoreEventToActivity");				
		m_minigameActivity.UpdateScore();
	}
}
