package com.qualgames.fastfingerz.minigame.controller;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.qualgames.fastfingerz.minigame.Statistics;
import com.qualgames.fastfingerz.minigame.model.MinigameModel;
import com.qualgames.fastfingerz.minigame.powerup.PowerUp.PowerUpType;
import com.qualgames.fastfingerz.minigame.view.MP_ScoreFragment;
import com.qualgames.fastfingerz.minigame.view.ScoreFragment;
import com.qualgames.fastfingerz.mp_session.Participant;
import com.qualgames.fastfingerz.mp_session.RaceUserScores;
import com.qualgames.fastfingerz.mp_session.controller.MP_SessionController;
import com.qualgames.fastfingerz.mp_session.server_session.OutgoingMessage;
import com.qualgames.fastfingerz.mp_session.server_session.ServerSession;
import com.qualgames.fastfingerz.mp_session.server_session.SocketIncomingMessageListener;

// handle time_bombs
// handle incoming scoring events
public class MP_MinigameController extends MinigameController implements SocketIncomingMessageListener {
	
	final String UPDATE_SCORE_STR = "UpdateScore";
	final static String TAG = "Fast Fingerz";

	private RaceUserScores m_currentAllUserScores;
	private String m_myUserName;
	private MP_SessionController m_mpSessionController;
	
	// for receiving/sending messages
    private static class IncomingMessageType
    {
        public static final String GAME_STATUS_UPDATE = "game_status_update";
        public static final String FINAL_SCORE_SUMMARY = "game_end";
    };
    
    public static class OutgoingMessageType
    {
        public static final String GAME_STATUS_UPDATE = "game_status_update_req";
        public static final String FINAL_SCORE = "score_final";
    }
    
    public static class MessageProtocol
    {       
        public static final String TYPE = "type";
        public static final String SCORE_UPDATE = "score_update";
        public static final String SCORE = "score";
        public static final String POWER_UP = "power_up";
        public static final String USER_ID = "id";
        public static final String DNF = "dnf";
        public static final String PAYLOAD = "payload";
        public static final String VALUE = "value";
        public static final String FROM = "from";
        public static final String TIME = "time";
        public static final String STATS = "stats";
        public static final String END_STATUS = "endStatus";
        public static final String FINISHED_POSITION = "finishedPosition";
        public static final String USER_ARRAY = "userArray";
    }
	
	public MP_MinigameController(MinigameModel minigameModel, MP_SessionController mpSessionController)
	{
	    super(minigameModel);
	    m_mpSessionController = mpSessionController;
	    
	    Init();
	}
	
	private void Init()
	{
		Log.d(TAG, "MP_MinigameController::Init");

		m_currentAllUserScores = new RaceUserScores(m_mpSessionController.GetParticipants());
		
		// register for all incoming messages
		ServerSession serverSession = m_mpSessionController.GetServerSession();
		serverSession.RegisterForIncomingMessage(IncomingMessageType.GAME_STATUS_UPDATE, this);
		serverSession.RegisterForIncomingMessage(IncomingMessageType.FINAL_SCORE_SUMMARY, this);
	}
	
	public int GetNumParticipants()
	{
	    return m_mpSessionController.GetParticipants().size();
	}
	
	public RaceUserScores GetUserScores()
	{
		return m_currentAllUserScores;
	}
	
	public class GameStatusUpdateMessage extends OutgoingMessage
	{
	    String type;
	    int payloadValue;
	    
	    public GameStatusUpdateMessage(String type, int payloadValue)
	    {
	        super(OutgoingMessageType.GAME_STATUS_UPDATE);
	        this.type = type;
	        this.payloadValue = payloadValue;
	    }
	    
        @Override
        public JSONObject EncodeToJSON() throws JSONException 
        {
            JSONObject data = new JSONObject();
            data.put(MessageProtocol.TYPE, type);
            data.put(MessageProtocol.PAYLOAD, payloadValue);
            return data;
        }
	    
	}
	
	public class FinalScoreMessage extends OutgoingMessage
    {
        int score;
        long time;
        Statistics stats;
        RaceFinishedStatus raceFinishedStatus;
        
        public FinalScoreMessage(int score, long time, Statistics stats, RaceFinishedStatus raceFinishedStatus)
        {
            super(OutgoingMessageType.FINAL_SCORE);
            this.score = score;
            this.time = time;
            this.stats = stats;
            this.raceFinishedStatus = raceFinishedStatus;
        }

        @Override
        public JSONObject EncodeToJSON() throws JSONException {
            
            JSONObject data = new JSONObject();
            data.put(MessageProtocol.SCORE, score);
            data.put(MessageProtocol.TIME, time);
            data.put(MessageProtocol.STATS, stats.EncodeToJSON());
            data.put(MessageProtocol.END_STATUS, raceFinishedStatus.string);
            
            return data;
        }
    }
	
	public void SendGameStatusUpdateMessage(String type, int payloadValue)
	{
		GameStatusUpdateMessage scoreMessage = new GameStatusUpdateMessage(type, payloadValue);
		ServerSession serverSession = m_mpSessionController.GetServerSession();
		try {
            serverSession.Send(scoreMessage);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
	}

	@Override
	protected void ProcessAdditionalScoreUpdate()
	{
		Log.d(TAG, "MP_MinigameController::ProcessAdditionalScoreUpdate");
		// update my score in hash map score
		m_currentAllUserScores.UpdateMyScore(m_myScore);
		
		// send message to server indicating my score
		SendGameStatusUpdateMessage(MessageProtocol.SCORE_UPDATE, m_myScore);
	}
	
	@Override
	protected void ProcessLeaveGameEvent()
	{
	    Log.d(TAG, "MP_MinigameController::ProcessLeaveGameEvent");
	    /*
	    try {
            MP_SessionController.getInstance().LeaveGame();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        */
	}
	
	@Override
	public ScoreFragment GetScoreFragmentInstance() {
		Log.d(TAG, "MP_MinigameController::GetScoreFragment");
		return new MP_ScoreFragment(this);
	}
	
	@Override
	protected MinigameModeType GetMyMinigameModeType()
	{
		return MinigameModeType.MULTI_PLAYER;
	}

	
    @Override
    public void HandleIncomingMessage(String event, JSONObject data) throws JSONException 
    {
        Log.d(TAG, "MP_MinigameController::HandleIncomingMessage msg " + data.toString());
        if (event == IncomingMessageType.GAME_STATUS_UPDATE)
        {
            ProcessGameStatusMessage(data);
        }   
        else if (event == IncomingMessageType.FINAL_SCORE_SUMMARY)
        {
            ProcessFinalScoreSummaryMessage(data);
        }
    }
    
    private void ProcessFinalScoreSummaryMessage(JSONObject data) throws JSONException
    {
        // let's read the message and send the UserScores to MP_SessionController
        JSONArray users = data.getJSONArray(MessageProtocol.USER_ARRAY);
        
        for (int i = 0; i < users.length(); i++)
        {
            JSONObject user = users.getJSONObject(i);
            Participant p = new Participant(user.getString(MessageProtocol.USER_ID));
            
            p.finishedPosition = user.getInt(MessageProtocol.FINISHED_POSITION);
            p.gameScore = user.getInt(MessageProtocol.SCORE);
            p.stats = Statistics.GenerateFromData(user.getJSONObject(MessageProtocol.STATS));
            p.raceFinishedStatus = RaceFinishedStatus.values()[user.getInt(MessageProtocol.END_STATUS)];
            p.timeElapsed = user.getLong(MessageProtocol.TIME); // TODO: convert to String?
            
            m_currentAllUserScores.WriteUserFinalScore(p);            
        }
        
        // notify MP_SessionController
        m_mpSessionController.MinigameComplete(m_currentAllUserScores);
        
    }
    
    private void ProcessGameStatusMessage(JSONObject data) throws JSONException
    {
        Log.d(TAG, "MP_MinigameController::ProcessGameStatusMessage");
        
        String eventType = data.getString(MessageProtocol.TYPE);
        int payload = data.getInt(MessageProtocol.PAYLOAD);
        String userID = data.getString(MessageProtocol.FROM);
        
        Log.d(TAG, "MP_MinigameController msg contents type " + eventType + ", payload " + payload + ", userID " + userID);
        
        if (eventType.equals(MessageProtocol.SCORE_UPDATE))
        {
            Log.d(TAG, "MP_MinigameController received SCORE_UPDATE");
            Participant p = new Participant(userID);
            p.gameScore = payload;
            
            m_currentAllUserScores.UpdateUserScore(p);
            
            // may need to remove this?
            SendScoreEventToActivity();
        }
        else if (eventType.equals(MessageProtocol.POWER_UP))
        {
            // handle incoming time bomb power up
        	if (payload == PowerUpType.TIME_BOMB.value) 
        	{
        		IFacePowerUpSelected(PowerUpType.TIME_BOMB_INCOMING);
        	}
        }
        else if (eventType.equals(MessageProtocol.DNF))
        {
            
        }
    }

    @Override
    protected void ProcessRaceFinished(RaceFinishedStatus raceFinishedStatus, long timeRemaining) {
       
        // send final_score message to server and wait to receive msg from server indicating race over
        // points, time, stats:{questions_answered, questions_answered_correct}, end_status
        FinalScoreMessage finalScoreMessage = new FinalScoreMessage(m_myScore, timeRemaining, m_stats, raceFinishedStatus);
        try {
            m_mpSessionController.GetServerSession().Send(finalScoreMessage, this);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }        
    }
}

