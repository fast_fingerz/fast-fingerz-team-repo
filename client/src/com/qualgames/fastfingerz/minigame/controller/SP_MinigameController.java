package com.qualgames.fastfingerz.minigame.controller;

import com.qualgames.fastfingerz.minigame.model.MinigameModel;
import com.qualgames.fastfingerz.minigame.view.SP_ScoreFragment;
import com.qualgames.fastfingerz.minigame.view.ScoreFragment;

public class SP_MinigameController extends MinigameController {
    
    public SP_MinigameController(MinigameModel gameData)
    {
        super(gameData);
    }

	@Override
	protected void ProcessAdditionalScoreUpdate() {		
		// nothing to do
		return;
	}

	@Override
	public ScoreFragment GetScoreFragmentInstance() {
		return new SP_ScoreFragment(this);
	}

	@Override
	protected MinigameModeType GetMyMinigameModeType() {
		
		return MinigameController.MinigameModeType.SINGLE_PLAYER;
	}

    @Override
    protected void ProcessLeaveGameEvent() {
        // TODO Auto-generated method stub
        
    }

    @Override
    protected void ProcessRaceFinished(RaceFinishedStatus raceFinishedStatus,
            long timeRemaining) {
        // TODO Auto-generated method stub
        
    }

}
