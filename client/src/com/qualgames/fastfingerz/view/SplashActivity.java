package com.qualgames.fastfingerz.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

import com.qualgames.fastfingerz.R;

public class SplashActivity extends Activity {
    private final static int SPLASH_TIMER = 2000;
    private Handler splashHandler = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        splashHandler = new Handler();
        splashHandler.postDelayed(new Runnable() {

            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this, GameTypeActivity.class);
                startActivity(intent);
                
                finish();
            }

        }, SPLASH_TIMER);
    }

    @Override
    public void onStop() {
        super.onStop();

        splashHandler.removeCallbacksAndMessages(null);
    }
}
