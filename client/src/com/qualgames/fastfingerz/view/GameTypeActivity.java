package com.qualgames.fastfingerz.view;

import org.json.JSONException;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.qualgames.fastfingerz.R;
import com.qualgames.fastfingerz.controller.MainController;
import com.qualgames.fastfingerz.mp_session.SessionParams;
import com.qualgames.fastfingerz.mp_session.SessionParams.Difficulty;

public class GameTypeActivity extends Activity {
    private static final String LOG_TAG = "GameTypeFragment";
    private View gameOptions = null;
    private View gameCategories = null;
    private View gameLevels = null;
    private MainController m_mainController;
    private SessionParams params = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_game_type);

        gameOptions = findViewById(R.id.game_options);
        gameCategories = findViewById(R.id.game_categories);
        gameLevels = findViewById(R.id.game_levels);

        params = new SessionParams();
        createOptions();

        // initialize the main controller with our Activity
        m_mainController = new MainController(this);
    }

    private void createOptions() {
        SessionParams.RaceType[] raceTypes = SessionParams.RaceType.values();
        for (final SessionParams.RaceType raceType : raceTypes) {
            RadioButton option = new RadioButton(this);
            option.setText(raceType.captionId);
            option.setTextColor(Color.WHITE);
            ((RadioGroup) gameOptions).addView(option);

            option.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    params.raceType = raceType;
                }
            });

            if (params.raceType == raceType) {
                option.setChecked(true);
            }
        }

        SessionParams.CategoryType[] categories = SessionParams.CategoryType.values();
        for (final SessionParams.CategoryType category : categories) {
            RadioButton option = new RadioButton(this);
            option.setText(category.captionId);
            option.setTextColor(Color.WHITE);
            ((RadioGroup) gameCategories).addView(option);

            option.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    params.categoryType = category;
                }
            });

            if (params.categoryType == category) {
                option.setChecked(true);
            }
        }

        SessionParams.Difficulty[] difficulties = SessionParams.Difficulty.values();
        for (final SessionParams.Difficulty difficulty : difficulties) {
            if (difficulty != Difficulty.UNDEFINED) {
                RadioButton option = new RadioButton(this);
                option.setText(difficulty.captionId);
                option.setTextColor(Color.WHITE);
                ((RadioGroup) gameLevels).addView(option);

                option.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        params.difficulty = difficulty;
                    }
                });

                if (params.difficulty == difficulty) {
                    option.setChecked(true);
                }
            }
        }
    }

    public void onGameOptionClicked(View view) {
        int newVisiblity = (gameOptions.getVisibility() != View.GONE) ? View.GONE : View.VISIBLE;
        int newIcon = (gameOptions.getVisibility() != View.GONE) ? R.drawable.collapsed
                : R.drawable.expanded;
        gameOptions.setVisibility(newVisiblity);
        ((TextView) view).setCompoundDrawablesRelativeWithIntrinsicBounds(newIcon, 0, 0, 0);
    }

    public void onCategoryClicked(View view) {
        int newVisiblity = (gameCategories.getVisibility() != View.GONE) ? View.GONE : View.VISIBLE;
        int newIcon = (gameCategories.getVisibility() != View.GONE) ? R.drawable.collapsed
                : R.drawable.expanded;
        gameCategories.setVisibility(newVisiblity);
        ((TextView) view).setCompoundDrawablesRelativeWithIntrinsicBounds(newIcon, 0, 0, 0);
    }

    public void onLevelClicked(View view) {
        int newVisiblity = (gameLevels.getVisibility() != View.GONE) ? View.GONE : View.VISIBLE;
        int newIcon = (gameLevels.getVisibility() != View.GONE) ? R.drawable.collapsed
                : R.drawable.expanded;
        gameLevels.setVisibility(newVisiblity);
        ((TextView) view).setCompoundDrawablesRelativeWithIntrinsicBounds(newIcon, 0, 0, 0);
    }

    public void OnPlayButtonClicked(View view) {
        try {
            Log.d(LOG_TAG, "Session: race-" + params.raceType + ", ctg-" + params.categoryType
                    + ", diff-" + params.difficulty);
            m_mainController.IFaceMultiPlayerButtonPress();
        } catch (JSONException exp) {
            exp.printStackTrace();
        }
    }
}
