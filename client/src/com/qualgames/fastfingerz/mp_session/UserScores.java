package com.qualgames.fastfingerz.mp_session;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import android.util.Log;

// TODO: Make this into an abstract class which can do same calculations but on Participant.gameScore and Participant.overallScore?
public abstract class UserScores {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    final static String TAG = "Fast Fingerz";

    protected ArrayList<Participant> m_sortedScoreList;

    protected abstract int GetScore(Participant p);
    
    public UserScores(ArrayList<Participant> participants)
    {
        m_sortedScoreList = participants;
        SortBasedOnScore();
    }

    // put all users in hash map 
    // TODO: Do we need this still?
    public void InitWithAllParticipants(ArrayList<Participant> participants)
    {
        Log.d(TAG, "UserScores::InitWithAllParticipants");

        m_sortedScoreList = participants;

        // add all users to sorted list just in case it needs to be accessed
        SortBasedOnScore();
    }

    // should be abstract - for all scores 
    protected void UpdateUser(Participant p)
    {
        Log.d(TAG, "UserScores::UpdateScoreForUser participantID " + p.id);
        m_sortedScoreList.remove(p);
        m_sortedScoreList.add(p);

        SortBasedOnScore();
    }


    public void RemoveUsers(Participant p)
    {
        Log.d(TAG, "UserScores::RemoveUsers");

        m_sortedScoreList.remove(p);

        SortBasedOnScore();
    }
    
    // this will sort the user score array list based on score (should be abstract based on overall score or game score)
    private void SortBasedOnScore()
    {
        Log.d(TAG, "UserScores::SortBasedOnScore");     
        // here we sort from largest to smallest for convenience... since this will be easier for other modules to deal with
        Collections.sort(m_sortedScoreList, new Comparator<Participant>()
        {
            @Override
            public int compare(Participant s1, Participant s2)
            {
                Log.d(TAG, "UserScores::sort s1.score " + GetScore(s1) + " s2.score " + GetScore(s2));
                if (GetScore(s1) > GetScore(s2))
                {
                    return -1;
                }
                if (GetScore(s1) < GetScore(s2))
                {
                    return 1;
                }
                return 0;
            }
        });
    }


    public ArrayList<Participant> GetSortedUserScoreList()
    {
        Log.d(TAG, "UserScores::GetSortedUserScoreList");
        return m_sortedScoreList;
    }


    public int GetMyRank()
    {
        Log.d(TAG, "UserScores::GetMyRank");
        return (m_sortedScoreList.indexOf(Participant.GetSelf()) + 1);
    }


    // returns limited UserScores based on participantIDs 
    // maybe later for elimination?
    /*
    public UserScores GetUserScoresSubset(List<String> participantIDs)
    {
        Log.d(TAG, "UserScores::GetUserScoresSubset");

        UserScores userScores = new UserScores();
        // Inefficient comparison, but it's ok since participantIDs will normally only be 1 and our set will be small
        for ( String participantID : participantIDs )
        {
            for ( Entry<String, UserScore> entry : this.entrySet() )
            {
                if( entry.getKey().equals(participantID) )
                {
                    userScores.put(entry.getKey(), entry.getValue());
                }
            }
        }

        return userScores;
    }
     */


    /*
    // this will add on the UserScores object to the existing one
    public void Concatenate(UserScores scoresToConcatenate)
    {
        Log.d(TAG, "UserScores::Concatenate");

        for (Entry<String, UserScore> entry : scoresToConcatenate.entrySet())
        {
            this.put(entry.getKey(), entry.getValue());
        }

        SortBasedOnScore();
    }
     */
}
