package com.qualgames.fastfingerz.mp_session.server_session.social_media;

import org.json.JSONException;

import com.qualgames.fastfingerz.mp_session.server_session.ServerSession.SignInStatusType;

public interface SocialMediaSessionStateListener {

    public void SignInStatus(SignInStatusType signInStatusType, String token) throws JSONException;
}
