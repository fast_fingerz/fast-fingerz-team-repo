package com.qualgames.fastfingerz.mp_session.server_session;

import java.net.URISyntaxException;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Handler;
import android.util.Log;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Ack;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

// this class will need to be a singelton I think
public class TCPSocket {

    private static final String TAG = "Fast Fingerz:: " + TCPSocket.class.getSimpleName() + " ";
    
    public static final String ACK = "ACK";

    private Socket m_socket = null;
    private boolean m_isConnected = false;
    private SocketStateListener m_stateListener;
    
    private Handler m_handler = new Handler();
    
    private class IncomingMessageRunnable implements Runnable 
    {
        public String event;
        public JSONObject data;
        SocketIncomingMessageListener listener;
        Ack ack = null;
        
        public IncomingMessageRunnable(String event, JSONObject data, SocketIncomingMessageListener listener) 
        {
            super();
            this.event = event;
            this.data = data;
            this.listener = listener;
        }
        
        public IncomingMessageRunnable(String event, JSONObject data, SocketIncomingMessageListener listener, Ack ack)
        {
            super();
            this.event = event;
            this.data = data;
            this.listener = listener;
            this.ack = ack;
        }
        
        @Override
        public void run() 
        {
            try {
                listener.HandleIncomingMessage(event, data);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            
            if (ack != null)
            {
                Log.d(TAG, "ACK it like it's hoooooooot for event " + event);
                ack.call();
            }
        }
    };
    
    
    /*
    private class IncomingMessageWithAckRunnable extends IncomingMessageRunnable
    {
        public Ack ack;
        public IncomingMessageWithAckRunnable(String event, JSONObject data, SocketIncomingMessageListener listener, Ack ack)
        {
            super(event, data, listener);
            this.ack = ack;
        }
        
        @Override 
        protected void HandleAction()
        {
            super.HandleAction();
            
            Log.d(TAG, "ACK it like it's hoooooooot for event " + event);
            ack.call();
        }
    };
    */

    public TCPSocket(SocketStateListener stateListener)
    {
        m_stateListener = stateListener;
    }

    public void OpenSocket(String token, String optionalName, int avatarID, String loginType, String serverURL) throws JSONException
    {
        IO.Options options = new IO.Options();

        options.forceNew = true;
        // TODO: This is hack for now
        options.query = "token=" + token + "&" + "logintype=" + loginType + "&" + "friendlyname=test_" + optionalName + "&" + "avatarid=" + avatarID;
        options.transports = new String[] {"websocket"};
        options.reconnection = false;

        Log.d(TAG, "Open scoket for " + serverURL + " with query " + options.query);

        try {
            m_socket = IO.socket(serverURL, options);
            // TODO: Handle all other socket error conditions
            m_socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() 
            {                
                @Override
                public void call(Object... args) 
                {
                    Log.d(TAG, "Socket connceted... great success!!!");
                    m_isConnected = true;
                    try {
                        m_stateListener.HandleSocketConnected();
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            });

            RegisterListenerForError(Socket.EVENT_DISCONNECT);
            RegisterListenerForError(Socket.EVENT_CONNECT_ERROR);
            RegisterListenerForError(Socket.EVENT_CONNECT_TIMEOUT);
            RegisterListenerForError(Socket.EVENT_ERROR);
            RegisterListenerForError(Socket.EVENT_RECONNECT_ERROR);
            RegisterListenerForError(Socket.EVENT_RECONNECT_FAILED);

            // open socket
            m_socket.connect();

        } catch (URISyntaxException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void Close()
    {
        m_socket.close();
    }

    public boolean IsConnected()
    {
        return m_isConnected;
    }

    public void RegisterListenerForIncomingMessage(final String event, final SocketIncomingMessageListener listener)
    {
        RegisterListenerForIncomingMessage(event, listener, false);
    }
    
    public void RegisterListenerForIncomingMessage(final String event, final SocketIncomingMessageListener listener, final boolean toAck)
    {
        m_socket.on(event, new Emitter.Listener() {

            @Override
            public void call(Object... args) {

                    Log.d(TAG, "Received " + event + " as JSON with contents " + ((JSONObject)args[0]).toString());
                    
                    // need to switch context to main thread
                    IncomingMessageRunnable incomingMessageRunnable = null;
                    if (toAck)
                    {
                        incomingMessageRunnable = new IncomingMessageRunnable(event, (JSONObject)args[0], listener, (Ack) args[args.length - 1]);
                    }
                    else
                    {
                        incomingMessageRunnable = new IncomingMessageRunnable(event, (JSONObject)args[0], listener);
                    }
                    m_handler.postDelayed(incomingMessageRunnable, 1);
                    
                    /*
                    listener.HandleIncomingMessage(event, (JSONObject)args[0]);
                    if (toAck)
                    {
                        // optional ack
                        Ack ack = (Ack) args[args.length - 1];
                        ack.call();
                    }
                    */
            }
        });
    }

    public void Send(final OutgoingMessage data) throws JSONException
    {
        m_socket.emit(data.GetEventName(), data.EncodeToJSON());
    }

    public void Send(final OutgoingMessage data, final SocketIncomingMessageListener ackListener) throws JSONException
    {
        Log.d(TAG, "Sending message " + data.GetEventName() + " with contents " + data.EncodeToJSON().toString());
        m_socket.emit(data.GetEventName(), data.EncodeToJSON(), new Ack() {

            @Override
            public void call(Object... args) {
                Log.d(TAG, "Received " + data.GetEventName() + " ACK with contents " + ((JSONObject)args[0]).toString());
                // need to switch context
                IncomingMessageRunnable incomingMessageRunnable = new IncomingMessageRunnable(data.GetEventName() + "ACK", (JSONObject)args[0], ackListener);
                m_handler.postDelayed(incomingMessageRunnable, 1);
                //ackListener.HandleIncomingMessage(data.GetEventName() + "ACK", (JSONObject)args[0]);
            }
        });
    }


    private void RegisterListenerForError(final String message)
    {
        m_socket.on(message, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d(TAG, args[0].toString());
                try {
                    m_stateListener.HandleSocketError(message);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });
    }

}
