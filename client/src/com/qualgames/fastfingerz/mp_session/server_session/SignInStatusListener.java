package com.qualgames.fastfingerz.mp_session.server_session;

import com.qualgames.fastfingerz.mp_session.server_session.ServerSession.SignInStatusType;
import com.qualgames.fastfingerz.mp_session.server_session.ServerSession.SignInType;

public interface SignInStatusListener {

    public void SignInError(SignInStatusType signInStatusType);
    public void SignInCompleted(SignInType signInType);
}
