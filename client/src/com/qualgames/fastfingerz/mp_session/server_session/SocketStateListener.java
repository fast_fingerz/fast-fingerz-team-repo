package com.qualgames.fastfingerz.mp_session.server_session;

import org.json.JSONException;

public interface SocketStateListener {

    public void HandleSocketConnected() throws JSONException;
    public void HandleSocketError(String errorEvent) throws JSONException;
}

