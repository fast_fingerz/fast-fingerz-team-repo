package com.qualgames.fastfingerz.mp_session.server_session;

import org.json.JSONException;
import org.json.JSONObject;

public class SimpleOutgoingMessage extends OutgoingMessage {

    public SimpleOutgoingMessage(String eventName) 
    {
        super(eventName);
    }

    @Override
    public JSONObject EncodeToJSON() throws JSONException {
        return new JSONObject("");
    }

}
