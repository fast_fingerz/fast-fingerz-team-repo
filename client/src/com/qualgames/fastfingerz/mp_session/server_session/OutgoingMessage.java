package com.qualgames.fastfingerz.mp_session.server_session;

import org.json.JSONException;
import org.json.JSONObject;

public abstract class OutgoingMessage {
    
    private String m_eventName;
    
    public OutgoingMessage(String eventName)
    {
        m_eventName = eventName;
    }
    
    public String GetEventName()
    {
        return m_eventName;
    }
    
    public abstract JSONObject EncodeToJSON() throws JSONException;
}
