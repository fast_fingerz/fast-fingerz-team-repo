package com.qualgames.fastfingerz.mp_session.server_session;

import org.json.JSONException;
import org.json.JSONObject;

public abstract class IncomingMessage {
    
    private String m_eventName;
    
    private JSONObject m_data;
    
    public IncomingMessage(String eventName, JSONObject json) throws JSONException
    {
        m_eventName = eventName;
        m_data = json;
    }
    
    public JSONObject GetData()
    {
        return m_data;
    }
    
    public String GetEventName()
    {
        return m_eventName;
    }

}
