package com.qualgames.fastfingerz.mp_session.server_session;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.qualgames.fastfingerz.mp_session.Participant;
import com.qualgames.fastfingerz.mp_session.controller.MP_SessionController;
import com.qualgames.fastfingerz.mp_session.server_session.social_media.SocialMediaSessionStateListener;


public class ServerSession implements SocketStateListener, SocialMediaSessionStateListener
{
    private final static String TAG = "Fast Fingerz::" + ServerSession.class.getSimpleName() + " ";
    // TODO: Put in string resources
    private final static String SERVER_URL = "http://fastfingerz.herokuapp.com/";
    
    private SignInStatusListener m_signInStatusListener;
    private SignInType m_signInType;
    private ServerSessionState m_state;
    
    private TCPSocket m_tcpSocket;
    
    public enum SignInType {
        NONE(""),
        USERID("userid"),
        FACEBOOK("facebook"),
        TWITTER("twitter"),
        GOOGLE_PLUS("google_plus");
        
        // TODO: Remove this and use Participant display name instead
        public String optionalName;
        public String token;
        public String value;
        
        private SignInType(String value)
        {
            this.value = value;
        }
    };
    
    public enum SignInStatusType
    {
        SUCCESS,
        NETWORK_ERROR,
        SOCIAL_MEDIA_SERVER_REJECT,     //FB, twitter rejected us
        SERVER_REJECT;  // our own server rejected us
    }
    
    private enum ServerSessionState {
        INIT,
        SOCIAL_MEDIA_SIGNING_IN,
        SOCIAL_MEDIA_SIGNED_IN_OPENING_SOCKET,
        NO_SOCIAL_MEDIA_OPENING_SOCKET,
        SOCIAL_MEDIA_SIGN_IN_ERROR,
        SIGNED_IN,
        SOCKET_ERROR
    }
    
    public ServerSession(SignInStatusListener listener) 
    {
        m_signInStatusListener = listener;
        
        // TODO: need to check sharedpreferences or similar cache if previous sign in happened...
        m_signInType = SignInType.NONE;
        m_state = ServerSessionState.INIT;
        m_tcpSocket = new TCPSocket(this);
    }
    
    
    
    public SignInType SignIn() throws JSONException 
    {
        Log.d(TAG, "SignIn");
        
        if (IsConnected())
        {
            // if we're already signed in and connected let listener know that we're signed in
            m_signInStatusListener.SignInCompleted(m_signInType);
            return m_signInType;
        }
        // check shared prefernces or some other cache for preferred options or if we're already signed in
        // if not return NONE
        if (/*check shared preferences for options and check socket status*/ false) 
        {
            m_signInType = SignInType.USERID;
            SignIn(m_signInType);
            return m_signInType;
        }
        return SignInType.NONE;
    }
    
    // TODO: Signing in will be a two step process... first social media then open socket
    public void SignIn(SignInType signInType) throws JSONException
    {
        Log.d(TAG, "SignIn with " + signInType);
        m_signInType = signInType;
        InitiateSignIn();
    }
    
    public void Close()
    {
        m_tcpSocket.Close();
    }
    
    
    public void RegisterForIncomingMessage(String event, SocketIncomingMessageListener listener)
    {
        RegisterForIncomingMessage(event, listener, false);
    }
    
    public void RegisterForIncomingMessage(String event, SocketIncomingMessageListener listener, boolean toAck)
    {
        Log.d(TAG, "RegisterForIncomingMessage");
        m_tcpSocket.RegisterListenerForIncomingMessage(event, listener, toAck);
    }
    
    public void Send(final OutgoingMessage data) throws JSONException
    {
        Log.d(TAG, "Send");
        m_tcpSocket.Send(data);
    }
    
    public void Send(final OutgoingMessage data, final SocketIncomingMessageListener ackListener) throws JSONException
    {
        Log.d(TAG, "Send and listen for ACK");
        m_tcpSocket.Send(data, ackListener);
    }
    
    public boolean IsConnected()
    {
        return m_state == ServerSessionState.SIGNED_IN;
    }
    
    @Override
    public void HandleSocketConnected() throws JSONException
    {
        m_state = ServerSessionState.SIGNED_IN;
        m_signInStatusListener.SignInCompleted(m_signInType);
    }

    @Override
    public void HandleSocketError(String errorEvent) throws JSONException
    {
        Log.d(TAG, "SocketError of some sort - " + errorEvent);
        m_state = ServerSessionState.SOCKET_ERROR;
    }
    
    // this will come only from social media session
    @Override
    public void SignInStatus(SignInStatusType signInStatusType, String token) throws JSONException 
    {
        // notify mp_sessionController if sign in to social media failed
        if ( signInStatusType != SignInStatusType.SUCCESS )
        {
            m_state = ServerSessionState.SOCIAL_MEDIA_SIGN_IN_ERROR;
            m_signInStatusListener.SignInError(signInStatusType);
        }
        else
        {
            // since we signed in to social media we can open socket to connect to our server
            m_state = ServerSessionState.SOCIAL_MEDIA_SIGNED_IN_OPENING_SOCKET;
            // get token from social media sign in
            m_signInType.token = token;
            OpenSocket();
        }
    }
    
    private void InitiateSignIn() throws JSONException 
    {
        Log.d(TAG, "InitiateSignIn");
        switch (m_signInType)
        {
            case USERID:
                m_state = ServerSessionState.NO_SOCIAL_MEDIA_OPENING_SOCKET;
                // TODO: Remove this hack
                m_signInType.token = "test_" + m_signInType.optionalName;
                OpenSocket();
                break;
            case FACEBOOK:
            case GOOGLE_PLUS:
            case TWITTER:
                m_state = ServerSessionState.SOCIAL_MEDIA_SIGNING_IN;
                break;
            default:
                break;
        }
    }
    
    // open socket with random user id
    private void OpenSocket() throws JSONException
    {
        Log.d(TAG, "OpenSocket");
        m_tcpSocket.OpenSocket(m_signInType.token, m_signInType.optionalName, 0/*read avatarid from shared preferences*/, m_signInType.value, SERVER_URL);
    }
}
