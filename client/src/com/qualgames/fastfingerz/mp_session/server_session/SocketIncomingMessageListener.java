package com.qualgames.fastfingerz.mp_session.server_session;

import org.json.JSONException;
import org.json.JSONObject;

public interface SocketIncomingMessageListener {

    public void HandleIncomingMessage(String event, JSONObject data) throws JSONException;
}
