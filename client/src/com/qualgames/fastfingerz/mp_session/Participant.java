package com.qualgames.fastfingerz.mp_session;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.qualgames.fastfingerz.minigame.Statistics;
import com.qualgames.fastfingerz.minigame.controller.MinigameController;

public class Participant {
    
    private static final String TAG = "FastFingerz::Participant";
    
    public final static String HACK_ID = "1239";
    
    // name displayed to everyone
    public String displayName = "";
    // id for server to know
    public String id = "";
    // score of current minigame
    public int gameScore = 0;
    // total score so far
    public int totalScore = 0;
    // is self
    public boolean isSelf = false;
    // TODO: Add avatar id
    public int avatarID = 0;
    // is eliminated
    public boolean isEliminated = false;
    // how did user finish last race
    public MinigameController.RaceFinishedStatus raceFinishedStatus;
    // time elapsed to finish the race
    public long timeElapsed;
    // stats
    public Statistics stats;
    // finished position
    public int finishedPosition;
    
    
    // this is a static id only used to identify self... kinda hacky but easier to get an object of myself
    public static String my_id = "test_" + HACK_ID;
    
    public static Participant GetSelf()
    {
        return new Participant(true);
    }
    
    private Participant(boolean isSelf)
    {
        this.isSelf = isSelf;
        if (isSelf)
        {
            // TODO: read properties from shared preferences
            this.id = my_id;
            // TODO: Remove this hacky hack
            this.displayName = "test_" + HACK_ID;
        }
    }
    
    private enum MessageProtocol
    {
        DSIPLAY_NAME("displayName"),
        ID("id"),
        AVATAR_ID("avatarId");
        
        public String string;
        
        private MessageProtocol(String string)
        {
            this.string = string;
        }
    }
    
    public Participant(String id)
    {
        this.id = id;
    }
    
    public Participant(String displayName, String id, int avatarID)
    {
        this.displayName = displayName;
        this.id = id;
        this.avatarID = avatarID;
    }
    
    public Participant(String displayName, String id, int avatarID, boolean isSelf)
    {
        this.displayName = displayName;
        this.id = id;
        this.isSelf = isSelf;
        this.avatarID = avatarID;
    }
    
    
    public JSONObject EncodeToJSON() throws JSONException
    {
        JSONObject json = new JSONObject();
        
        json.put("displayName", displayName);
        json.put("id", id);
        json.put("avatarId", avatarID);
        
        return json;
    }
    
    public static Participant GenerateFromData(JSONObject json) throws JSONException
    {
        Participant p = new Participant(json.getString(MessageProtocol.DSIPLAY_NAME.string), json.getString(MessageProtocol.ID.string), json.getInt(MessageProtocol.AVATAR_ID.string));
        Log.d(TAG, "Generate Participant with id " + p.id);
        return p;
    }
    
    public static ArrayList<Participant> GenerateFromData(JSONArray jsonArray) throws JSONException
    {
        ArrayList<Participant> pArray = new ArrayList<Participant>();
        for (int i = 0; i < jsonArray.length(); i++)
        {
            pArray.add(Participant.GenerateFromData(jsonArray.getJSONObject(i)));
        }
        
        return pArray;        
    }
    
    @Override
    public boolean equals(Object p)
    {
        if (p == null) 
            return false;
        if (p == this) 
            return true;
        if (!(p instanceof Participant)) 
            return false;
        
        Participant participant = (Participant) p;
        return participant.id.equals(this.id);
    }

}
