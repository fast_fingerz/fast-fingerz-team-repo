package com.qualgames.fastfingerz.mp_session.view;
import java.util.ArrayList;

import org.json.JSONException;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.WindowManager;

import com.qualgames.fastfingerz.R;
import com.qualgames.fastfingerz.minigame.GameType;
import com.qualgames.fastfingerz.minigame.controller.MP_MinigameController;
import com.qualgames.fastfingerz.minigame.model.MinigameModel;
import com.qualgames.fastfingerz.minigame.view.MinigameScreen;
import com.qualgames.fastfingerz.mp_session.Participant;
import com.qualgames.fastfingerz.mp_session.SessionParams;
import com.qualgames.fastfingerz.mp_session.SessionParams.Mode;
import com.qualgames.fastfingerz.mp_session.SessionParams.RaceType;
import com.qualgames.fastfingerz.mp_session.UserScores;
import com.qualgames.fastfingerz.mp_session.controller.MP_SessionController;
import com.qualgames.fastfingerz.mp_session.server_session.ServerSession.SignInType;

public class MP_SessionActivity extends FragmentActivity {

	// For logging
	final static String TAG = "Fast Fingerz::" + MP_SessionActivity.class.getSimpleName() + " ";
	
	private MP_SessionController m_mpSessionController;
	private WaitingRoom m_waitingRoom;
	private ArrayList<Participant> m_participants;
	private CurrentScreen m_currentScreen;
	
	private enum CurrentScreen
	{
	    SIGN_IN,
	    WAITING_ROOM,
	    SPLASH_COUNTDOWN,
	    MINIGAME,
	    LEADERBOARD
	}
	
	
	@Override
    public void onCreate(Bundle savedInstanceState) 
	{		
		Log.d(TAG, "MP_SessionActivity onCreate");
		
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mp_session_layout);
        
        // keep screen on
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        
        // TODO: Make session params parcelable
        SessionParams sessionParams = (SessionParams)getIntent().getParcelableExtra("SessionParams");
        // HACK: Remove this later
        sessionParams = new SessionParams();
        sessionParams.gameType = GameType.SWIPY;
        sessionParams.mode = Mode.MULTIPLAYER_STANDARD;
        sessionParams.raceType = RaceType.SINGLE_RACE;
        // HACK: Remove this later

        m_mpSessionController = new MP_SessionController(this, sessionParams);
        
        m_currentScreen = CurrentScreen.SIGN_IN;
        
        // initiate signing in
        try {
            m_mpSessionController.SignInAuto();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
	
	@Override
	public void onDestroy()
	{
	    super.onDestroy();
	    finish();
	    m_mpSessionController.CloseSession();
	}
	
	@Override
	public void onBackPressed()
	{
	    super.onBackPressed();
	    finish();
	    m_mpSessionController.CloseSession();
	}
	
	
	/******************** SIGN IN ***************************************/
	public void DisplaySignInOptions() throws JSONException
	{
	    // launch sign in pop up
	    // TODO: HACK to test out get rid of this later
	    SignInType signInType = SignInType.USERID;
	    signInType.optionalName = Participant.HACK_ID;
	    m_mpSessionController.SignInUserOption(signInType);
	}
	
	   
    public void SignedIn(SignInType signInType)
    {
        // hide pop up or something
    }
    /******************** SIGN IN ***************************************/
    
	
	/********************* WAITING ROOM *********************************/
	public void DisplayWaitingRoom(ArrayList<Participant> participants)
	{
	    m_currentScreen = CurrentScreen.WAITING_ROOM;
	    RemoveCurrentFragment();
        // let's instantiate the fragment
        m_waitingRoom = new WaitingRoom(participants);
	    // let's put the waiting room fragment into the container
	    getSupportFragmentManager().beginTransaction().add(R.id.full_screen_container, m_waitingRoom).commit();
	    // pass the users
	    UpdateRoom(participants);
	}
		
	public void UpdateRoom(ArrayList<Participant> participants)
	{
	    m_participants = participants;
	    if (m_currentScreen == CurrentScreen.WAITING_ROOM)
	    {
	        m_waitingRoom.UpdateUsersInRoom(participants);
	    }
	}
	
	public void RoomReadyToPlay()
	{
	    // show a spinner or something here
	}
	/********************* WAITING ROOM *********************************/
	
	
	public void DisplayCountdownSplashScreen(long timerValue)
	{
	    m_currentScreen = CurrentScreen.SPLASH_COUNTDOWN;
	    // launch countdown screen with timerValue
	    CountdownSplashScreen displayScreen = new CountdownSplashScreen(timerValue, m_mpSessionController);
	    // remove current fragment
	    RemoveCurrentFragment();
	    // let's put the countdown room fragment into the container
	    getSupportFragmentManager().beginTransaction().add(R.id.full_screen_container, displayScreen).commit();
	}
	
	
	public void DisplayMinigame(MinigameModel minigameModel)
	{
	    // launch MinigameActivityFragment to take over
	    m_currentScreen = CurrentScreen.MINIGAME;
	    
	    RemoveCurrentFragment();
	    MinigameScreen minigameScreen = new MinigameScreen(minigameModel, new MP_MinigameController(minigameModel, m_mpSessionController));
	    // let's put the minigame screen fragment into the container
        getSupportFragmentManager().beginTransaction().add(R.id.full_screen_container, minigameScreen).commit();
	}
	
	
	public void DisplayLeaderboard(UserScores gameScores, UserScores totalScores)
	{
	    // this is in between games
	}
	

	public void HandleNetworkError(String message)
	{
	    
	}
	
	public void Close() {
	    finish();
	}
	
	
	private void RemoveCurrentFragment()
	{
	    if (getSupportFragmentManager().findFragmentById(R.id.full_screen_container) != null)
	    {
	        getSupportFragmentManager().beginTransaction().remove(getSupportFragmentManager().findFragmentById(R.id.full_screen_container)).commit();
	    }
	}
}
