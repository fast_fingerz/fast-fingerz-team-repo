package com.qualgames.fastfingerz.mp_session.view;

import java.util.ArrayList;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.qualgames.fastfingerz.R;
import com.qualgames.fastfingerz.mp_session.Participant;

public class WaitingRoom extends Fragment {
	
	// For logging
	final static String TAG = "Fast Fingerz::" + WaitingRoom.class.getSimpleName();
	
	private ArrayList<Participant> m_participants;
	
	public WaitingRoom(ArrayList<Participant> participants)
	{
	    m_participants = participants;
	}
		
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, 
            Bundle savedInstanceState) {
        
        Log.d(TAG, "onCreateView");
        
        View view = inflater.inflate(R.layout.waiting_room_fragment_layout, container, false);
        
        return view;
    }
	
	
	public void UpdateUsersInRoom(ArrayList<Participant> participants)
	{
		m_participants = participants;
	}
	
}
