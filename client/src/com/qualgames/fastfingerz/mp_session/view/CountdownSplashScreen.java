package com.qualgames.fastfingerz.mp_session.view;

import org.json.JSONException;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.qualgames.fastfingerz.R;
import com.qualgames.fastfingerz.mp_session.controller.MP_SessionController;

public class CountdownSplashScreen extends Fragment {
    
    // For logging
    final static String TAG = "Fast Fingerz::" + CountdownSplashScreen.class.getSimpleName();
    
    private Handler m_updateCountdownHandler = new Handler();
    
    private long m_countdownInMSecs;
    private MP_SessionController m_sessionController;
    
    public CountdownSplashScreen(long countdownInMSecs, MP_SessionController sessionController)
    {
        super();
        m_countdownInMSecs = countdownInMSecs;
        m_sessionController = sessionController;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        
        Log.d(TAG, "onCreateView");
        View view = inflater.inflate(R.layout.countdown_splash_screen_layout, container, false);
        
        // do the needy
        m_updateCountdownHandler.postDelayed(m_updateCountdownThread, 1);
        
        return view;
    }
    
    
    private Runnable m_updateCountdownThread = new Runnable() 
    {
        public void run()
        {
            Log.d(TAG, "updateCountdown");
            // this will ensure that the countdown value is rounded down
            Log.d(TAG, "m_countdownInMsecs - " + m_countdownInMSecs);
            
            TextView scoreView = (TextView) getActivity().findViewById(R.id.countdown_in_secs);
            scoreView.setText(String.valueOf((int)(m_countdownInMSecs/1000)));
            
            // if we are down to 0
            if (m_countdownInMSecs <= 0)
            {
                try {
                    m_sessionController.MinigameCountdownComplete();
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            else
            {
                // decrease countdown by 1 sec
                m_countdownInMSecs -= 1000;
                // schedule next event
                m_updateCountdownHandler.postDelayed(m_updateCountdownThread, 1000);
            }
        }
    };
}
