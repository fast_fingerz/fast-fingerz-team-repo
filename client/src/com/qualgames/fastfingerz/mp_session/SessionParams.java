package com.qualgames.fastfingerz.mp_session;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.qualgames.fastfingerz.R;
import com.qualgames.fastfingerz.minigame.GameType;

public class SessionParams {
    
    private final static String TAG = SessionParams.class.getSimpleName();
    
    public enum Mode
    {
        MULTIPLAYER_STANDARD(0),
        MULTIPLAYER_ELIMINATION(1),
        HEAD_TO_HEAD(2);
        
        public int value;
        private Mode(int value) {
            this.value = value;
        }
    }
    
    public enum Difficulty
    {
        UNDEFINED(0, 0),
        EASY(1, R.string.difficultyl_easy),
        MEDIUM(2, R.string.difficulty_medium),
        HARD(3, R.string.difficulty_hard);
        
        public int value;
        public int captionId;
        private Difficulty(int value, int captionId) {
            this.value = value;
            this.captionId = captionId;
        }
    }
    
    public enum RaceType
    {
        SINGLE_RACE(0, R.string.race_type_single),
        CUP(1, R.string.race_type_multi);
        
        public int value;
        public int captionId;
        private RaceType(int value, int captionId) {
            this.value = value;
            this.captionId = captionId;
        }
    }
    
    public enum CategoryType
    {
        MATH(0, R.string.category_math),
        MEMORY(1, R.string.category_memory),
        SPEED(2, R.string.category_speed),
        TRIVIA(3, R.string.category_trivia),
        // add more categories here
        BRAIN(99, R.string.category_brain);  // this means everything!
        
        public int value;
        public int captionId;
        private CategoryType(int value, int captionId) {
            this.value = value;
            this.captionId = captionId;
        }
    }
    
    public Mode mode = Mode.MULTIPLAYER_STANDARD;
    public Difficulty difficulty = Difficulty.EASY;
    public RaceType raceType = RaceType.CUP;
    public GameType gameType = GameType.SWIPY; // which specific minigame are we interested in?
    public CategoryType categoryType = CategoryType.MATH;
    
    public JSONObject EncodeToJSON() throws JSONException
    {
        JSONObject json = new JSONObject();
        json.put("difficulty", difficulty.value);
        json.put("raceType", raceType.value);
        
        if (raceType == RaceType.SINGLE_RACE)
        {
            json.put("gameType", gameType.value);
        }
        
        else if (raceType == RaceType.CUP)
        {
            json.put("mode", mode.value);
            json.put("category", categoryType.value);
        }
        else
        {
            Log.d(TAG, "ERROR!!! No race type defined!");
        }
        
        return json;
    }

}
