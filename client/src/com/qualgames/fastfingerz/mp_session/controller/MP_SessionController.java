package com.qualgames.fastfingerz.mp_session.controller;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.os.Handler;
import android.util.Log;

import com.qualgames.fastfingerz.GlobalDataContainer;
import com.qualgames.fastfingerz.minigame.controller.MP_MinigameController;
import com.qualgames.fastfingerz.minigame.controller.MinigameController;
import com.qualgames.fastfingerz.minigame.model.MinigameModel;
import com.qualgames.fastfingerz.minigame.swipy.model.SwipyMinigameModel;
import com.qualgames.fastfingerz.mp_session.Participant;
import com.qualgames.fastfingerz.mp_session.RaceUserScores;
import com.qualgames.fastfingerz.mp_session.SessionParams;
import com.qualgames.fastfingerz.mp_session.TotalUserScores;
import com.qualgames.fastfingerz.mp_session.UserScores;
import com.qualgames.fastfingerz.mp_session.controller.Room.RoomUpdateListener;
import com.qualgames.fastfingerz.mp_session.controller.RoomEventPayload.RoomEventType;
import com.qualgames.fastfingerz.mp_session.server_session.ServerSession;
import com.qualgames.fastfingerz.mp_session.server_session.ServerSession.SignInStatusType;
import com.qualgames.fastfingerz.mp_session.server_session.ServerSession.SignInType;
import com.qualgames.fastfingerz.mp_session.server_session.SignInStatusListener;
import com.qualgames.fastfingerz.mp_session.server_session.SocketIncomingMessageListener;
import com.qualgames.fastfingerz.mp_session.view.CountdownSplashScreen;
import com.qualgames.fastfingerz.mp_session.view.MP_SessionActivity;

// TODO: add logging to all me

public class MP_SessionController implements SocketIncomingMessageListener, RoomUpdateListener, SignInStatusListener
{	
	// For logging
	final static String TAG = "Fast Fingerz";
	
	final int MIN_OPPONENTS = 1, MAX_OPPONENTS = 5, NUM_MINIGAMES = 8;
	final int DELTA_BETWEEN_GAMES_IN_MSECS = 5000;
	final String MINIGAME_START_TIME_STR = "MinigameStartTime";
	final int TIME_TO_NEXT_GAME_STARTING_SCORE_CHECK = 5000;
	final int TIME_TO_NEXT_GAME_STARTING_COUNTDOWN = 5000;
	final int LEAVE_GAME_DELAY = 500;
	
	private MP_SessionControllerState m_state;
	private MP_SessionCurrentScreen m_curScreen;
	
	private Room m_room;
	private ServerSession m_serverSession;
	private SessionParams m_sessionParams;
	
	private int m_currentMinigameIndex;
	private int m_numGames;
	private ArrayList<MinigameModel> m_gameData;
	
	// set default to true but may change in future
	private boolean m_bIsEliminationGame = true;
	// determines whether the leave room event was expected or a user left game unexpectedly
	private boolean m_bIsExpectedLeave = false;
	// if only two users are left in room then it is final game
	private boolean m_bIsFinalGame = false;
	
	// the place in which we finished the game
	private int m_myPlaceFinish;
	
	// list of participants for scoring
	private ArrayList<Participant> m_participants;
	
	private TotalUserScores m_overallUserScores, m_eliminatedUsersScores;
	private RaceUserScores m_lastMinigameUserScores;
	// TODO: Get rid of UserScore class... this is basically Participant class
	private Participant m_finalUpdateUserScore;
	
	private MP_SessionActivity m_activity;
	
	private class EventPayload {
	  
	    public SignInType signInType;
	    public SignInStatusType signInStatusType;
	    public ArrayList<Participant> participantList;
	    public Participant participant;
	    public String message;
	    public JSONObject json;
	    public RaceUserScores raceUserScores;
	};
	
	/**************************** EVENTS FOR ACTIVITIES ******************************/
	public static final String INCOMING_CONTROLLER_EVENT = "INCOMING_CONTROLLER_EVENT";
	public static final String MSECS_COUNTDOWN = "MSECS_TO_COUNTDOWN";
	public enum MP_SessionControllerToActivityEvent
	{
		UPDATE_ROOM,
		UPDATE_SCORE,
		CLOSE_ACTIVITY;
	}
	
	// enum for controller state
	private enum MP_SessionControllerState
	{
		INIT,
		SIGNING_IN,
		JOINING_ROOM,
		WAITING_FOR_PLAYERS,
		SYNCING_GAME_DATA,
		WAITING_FOR_START_GAME,
		IN_BETWEEN_GAMES_WAITING_FOR_START_GAME,  // waiting for start_game msg from server
		IN_BETWEEN_GAMES_COUTNDOWN,   // after receiving start_game msg, set a countdown until new game starts
		PLAYING_GAME,
		PLAYING_GAME_COUNTDOWN;	
	}
	
	// enum for controller events
	private enum MP_SessionControllerEvent
	{
		SIGN_IN_AUTO,  // try to sign in from preferences
		SIGN_IN_USER_OPTION,   // user tells us which form of sign in is preferred
		SIGN_IN_ERROR,
		SIGN_IN_SUCCESS,
		ROOM_JOINED,
		ROOM_USER_JOINED,
		ROOM_USER_LEFT,
		ROOM_READY_TO_PLAY,
		ROOM_ERROR,
		LEFT_ROOM,
		LEAVE_GAME,
		INCOMING_MESSAGE,
		GAME_DATA_SYNCED,
		MINIGAME_COUNTDOWN_COMPLETE,
		MINIGAME_COMPLETE;
	}
	
	// enum for current screen
	private enum MP_SessionCurrentScreen
	{
	    SIGN_IN,
		WAITING_ROOM,
		COUNTDOWN_TO_GAME,		// TODO: Leaderboard and countdown to game can be same screen
		MINIGAME,
		LEADERBOARD;
	}
	
	// for receiving/sending messages
	private enum IncomingMessageType
    {
        GAME_DATA("game_data"),
        GAME_START("game_start"),
        // TODO: Get rid of this... MP_MinigameControl
        GAME_END("game_end");
        
        public String string;
        
        private IncomingMessageType(String string)
        {
            this.string = string;
        }
    }
	
	private enum MessageProtocol
	{	    
	    GAME_DATA_STR("gameDataArray");
        
        public String string;
        
        private MessageProtocol(String string)
        {
            this.string = string;
        }
	}
	
	public MP_SessionController(MP_SessionActivity activity, SessionParams sessionParams) 
	{
	    Log.d(TAG, "MP_SessionController constructor");
        m_activity = activity;
        m_sessionParams = sessionParams;
        Init();
	}

	
	private void Init()
	{
		Log.d(TAG, "MP_SessionController Init");
		m_state = MP_SessionControllerState.INIT;
		m_curScreen = MP_SessionCurrentScreen.SIGN_IN;
		m_currentMinigameIndex = 0;
		m_bIsFinalGame = false;
		m_bIsExpectedLeave = false;
		
		m_serverSession = new ServerSession(this);
		m_room = new Room(m_serverSession, this);
	}
	
	/**************************** SIGN IN FUNCTIONS **********************************/
	// attempt to sign based on saved preferences if any
	public void SignInAuto() throws JSONException 
	{
	    Log.d(TAG, "MP_SessionController SignIn");
	    HandleControllerEvent(MP_SessionControllerEvent.SIGN_IN_AUTO, null);
	}
	
	// sign in with user selected option
	public void SignInUserOption(SignInType signInType) throws JSONException
	{
	    Log.d(TAG, "MP_SessionController UserInitiatedSignIn");
	    
	    EventPayload payload = new EventPayload();
	    payload.signInType = signInType;
	    
	    HandleControllerEvent(MP_SessionControllerEvent.SIGN_IN_USER_OPTION, payload);
	}
	
	
	public void CloseSession()
	{
	    Log.d(TAG, "MP_SessionController CloseSession");
	    m_serverSession.Close();
	}
	
    @Override
    public void SignInError(SignInStatusType signInStatusType) 
    {
        Log.d(TAG, "MP_SessionController SignInError");
        
        EventPayload payload = new EventPayload();
        payload.signInStatusType = signInStatusType;
        
        try {
            HandleControllerEvent(MP_SessionControllerEvent.SIGN_IN_ERROR, payload);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } 
    }

    @Override
    public void SignInCompleted(SignInType signInType) 
    {
        Log.d(TAG, "MP_SessionController SignInCompleted");
        
        EventPayload payload = new EventPayload();
        payload.signInType = signInType;
        
        try {
            HandleControllerEvent(MP_SessionControllerEvent.SIGN_IN_SUCCESS, payload);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }         
    }
	/**************************** SIGN IN FUNCTIONS **********************************/

	
	
	/**************************** HANDLE INCOMING DATA **********************************/
    @Override
    public void HandleIncomingMessage(String event, JSONObject message) 
    {
        Log.d(TAG, "MP_SessionController HandleIncomingMessage");
        
        EventPayload payload = new EventPayload();
        payload.json = message;
        payload.message = event;
        
        try {
            HandleControllerEvent(MP_SessionControllerEvent.INCOMING_MESSAGE, payload);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    /**************************** HANDLE INCOMING DATA **********************************/
    
    /**************************** HANDLE ROOM EVENTS **********************************/
    @Override
    public void HandleUserJoined(Participant p) 
    {
        Log.d(TAG, "MP_SessionController HandleUserJoined");
        
        EventPayload payload = new EventPayload();
        payload.participant = p;
        
        try {
            HandleControllerEvent(MP_SessionControllerEvent.ROOM_USER_JOINED, payload);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void HandleUserLeft(Participant p) 
    {
        Log.d(TAG, "MP_SessionController HandleUserLeft");
        
        EventPayload payload = new EventPayload();
        payload.participant = p;
        
        try {
            HandleControllerEvent(MP_SessionControllerEvent.ROOM_USER_LEFT, payload);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    @Override
    public void HandleRoomDestroyed() 
    {
        Log.d(TAG, "MP_SessionController HandleRoomDestroyed");        
    }

    @Override
    public void HandleRoomJoined(ArrayList<Participant> participantsInRoom) 
    {
        Log.d(TAG, "MP_SessionController HandleRoomJoined");
        
        EventPayload payload = new EventPayload();
        payload.participantList = participantsInRoom;
        
        try {
            HandleControllerEvent(MP_SessionControllerEvent.ROOM_JOINED, payload);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }        
    }

    @Override
    public void HandleRoomReady(ArrayList<Participant> participantsInRoom) 
    {
        Log.d(TAG, "MP_SessionController HandleRoomReady");
        
        EventPayload payload = new EventPayload();
        payload.participantList = participantsInRoom;
        
        try {
            HandleControllerEvent(MP_SessionControllerEvent.ROOM_READY_TO_PLAY, payload);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void HandleUnexpectedError(String message) 
    {
        Log.d(TAG, "MP_SessionController HandleRoomReady");
        
        EventPayload payload = new EventPayload();
        payload.message = message;
        
        try {
            HandleControllerEvent(MP_SessionControllerEvent.ROOM_ERROR, payload);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
    }
    /**************************** HANDLE ROOM EVENTS **********************************/
    
    
	/**************************** INTERFACE FOR CONTROLLERS ******************************/
	
	
	public void SetGameData(ArrayList<MinigameModel> gameData, int numGames)
	{
		Log.d(TAG, "MP_SessionController SetGameData");
		m_gameData = gameData;
		m_numGames = numGames;
	}
	
	
	public void GameDataSynced() throws JSONException
	{
		Log.d(TAG, "MP_SessionController GameDataSynced");
		HandleControllerEvent(MP_SessionControllerEvent.GAME_DATA_SYNCED, null);
	}
	
	
	public void MinigameCountdownComplete() throws JSONException
	{
		Log.d(TAG, "MP_SessionController MinigameCountdownComplete");
		HandleControllerEvent(MP_SessionControllerEvent.MINIGAME_COUNTDOWN_COMPLETE, null);
	}
	
	
	// MP_MinigameController has notified that minigame is over with the final scores
	public void MinigameComplete(RaceUserScores finalUserScores) throws JSONException
	{
	    Log.d(TAG, "MP_SessionController MinigameComplete");
	    
	    EventPayload payload = new EventPayload();
	    payload.raceUserScores = finalUserScores;
	    HandleControllerEvent(MP_SessionControllerEvent.MINIGAME_COMPLETE, payload);	    
	}
	
	// Activities will call this to get latest scores
	public UserScores GetOverallUserScores()
	{
		return m_overallUserScores;
	}
	
	// Activities will call this to get scores for last minigame
	public UserScores GetLastMinigameUserScores()
	{
		return m_lastMinigameUserScores;
	}
	
	public ArrayList<Participant> GetParticipants() 
	{
	    if (m_participants == null)
        {
            Log.d(TAG, "How is this shite null?!!");
        }
	    return m_participants;
	}
	
	public ServerSession GetServerSession()
	{
	    return m_serverSession;
	}
	
	// the user has chosen to quit the game
	public void LeaveGame() throws JSONException
	{
	    Log.d(TAG, "MP_SessionController LeaveGame");
	    HandleControllerEvent(MP_SessionControllerEvent.LEAVE_GAME, null);
	}
	
	// this callback indicates that user has actually left the room
	public void LeftRoom() throws JSONException
	{
		Log.d(TAG, "MP_SessionController LeftRoom");
		HandleControllerEvent(MP_SessionControllerEvent.LEFT_ROOM, null);
	}
	
	/**************************** PROCESS EVENTS INTERNALLY ******************************/
	// determine if this event came in a correct state and should be handled
	// TODO: Revisit this class after all the changes have been made
	private boolean IsExpectedControllerEvent(MP_SessionControllerEvent event)
	{
		Log.d(TAG, "MP_SessionController IsExpectedControllerEvent");
		boolean bIsExpectedEvent = true;
		
		switch (m_state)
		{
			case INIT:
				if ( event != MP_SessionControllerEvent.SIGN_IN_AUTO )
					bIsExpectedEvent = false;
				break;
				
			case WAITING_FOR_PLAYERS:
				if ( event == MP_SessionControllerEvent.MINIGAME_COUNTDOWN_COMPLETE ||
					 event == MP_SessionControllerEvent.GAME_DATA_SYNCED )
					bIsExpectedEvent = false;
				break;
			
			case SYNCING_GAME_DATA:
				if ( event == MP_SessionControllerEvent.MINIGAME_COUNTDOWN_COMPLETE )
					bIsExpectedEvent = false;
				break;
				
			case PLAYING_GAME:
			case PLAYING_GAME_COUNTDOWN:
			case WAITING_FOR_START_GAME:
				if ( event == MP_SessionControllerEvent.ROOM_READY_TO_PLAY )
					bIsExpectedEvent = false;
				break;
			case SIGNING_IN:
			    break;
			default:
			    break;
		}
		
		if ( !bIsExpectedEvent )
		{
			Log.d(TAG, "MP_SessionController:: Unexpected event " + event + "in state " + m_state);
			// should we throw an error to MainController in order to recover?
		}
		
		return bIsExpectedEvent;
	}
	
	// Handle internal event
	private void HandleControllerEvent(MP_SessionControllerEvent event, EventPayload payload) throws JSONException
	{
	    Log.d(TAG, "MP_SessionController::HandleControllerEvent event " + event);
	    if (!IsExpectedControllerEvent(event))
	        return;

	    switch (event)
	    {
	    case INCOMING_MESSAGE:
	        ProcessIncomingMessage(payload.message, payload.json);
	        break;
	    case GAME_DATA_SYNCED:
	        ProcessGameDataSyncedEvent();
	        break;
	    case MINIGAME_COUNTDOWN_COMPLETE:
	        ProcessMiniGameCountdownCompleteEvent();
	        break;
	    case MINIGAME_COMPLETE:
	        ProcessMinigameComplete(payload.raceUserScores);
	        break;
	    case LEFT_ROOM:
	        ProcessLeftRoomEvent();
	        break;
	    case LEAVE_GAME:
	        ProcessLeaveGameEvent();
	        break;
	    case SIGN_IN_AUTO:
	        ProcessSignInEvent();
	        break;
	    case SIGN_IN_USER_OPTION:
	        ProcessUserInitiatedSignIn(payload.signInType);
	        break;
	    case SIGN_IN_ERROR:
	        ProcessUserSignInError(payload.signInStatusType);
	        break;
	    case SIGN_IN_SUCCESS:
	        ProcessUserSignInSuccess(payload.signInType);
	        break;
	    case ROOM_JOINED:
	        ProcessRoomJoined(payload.participantList);
	        break;
	    case ROOM_READY_TO_PLAY:
	        ProcessRoomReadyToPlayEvent(payload.participantList);
	        break;
	    case ROOM_USER_JOINED:
	        ProcessRoomUserJoined(payload.participant);
	        break;
	    case ROOM_USER_LEFT:
	        ProcessRoomUserLeft(payload.participant);
	        break;
       case ROOM_ERROR:
            ProcessRoomErrorEvent(payload.message);
            break;
	    default:
	        break;
	    }
	}
	
	
	/****************************************** INCOMING MESSAGE *********************************************/
	private void ProcessIncomingMessage(String event, JSONObject data) throws JSONException
	{
	    if (event.equals(IncomingMessageType.GAME_DATA.string))
	    {
	        ProcessGameDataMessage(data);
	    }
	    else if (event.equals(IncomingMessageType.GAME_START.string))
	    {
	        ProcessGameStartMessage(data);
	    }
	    else if (event.equals(IncomingMessageType.GAME_END.string))
	    {
	        ProcessGameEndMessage(data);
	    }
	    else
	    {
	        Log.d(TAG, "Unexepcted incoming message received " + event + "in state " + m_state);
	    }
	}
	
	private void ProcessGameDataMessage(JSONObject data) throws JSONException
	{
	    //SwipyMinigameModel minigameModel = new SwipyMinigameModel();
	    //Log.d(TAG, minigameModel.EncodeToJSON().toString());
	    // this is all our game data
        m_gameData = MinigameController.GenerateGameData(data.getJSONArray(MessageProtocol.GAME_DATA_STR.string));
        // this is the number of games
        m_numGames = m_gameData.size();
        // change state
        m_state = MP_SessionControllerState.WAITING_FOR_START_GAME;
	}
	
	private void ProcessGameStartMessage(JSONObject data)
    {
	    // display the countdown screen
        // TODO: Decide if server tell us the countdown value?
	    Log.d(TAG, "ProcessGameStartMessage in state " + m_state);
	    if (m_state == MP_SessionControllerState.WAITING_FOR_START_GAME)
	    {
            m_activity.DisplayCountdownSplashScreen(DELTA_BETWEEN_GAMES_IN_MSECS);
            // change state
            m_state = MP_SessionControllerState.PLAYING_GAME_COUNTDOWN;
	    }
	    else if (m_state == MP_SessionControllerState.IN_BETWEEN_GAMES_WAITING_FOR_START_GAME)
	    {
	        // TODO: Figure out what to do in between games here
	    }
    }
	
	private void ProcessGameEndMessage(JSONObject data)
    {
        // this means that the game timer expired or all participants (including myself) have finished the race
    }
	/****************************************** INCOMING MESSAGE *********************************************/
	
	private void ProcessMinigameComplete(RaceUserScores finalUserScores) 
	{
	    m_lastMinigameUserScores = finalUserScores;
	    // add to the overall scores
	    m_overallUserScores.Add(m_lastMinigameUserScores.GetSortedUserScoreList());
	    
	    // will need to notify in between games screen
	    // change state to wait for next start_game msg
	    m_state = MP_SessionControllerState.IN_BETWEEN_GAMES_WAITING_FOR_START_GAME;
	    // here we will need to wait a different amount of time from the time we get the start_msg to playing (different value than playing first time from waiting room screen)
	}
	
	private void ProcessLeaveGameEvent()
    {
        Log.d(TAG, "MP_SessionController ProcessLeaveGameEvent");
    }
	
   // the game is unexpectedly over due to network error... close the active activity (with popup msg or something) and return to main screen
    private void ProcessRoomErrorEvent(String message)
    {
        Log.d(TAG, "MP_SessionController ProcessRoomErrorEvent");
        // this will make activity display a pop up and kill itself when user presses ok... something like that
        m_activity.HandleNetworkError(message);
    }
	
	private void ProcessRoomReadyToPlayEvent(ArrayList<Participant> participants) throws JSONException
	{
        Log.d(TAG, "MP_SessionController ProcessRoomReadyToPlayEvent");
        
        m_state = MP_SessionControllerState.SYNCING_GAME_DATA;
        // this is our permament list of participants
        m_participants = participants;
        // create the user scores
        m_overallUserScores = new TotalUserScores(m_participants);
        m_eliminatedUsersScores = new TotalUserScores(m_participants);
        // tell activity we're ready
        m_activity.RoomReadyToPlay();
    }
	
	
	private void ProcessRoomUserJoined(Participant p)
	{
	    // should we send the specific participant or not....
	    m_activity.UpdateRoom(m_room.GetParticipants());
	}
	
	private void ProcessRoomUserLeft(Participant p)
	{
	    // should we send the specific participant or not....
        m_activity.UpdateRoom(m_room.GetParticipants());
	}
	
	private void ProcessRoomJoined(ArrayList<Participant> participants)
	{
	    m_state = MP_SessionControllerState.WAITING_FOR_PLAYERS;
	    // let ui know that we have joined the room with a list of participants
	    // note - we ask room for all the participants because that includes ourselves too.. the payload is everyone but self in the room
	    m_activity.DisplayWaitingRoom(m_room.GetParticipants());
	    // wait for ready to play event from room
	}
	
	
	private void ProcessUserSignInSuccess(SignInType signInType) throws JSONException
	{
        // we signed in great success!
        m_state = MP_SessionControllerState.JOINING_ROOM;
        // register with server session for messages
        m_serverSession.RegisterForIncomingMessage(IncomingMessageType.GAME_DATA.string, this, true);
        m_serverSession.RegisterForIncomingMessage(IncomingMessageType.GAME_START.string, this);
        m_serverSession.RegisterForIncomingMessage(IncomingMessageType.GAME_END.string, this);
        // initiate the joining of the room
        m_room.Join(m_sessionParams);
	}
	
	private void ProcessUserSignInError(SignInStatusType signInStatusType)
	{
	    m_state = MP_SessionControllerState.INIT;
	    // TODO: notify activity of error with signing in
	}
	
	
	private void ProcessUserInitiatedSignIn(SignInType signInType) throws JSONException
	{
	    m_state = MP_SessionControllerState.SIGNING_IN;
	    // tell signincontroller to use preferred sign in type
	    m_serverSession.SignIn(signInType);
	}
	
	private void ProcessSignInEvent() throws JSONException
	{
	    m_state = MP_SessionControllerState.SIGNING_IN;
	    // TODO: Check if an avatar has been selected and if not prompt user to select one
	    if (m_serverSession.SignIn() == SignInType.NONE)
	    {
	        // means no sign in data is cached... tell activity to display the sign in options
	        m_activity.DisplaySignInOptions();
	    }
	    // wait for sign in controller to let us know that we have signed in
	}
	
	// the user has voluntarily left the room
	private void ProcessLeftRoomEvent() throws JSONException
	{
		Log.d(TAG, "MP_SessionController ProcessLeftRoomEvent");
			
	}	
	
	private void ProcessMiniGameCountdownCompleteEvent() throws JSONException
	{
		Log.d(TAG, "MP_SessionController ProcessMiniGameCountdownCompleteEvent");
		
		// display the current minigame
		m_activity.DisplayMinigame(m_gameData.get(m_currentMinigameIndex));
	}
	
	
	private void ProcessGameDataSyncedEvent() throws JSONException
	{
		
		Log.d(TAG, "MP_SessionController ProcessGameDataSyncedEvent");
		// init the overall scores hashmap
		//m_overallUserScores.InitWithAllParticipants();
		// clear the eliminated user scores
		//m_eliminatedUsersScores.clear();
		
		// set the minigame index to 0
		m_currentMinigameIndex = 0;
		
		ProcessGameStarting();
	}
	
	
	private void ProcessGameStarting() throws JSONException
	{
		Log.d(TAG, "MP_SessionController ProcessGameStarting");
		
		// check if we are in final game or lone user before game starts
		// TODO: if we are lone user due to a user leaving room, we get Room Error... need to handle this scenario somehow where we are determined the winner
		
		m_state = MP_SessionControllerState.WAITING_FOR_START_GAME;
		// TODO: Start fail safe timer in case we don't get GAME_START message
	}
	
}
