package com.qualgames.fastfingerz.mp_session.controller;

import java.util.List;

public class RoomEventPayload {

	public Room room = null;
	public enum RoomEventType
	{
		USERS_JOINED,
		USERS_LEFT,
		GENERIC
	}
	public RoomEventType roomEventType = null;
	public List<String> updatedParticipantIDs = null;
	
	public RoomEventPayload(Room room)
	{
		this.room = room;
		this.roomEventType = RoomEventType.GENERIC;
	}
	
	public RoomEventPayload(Room room, RoomEventType roomEvent, List<String> updatedParticipantIDs)
	{
		this.room = room;
		this.roomEventType = roomEvent;
		this.updatedParticipantIDs = updatedParticipantIDs;
	}
}
