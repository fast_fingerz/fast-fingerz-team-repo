package com.qualgames.fastfingerz.mp_session.controller;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.qualgames.fastfingerz.mp_session.Participant;
import com.qualgames.fastfingerz.mp_session.SessionParams;
import com.qualgames.fastfingerz.mp_session.server_session.OutgoingMessage;
import com.qualgames.fastfingerz.mp_session.server_session.ServerSession;
import com.qualgames.fastfingerz.mp_session.server_session.SimpleOutgoingMessage;
import com.qualgames.fastfingerz.mp_session.server_session.SocketIncomingMessageListener;
import com.qualgames.fastfingerz.mp_session.server_session.TCPSocket;

public class Room implements SocketIncomingMessageListener {
    
    private final static String TAG = "Fast Fingerz::" + Room.class.getSimpleName();
    
    private ArrayList<Participant> m_participants;
    private Participant m_self;
    private String m_id;
    private RoomState m_state;
    private ServerSession m_serverSession;
    private RoomUpdateListener m_roomUpdateListener;
    
    public interface RoomUpdateListener 
    {    
        public void HandleUserJoined(Participant p);
        public void HandleUserLeft(Participant p);
        public void HandleRoomDestroyed();
        public void HandleRoomJoined(ArrayList<Participant> participantsInRoom);
        public void HandleRoomReady(ArrayList<Participant> participantsInRoom);
        public void HandleUnexpectedError(String message);
    }

    
    private enum OutgoingMessageType
    {
        JOIN_ROOM("join_room"),
        LEAVE_ROOM("leave_room");
        
        public String string;
        
        private OutgoingMessageType(String string)
        {
            this.string = string;
        }
    }
    
    private enum IncomingMessageType
    {
        JOIN_ROOM_ACK(""),  // TODO: Define in server maybe?
        USER_JOINED("room_user_joined"),    // TODO: Have a room status message instead of joined and left?
        USER_LEFT("room_user_left"),
        ROOM_READY("room_ready"),
        END("room_end");        
        
        public String string;
        
        private IncomingMessageType(String string)
        {
            this.string = string;
        }
    }
    
    private enum MessageProtocol
    {
        PARAMS("params"),
        USER("user"),
        USERS("users"),
        ROOM_ID("roomId"),
        ERR("err"),
        DATA("data");       
        
        public String string;
        
        private MessageProtocol(String string)
        {
            this.string = string;
        }
    }
    
    private enum RoomState 
    {
        INIT,
        JOINING_ROOM,
        WAITING_FOR_OTHERS_TO_JOIN,
        READY_TO_PLAY
    }
    
    
    public Room( ServerSession serverSession, RoomUpdateListener roomUpdateListener ) 
    {
        m_participants = new ArrayList<Participant>();
        m_self = Participant.GetSelf();
        m_participants.add(m_self);
        m_state = RoomState.INIT;
        m_serverSession = serverSession;
        m_roomUpdateListener = roomUpdateListener;
    }
    
    
    public ArrayList<Participant> GetParticipants()
    {
        return m_participants;
    }
    
    
    public void Join(SessionParams params) throws JSONException
    {
        if (!m_serverSession.IsConnected())
        {
            // SHOULD NOT BE HERE!!! Need to throw some error here 
            Log.d(TAG, "ERROR! Server session not established!");
            m_roomUpdateListener.HandleUnexpectedError("Not connected to server!");
            return;
        }
        
        // let's register for messages if we haven't already
        m_serverSession.RegisterForIncomingMessage(IncomingMessageType.USER_JOINED.string, this);
        m_serverSession.RegisterForIncomingMessage(IncomingMessageType.ROOM_READY.string, this);
        m_serverSession.RegisterForIncomingMessage(IncomingMessageType.USER_LEFT.string, this);
        m_serverSession.RegisterForIncomingMessage(IncomingMessageType.END.string, this);
        
        // send the join request and wait for ACK
        m_serverSession.Send(new JoinRoomMessage(params, m_self), this);
        
        m_state = RoomState.JOINING_ROOM;
    }
    
    public class JoinRoomMessage extends OutgoingMessage
    {
        private SessionParams m_params;
        private Participant m_self;
        
        public JoinRoomMessage(SessionParams params, Participant self)
        {
            super(OutgoingMessageType.JOIN_ROOM.string);
            m_params = params;
            m_self = self;
        }
        
        @Override
        public JSONObject EncodeToJSON() throws JSONException 
        {
            Log.d(TAG, "JoinRoomMessage::EncodeToJSON");
            JSONObject json = new JSONObject();
            Log.d(TAG, "JoinRoomMessage::no crash 1");
            
            json.put(MessageProtocol.PARAMS.string, m_params.EncodeToJSON());
            Log.d(TAG, "JoinRoomMessage::no crash 2");
            //json.put(MessageProtocol.USER.string, m_self.EncodeToJSON());

            return json;
        }
    } 
    
    
    public void LeaveRoom() throws JSONException
    {
        m_state = RoomState.INIT;
        m_serverSession.Send(new SimpleOutgoingMessage(OutgoingMessageType.LEAVE_ROOM.string));
    }
    
    
    @Override
    public void HandleIncomingMessage(String event, JSONObject data) throws JSONException 
    {
        Log.d(TAG, "Received incoming message " + data.toString());
        if ( event.equals(IncomingMessageType.USER_JOINED.string) )
        {
            HandleUserJoined(data);
        }
        else if ( event.equals(IncomingMessageType.USER_LEFT.string) )
        {
            HandleUserLeft(data);
        }
        else if ( event.equals(IncomingMessageType.END.string) )
        {
            HandleRoomEnded(data);
        }
        else if ( event.equals(OutgoingMessageType.JOIN_ROOM.string + TCPSocket.ACK) )
        {
            HandleRoomJoinAck(data);
        }
        else if ( event.equals(IncomingMessageType.ROOM_READY.string) )
        {
            HandleRoomReady(data);
        }
        else
        {
            Log.d(TAG, "Unexpected incoming message " + event + " in state " + m_state);
        }   
    }
    
    
    private void HandleRoomReady(JSONObject json) throws JSONException
    {
        Log.d(TAG, "HandleRoomReady");
        
        m_state = RoomState.READY_TO_PLAY;
        
        m_roomUpdateListener.HandleRoomReady(m_participants);
    }
    
    private void HandleRoomJoinAck(JSONObject json) throws JSONException
    {
        Log.d(TAG, "HandleRoomJoinedAck");
        
        
        if (!json.getString(MessageProtocol.ERR.string).isEmpty())
        {
            Log.d(TAG, "Error " + json.getString(MessageProtocol.ERR.string) + " for Room Join ACK!!!");
            return;
        }
        
        JSONObject data = json.getJSONObject(MessageProtocol.DATA.string);
        ArrayList<Participant> participants = Participant.GenerateFromData(data.getJSONArray(MessageProtocol.USERS.string));
        AddParticipants(participants); 
        
        for (Participant p : m_participants)
        {
            Log.d(TAG, "p.id " + p.id);
        }

        m_id = data.getString(MessageProtocol.ROOM_ID.string);
        m_state = RoomState.WAITING_FOR_OTHERS_TO_JOIN;
 
        m_roomUpdateListener.HandleRoomJoined(m_participants);
    }
    
    private void HandleRoomEnded(JSONObject data)
    {
        Log.d(TAG, "HandleRoomEnded");
        m_participants.clear();
        m_state = RoomState.INIT;
        
        m_roomUpdateListener.HandleRoomDestroyed();
    }
    
    private void HandleUserLeft(JSONObject json) throws JSONException
    {
        Log.d(TAG, "HandleUserLeft");
        Participant p = Participant.GenerateFromData(json);
        
        RemoveParticipant(p);
        m_roomUpdateListener.HandleUserLeft(p);
    }
    
    private void HandleUserJoined(JSONObject json) throws JSONException
    {
        Log.d(TAG, "HandleUserJoined");
        Participant p = Participant.GenerateFromData(json);
        
        AddParticipant(p);
        m_roomUpdateListener.HandleUserJoined(p);
    }
    
    
    private void AddParticipants(ArrayList<Participant> participantsToAdd)
    {
        for (Participant p : participantsToAdd)
        {
            AddParticipant(p);
        }
    }
    
    private void AddParticipant(Participant p)
    {
        m_participants.add(p);
    }
    
    
    private void RemoveParticipant(Participant p)
    {
        m_participants.remove(p);
    }
}
