package com.qualgames.fastfingerz.mp_session;

import java.util.ArrayList;

public class TotalUserScores extends UserScores {
    
    public TotalUserScores(ArrayList<Participant> participants)
    {
        super(participants);
    }

    @Override
    protected int GetScore(Participant p) {
        return p.totalScore;
    }

    public void Add(ArrayList<Participant> participantList)
    {
        for (Participant p : participantList)
        {
            Add(p);
        }
    }

    // this will add gamescore to overall score
    public void Add(Participant p)
    {
        Participant curParticipant = m_sortedScoreList.get(m_sortedScoreList.indexOf(p));
        // The total score should be based on finished position and NOT game score
        curParticipant.totalScore += GetScoreFromFinishedPosition(p.finishedPosition);

        UpdateUser(curParticipant);
    }
    
    // TODO: This information will come from server as part of "cupSummary" message
    private int GetScoreFromFinishedPosition(int finishedPosition)
    {
        switch (finishedPosition)
        {
        case 1:
            return 10;
        case 2:
            return 7;
        case 3:
            return 5;
        case 4:
            return 3;
        case 5:
            return 2;
        case 6:
            return 1;
        default:
            return 1;
        }
    }

}
