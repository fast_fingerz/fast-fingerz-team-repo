package com.qualgames.fastfingerz.mp_session;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import android.util.Log;

public class RaceUserScores extends UserScores {
    
    public RaceUserScores(ArrayList<Participant> participants)
    {
        super(participants);
    }

    @Override
    protected int GetScore(Participant p) 
    {
        return p.gameScore;
    }
    
    // for game score only
    public void UpdateMyScore(int myScore)
    {
        Log.d(TAG, "UserScores::UpdateMyScore");

        // let's find ourselves from the list
        Participant self = m_sortedScoreList.get(m_sortedScoreList.indexOf(Participant.GetSelf()));
        self.gameScore = myScore;

        UpdateUser(self);
    }

    public void UpdateUserScore(Participant p)
    {
        Log.d(TAG, "UserScores::UpdateUserScore index " + m_sortedScoreList.indexOf(p));
        Participant curParticipant = m_sortedScoreList.get(m_sortedScoreList.indexOf(p));
        curParticipant.gameScore = p.gameScore;

        UpdateUser(curParticipant);
    }
    
    public void WriteUserFinalScore(Participant p)
    {
        Participant curParticipant = m_sortedScoreList.get(m_sortedScoreList.indexOf(p));
        curParticipant.gameScore = p.gameScore;
        curParticipant.finishedPosition = p.finishedPosition;
        curParticipant.timeElapsed = p.timeElapsed;
        curParticipant.raceFinishedStatus = p.raceFinishedStatus;
        
        UpdateUser(curParticipant);
    }
    
    public ArrayList<Participant> GetFinishedPositionSortedList()
    {
        SortBasedOnFinishedPosition();
        
        return m_sortedScoreList;
    }
    
    private void SortBasedOnFinishedPosition()
    {
        Log.d(TAG, "UserScores::SortBasedOnFinishedPosition");  
        
        Collections.sort(m_sortedScoreList, new Comparator<Participant>()
        {

            @Override
            public int compare(Participant s1, Participant s2) {
                if (s1.finishedPosition > s2.finishedPosition)
                {
                    return -1;
                }
                if (s1.finishedPosition < s2.finishedPosition)
                {
                    return 1;
                }
                return 0;
            }
            
        });
    }
}
