package com.qualgames.fastfingerz;

import java.util.List;
import com.qualgames.fastfingerz.mp_session.controller.RoomEventPayload;

import android.content.Context;

public class GlobalDataContainer {

	private static Context myContext = null;
	private static RoomEventPayload myRoomEventPayload = null;
	private static List<String> updateRoomPayloadParticipantIDs = null;
	private static boolean isMyUserServerUser = false;
	private static String serverUserParticipantId = null;
	private static String myDisplayName = null;
	private static String myParticipantID = null;
	
	public static void SetContext(Context context)
	{
		myContext = context;
	}
	
	public static Context GetContext()
	{
		return myContext;
	}
	
	public static void SetRoomEventPayload(RoomEventPayload roomEventPayload)
	{
		myRoomEventPayload = roomEventPayload;
	}
	
	public static RoomEventPayload GetRoomEventPayload()
	{
		return myRoomEventPayload;
	}
	
	public static void SetIsMyUserServerUser(boolean isServerUser)
	{
		isMyUserServerUser = isServerUser;
	}
	
	public static boolean GetIsMyUserServerUser()
	{
		return isMyUserServerUser;
	}
	
	public static void SetServerUserParticipantID(String serverUserID)
	{
		serverUserParticipantId = serverUserID;
	}
	
	public static String GetServerUserParticipantID()
	{
		return serverUserParticipantId;
	}
	
	public static void SetMyDisplayName(String myName)
	{
		myDisplayName = myName;
	}
	
	public static String GetMyDisplayName()
	{
		return myDisplayName;
	}
	
	public static void SetMyParticipantID(String myPartID)
	{
		myParticipantID = myPartID;
	}
	
	public static String GetMyParticipantID()
	{
		return myParticipantID;
	}
}
