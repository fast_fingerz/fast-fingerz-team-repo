package com.qualgames.fastfingerz.controller;

import org.json.JSONException;

import com.qualgames.fastfingerz.GlobalDataContainer;
import com.qualgames.fastfingerz.minigame.view.MinigameScreen;
import com.qualgames.fastfingerz.mp_session.controller.MP_SessionController;
import com.qualgames.fastfingerz.mp_session.view.MP_SessionActivity;
import com.qualgames.fastfingerz.view.GameTypeActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

// TODO: get rid of JSON for this class
public class MainController {
	
	// For logging
	final static String TAG = "Fast Fingerz";
	
	private Context m_myContext;
	// most likely main screen will not need state but let's keep this variable just in case
	private MainControllerState m_state;
	// boolean to know we are waiting on sign to play a game
	private boolean m_signInPendingToPlayGame;
	
	private GameTypeActivity m_activity;
	
	// enum for controller state
	private enum MainControllerState
	{
		INIT,
		PLAYING_MULTI_PLAYER,
		PLAYING_SINGLE_PLAYER;
	}
	
	// enum for internal event
	private enum MainControllerEvent {
		
		SINGLE_PLAYER_BUTTON_PRESS,
		MULTI_PLAYER_BUTTON_PRESS,
		OPTIONS_BUTTON_PRESS,
		SIGN_IN_BUTTON_PRESS,
		ACTIVITY_RESUME,
		LAUNCH;
	}
	
	// Singleton implementation
	/*
	private static MainController m_instance = null;
	public static MainController getInstance() {
		Log.v(TAG, "MainController getInstance");
		if (m_instance == null) {
			Log.v(TAG, "New GameController");
			m_instance = new MainController();
		}
		return m_instance;
	}
	protected MainController()
	{
		Log.d(TAG, "MainController constructor");
		m_state = MainControllerState.INIT;
		m_signInPendingToPlayGame = false;
	}
	*/
	
	public MainController(GameTypeActivity activity)
	{
	    m_activity = activity;
	    m_state = MainControllerState.INIT;
	}
	
	// TODO: This class needs a whole stripping down and refactoring
	
	// MainActivity will call this from onCreate to pass Context
	public void Init(Activity activity) throws JSONException
	{
		Log.d(TAG, "MainController::Init");
		m_myContext = activity.getApplicationContext();
		// set the GlobalDataContainer
		GlobalDataContainer.SetContext(m_myContext);
		m_state = MainControllerState.INIT;
		
		//initialize sign-in controller
		//SignInController.getInstance().Init(activity);
	}
	
	// Interface functions for Activity classes to interact with MainController
	public void IFaceSinglePlayerButtonPress() throws JSONException
	{
		ProcessControllerEvent(MainControllerEvent.SINGLE_PLAYER_BUTTON_PRESS);
	}
	
	public void IFaceMultiPlayerButtonPress() throws JSONException
	{
		ProcessControllerEvent(MainControllerEvent.MULTI_PLAYER_BUTTON_PRESS);
	}

	public void IFaceSignInButtonPress() throws JSONException
	{
		ProcessControllerEvent(MainControllerEvent.SIGN_IN_BUTTON_PRESS);
	}
	
	public void IFaceOnResume() throws JSONException
	{
		ProcessControllerEvent(MainControllerEvent.ACTIVITY_RESUME);
	}
	
	// Call from other controllers to indicate take over
	// possibly have enums here detailing information about why we are back to main controller?
	public void Launch() throws JSONException
	{
		ProcessControllerEvent(MainControllerEvent.LAUNCH);
	}
	
	/**************************** EVENT HANDLING 
	 * @throws JSONException ******************************/
	// Process user event from MainActivity
	private void ProcessControllerEvent( MainControllerEvent event ) throws JSONException
	{
		Log.d(TAG, "MainController::ProcessControllerEvent event " + event);
		switch (m_state)
		{
		case INIT:
			ProcessControllerEventInInitState( event );
			break;
		case PLAYING_SINGLE_PLAYER:
			// TODO: Handle any actions to be taken (show ads?)
			// if we get an event in this state most likely activity is resuming
			ProcessControllerEventInPlayingSinglePlayerState( event );
			break;
		case PLAYING_MULTI_PLAYER:
			// if we get an event in this state most likely activity is resuming
			ProcessControllerEventInPlayingMultiPlayerState( event );
			break;
		default:
			Log.d(TAG, "MainController::Error in state " + event);
			break;
		}
	}
	
	private void ProcessControllerEventInPlayingSinglePlayerState (MainControllerEvent event)
	{
		Log.d(TAG, "MainController::ProcessControllerEventInPlayingSingePlayerState event " + event);
		switch (event)
		{
			case LAUNCH:
				ProcessLaunchEvent(true);
			default:
				break;
		}
	}
	
	private void ProcessControllerEventInPlayingMultiPlayerState (MainControllerEvent event)
	{
		Log.d(TAG, "MainController::ProcessControllerEventInPlayingMultiPlayerState event " + event);
		switch (event)
		{
			case LAUNCH:
				ProcessLaunchEvent(false);
				break;
			case ACTIVITY_RESUME:
				ProcessActivityResumeEvent();
				break;
			default:
				break;
		}
	}
	
	private void ProcessLaunchEvent(boolean isSinglePlayerState)
	{
		Log.d(TAG, "MainController::ProcessLaunchEvent");
		// go back to init state
		m_state = MainControllerState.INIT;
		// send intent to MainActivity to come in focus
		Intent intent = new Intent(m_myContext, GameTypeActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		m_myContext.startActivity(intent);
		// maybe launch an ad or something
	}
	
	private void ProcessControllerEventInInitState( MainControllerEvent event ) throws JSONException
	{
		Log.d(TAG, "MainController::ProcessControllerEventInInitState event " + event);
		switch (event)
		{
		case MULTI_PLAYER_BUTTON_PRESS:
			ProcessMultiPlayerButtonPressInInitState();
			break;
		case SINGLE_PLAYER_BUTTON_PRESS:
			ProcessSinglePlayerButtonPressInInitState();
			break;
		case OPTIONS_BUTTON_PRESS:
			// TODO: options such as volume and fx
			break;
		case SIGN_IN_BUTTON_PRESS:
			//SignInController.getInstance().UserInitiatedSignIn();
			break;
		case LAUNCH:
			// we should not be here!
			Log.d(TAG, "ERROR: MainController Received LAUNCH event in INIT state. Launch anyway");
			break;
		case ACTIVITY_RESUME:
			ProcessActivityResumeEvent();
			break;
		default:
			break;
		}
	}
	
	
	private void ProcessActivityResumeEvent()
	{
		Log.d(TAG, "MainController::ProcessActivityResumeEvent");
		
		// this means that we got a resume event after attempting to sign in... let's notify MP_SessionController
		if (m_signInPendingToPlayGame)
		{
			try {
				ProcessSignInSuccess();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		// reset our variables
		// maybe check from which state we came from and show ads?
		m_state = MainControllerState.INIT;
		m_signInPendingToPlayGame = false;
	}

	
	/***************************** MULTIPLAYER HANDLING 
	 * @throws JSONException ************************/
	private void ProcessMultiPlayerButtonPressInInitState() throws JSONException
	{
		Log.d(TAG, "MainController::ProcessMultiPlayerButtonPressInInitState");
		
		SwitchControlToMPSession();
		/*
		m_state = MainControllerState.PLAYING_MULTI_PLAYER;
		// sign in if we need to
		if ( SignInController.getInstance().IsSignedIn() == true )
		{
			SwitchControlToMPSession();
			return;
		}
		// attempt to sign in
		SignInController.getInstance().UserInitiatedSignIn();
		// set boolean so we know what to do when sign in events come in
		m_signInPendingToPlayGame = true;
		*/
	}	
	
	private void SwitchControlToMPSession() throws JSONException
	{
		Log.d(TAG, "MainController::SwitchControlToMPSession");
		
		Intent intent = new Intent(m_activity.getApplicationContext(), MP_SessionActivity.class);
        m_activity.startActivity(intent);
	}
	
	/***************************** SINGLE PLAYER HANDLING ************************/
	private void ProcessSinglePlayerButtonPressInInitState()
	{
		Log.d(TAG, "MainController::ProcessSinglePlayerButtonPressInInitState");
		m_state = MainControllerState.PLAYING_SINGLE_PLAYER;
	}	
	
	private void SwitchControlToSPSession()
	{
		Log.d(TAG, "MainController::SwitchControlToSPSession");
		// initialize the class if it hasn't been already
		//SP_SessionController.getInstance().Init(m_myContext);
		// tell MP_SessionController to take over
		//SP_SessionController.getInstance().Launch();
	}
	
	/***************************** GOOGLE+ SIGN IN HANDLING 
	 * @throws JSONException ************************/
	private void ProcessSignInSuccess() throws JSONException
	{
		Log.d(TAG, "MainController::ProcessSignInSuccess in state " + m_state);
		if (m_signInPendingToPlayGame == false)
		{
			Log.d(TAG, "Just signed in... nothing more to do");
			return;			
		}
		
		switch( m_state )
		{
			case PLAYING_MULTI_PLAYER:
				SwitchControlToMPSession();
				break;
			case PLAYING_SINGLE_PLAYER:
				SwitchControlToSPSession();
				break;
			default:
				break;
		}
		
		m_signInPendingToPlayGame = false;		
	}
	
	private void ProcessSignInFailure()
	{
		Log.d(TAG, "MainController::ProcessSignInFailure in state " + m_state);
		// reset variables
		m_signInPendingToPlayGame = false;
		m_state = MainControllerState.INIT;		
	}
	
	// interface for SignInController
	public void onSignInSuccess() throws JSONException
	{
		ProcessSignInSuccess();
	}
	
	public void onSignInFailure()
	{
		ProcessSignInFailure();
	}
	
}
