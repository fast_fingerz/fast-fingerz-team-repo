package com.qualgames.fastfingerz.controller;

import com.qualgames.fastfingerz.minigame.powerup.PowerUp.PowerUpType;
import com.qualgames.fastfingerz.mp_session.SessionParams;

public class CoinController {
	
	public static int GetCoinValue(PowerUpType powerUpType) {
		
		switch(powerUpType) {
		case EXTRA_LIFE:
			return 3;
		case INVINCIBLE:
		case SPEED_BOOST:
			return 5;
		case TIME_BOMB:
			return 10;
		default:
			// should never come here
			return 0;	
		}
	}
	
	public static int GetMyCoins() {
		// TODO: read from SharedPrefs
		return 20;
	}
	
	public static void UseCoins(int coinAmount) {
		// TODO: write to SharedPrefs
	}
	
	public static int CoinReward(int placeFinished, int numCompetitors, SessionParams.RaceType raceType) {
		
		// TODO: determine based on finish how many coins to reward and write to SharePrefs
		return 5;		
	}
	
	public static void AddCoins(int coinsToAdd) {
		// TODO: write to SharedPrefs
	}

}
