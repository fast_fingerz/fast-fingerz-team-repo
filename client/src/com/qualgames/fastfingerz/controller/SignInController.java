package com.qualgames.fastfingerz.controller;

import android.app.Activity;
import android.util.Log;

// this controller is responsible for only singing in
public class SignInController {

	// for logging
	final static String TAG = "Fast Fingerz";
	
	private SignInControllerState m_state;
	private Activity m_activity;
	
	// enum for controller state
	private enum SignInControllerState
	{
		INIT,
		SETUP_READY,
		SIGNING_IN,
		SIGNED_IN;	
	}
	
	// enum for controller events
	private enum SignInControllerEvent
	{
		INITIALIZE,
		USER_SIGN_IN_REQ,
		SIGN_IN_SUCCESS,
		SIGN_IN_FAILURE;
	}
	
	// Singleton implementation
	private static SignInController m_instance = null;
	public static SignInController getInstance() {
		Log.v(TAG, "SignInController getInstance");
		if (m_instance == null) {
			Log.v(TAG, "New SignInController");
			m_instance = new SignInController();
		}
		return m_instance;
	}
	protected SignInController()
	{
		Log.d(TAG, "SignInController constructor");
		m_state = SignInControllerState.INIT;
	}
	
	/*
	// Probably only MainController will call this
	public void Init(Activity activity) throws JSONException
	{
		
		Log.d(TAG, "SignInController Init");
		m_activity = activity;
		// check to make sure we are not already signed in
		if ( m_gameHelper != null && m_gameHelper.isSignedIn() )
		{
			ProcessSignInSuccessEvent();
			return;
		}
		// create game helper object
		m_gameHelper = new GameHelper(m_activity, GameHelper.CLIENT_GAMES);
		// enable logger
		m_gameHelper.enableDebugLog(true);
		// call setup
		m_gameHelper.setup(this);
		// change state to indicate setup is done
		m_state = SignInControllerState.SETUP_READY;
		
	}
	
	public void UserInitiatedSignIn() throws JSONException
	{
		Log.d(TAG, "SignInController::UserInitiatedSignIn");
		ProcessUserInitiatedSignIn();
	}
	
	public boolean IsSignedIn()
	{
		if (m_gameHelper == null)
			return false;
		
		return m_gameHelper.isSignedIn();
	}
	
	
	private void ProcessUserInitiatedSignIn() throws JSONException
	{
		Log.d(TAG, "SignInController::ProcessUserInitiatedSignIn");
		if ( m_gameHelper == null || m_state == SignInControllerState.INIT )
		{
			HandleUnexpectedEvent(SignInControllerEvent.USER_SIGN_IN_REQ);
		}
		if ( m_gameHelper.isSignedIn() )
		{
			ProcessSignInSuccessEvent();
			return;
		}
		// change state
		m_state = SignInControllerState.SIGNING_IN;
		
		// attempt sign in
		m_gameHelper.beginUserInitiatedSignIn();
	}
	
	
	private void ProcessSignInSuccessEvent() throws JSONException
	{
		Log.d(TAG, "SignInController::ProcessSignInSuccessEvent");
		
		// change state
		m_state = SignInControllerState.SIGNED_IN;
		
		// notify MainController of sign in success
		//MainController.getInstance().onSignInSuccess();
	}
	
	
	private void ProcessSignInFailureEvent()
	{
		Log.d(TAG, "SignInController::ProcessSignInFailureEvent");
		
		// change state
		m_state = SignInControllerState.SETUP_READY;
		
		// notify MainController of sign in failure
		//MainController.getInstance().onSignInFailure();
	}
	
	// Handle whenever we get in an unexpected state
	private void HandleUnexpectedEvent(SignInControllerEvent event)
	{
		Log.d(TAG, "MP_SessionController::HandleUnexpectedEvent for event " + event +
				"in state " + m_state);
		
		// tell MainController to launch indicating error
		//MainController.getInstance().onSignInFailure();		
	}

	@Override
	public void onSignInFailed() 
	{
		ProcessSignInFailureEvent();
		
	}

	@Override
	public void onSignInSucceeded() 
	{
		Log.d(TAG, "SignInController::onSignInSucceeded");
		GlobalDataContainer.SetGoogleAPIClient(m_gameHelper.getApiClient());
		try {
			ProcessSignInSuccessEvent();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	*/
}
